//
//  File.h
//  FitnessChat
//
//  Created by Алексей on 26.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "NBS3Uploader.h"


typedef enum : int {
    FILE_Image = 1,
    FILE_Audio = 2,
    FILE_AudioVoice = 3,
    FILE_Video = 4,
} FileType;

@class Message, User;


@interface File : NSManagedObject

//This entities custom attributes/properties created in 'PATTERN' Core Data model.
@property (nonatomic, retain) NSString  * name;
@property (nonatomic, retain) NSString  * url;
@property (nonatomic, retain) Message   *message;
@property (nonatomic, retain) User      *user;
@property (nonatomic) FileType type;
@property (nonatomic) int filesize;
@property (nonatomic, retain) NSString  *info;

@property (strong, nonatomic) NSData    *data;

- (NSURL *)localUrl;
-(NSString *)localPath;
- (void)clear;
- (void)loadDataIfNeddedWithCompletion:(void (^)(NSData *data))completion;
- (void)loadDataIfNeddedWithCompletion:(void (^)(NSData *data))completion progress: (NBS3UploaderProgress)progress;

@end
