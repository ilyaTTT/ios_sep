//
//  Message.m
//  FitnessChat
//
//  Created by user on 23.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "Message.h"
#import "Chat.h"


@implementation Message

@dynamic date;
@dynamic messageId;
@dynamic text;
@dynamic type;
@dynamic userId;
@dynamic chat;
@dynamic helpDate;
@dynamic paymentAmount;


- (BOOL)isMy
{
//    return YES;
//  
//    int l = rand();
//    return l %2 == 0;
//    //NSLog(@"%@ -- %@",self.userId,[User currentUser].userId);
    return [self.userId isEqualToString:[User currentUser].userId];
}

- (void)setDate:(NSDate *)date
{
    static NSDateFormatter *formatter;
    if (formatter == nil)
    {
        formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"yyyy.MM.dd"];
    }
    self.helpDate = [formatter stringFromDate:date];
    [self willChangeValueForKey:@"date"];
    [self setPrimitiveValue:date forKey:@"date"];
    [self didChangeValueForKey:@"date"];
}

- (NSString *)sectionName
{
    static NSDateFormatter *formatter;
    if (formatter == nil)
    {
        formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"dd MMMM"];
    }

    return [formatter stringFromDate:self.date];
}

@end
