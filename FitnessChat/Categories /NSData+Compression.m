//
//  NSData+Compression.m
//  FitnessChat
//
//  Created by Kirill Kitaev on 21/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import "NSData+Compression.h"

@implementation NSData (Compression)

+ (NSData *)compressImage:(UIImage *)image compressRatio:(CGFloat)ratio maxCompressRatio:(CGFloat)maxRatio
{
    int MIN_UPLOAD_RESOLUTION = 1136 * 640;
    int MAX_UPLOAD_SIZE = 50;
    
    float factor;
    float currentResolution = image.size.height * image.size.width;
    
    if (currentResolution > MIN_UPLOAD_RESOLUTION)
    {
        factor = sqrt(currentResolution / MIN_UPLOAD_RESOLUTION) * 2;
        
        CGSize newSize = CGSizeMake(image.size.width / factor, image.size.height / factor);
        
        UIGraphicsBeginImageContextWithOptions(newSize, YES, 0.0);
        [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    CGFloat compression = ratio;
    CGFloat maxCompression = maxRatio;
    
    NSData *imageData = UIImageJPEGRepresentation(image, compression);
    while ([imageData length] > MAX_UPLOAD_SIZE && compression > maxCompression)
    {
        compression -= 0.10;
        imageData = UIImageJPEGRepresentation(image, compression);
    }
    
    return imageData;
}

@end
