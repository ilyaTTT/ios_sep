//
//  NSData+Compression.h
//  FitnessChat
//
//  Created by Kirill Kitaev on 21/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Compression)

+(NSData *)compressImage:(UIImage *)image compressRatio:(CGFloat)ratio maxCompressRatio:(CGFloat)maxRatio;

@end
