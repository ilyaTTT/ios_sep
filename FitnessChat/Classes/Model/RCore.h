//
//  RCore.h
//  RGS
//
//  Created by Алексей on 29.04.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

@class User;
@class Message;

NSString *stringTime(int time);

@interface RCore : NSObject

+ (RCore *)shared;
@property (strong, nonatomic) NSString *token;

- (void)loadUser:(NSString *)userId withCompletion:(void (^)(User *user))completion;

@end


