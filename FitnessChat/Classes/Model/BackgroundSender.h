//
//  BackgroundSender.h
//  FitnessChat
//
//  Created by Михаил Кузеванов on 25.04.16.
//  Copyright © 2016 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "BackgroundSenderInfo.h"
#import "Chat.h"

@interface BackgroundSender : NSObject{
    Reachability *reachability;
    NSMutableArray<BackgroundSenderInfo*> *queueList;
    Chat *chat;
}

+(BackgroundSender *)shared;

-(void)queueInfo:(BackgroundSenderInfo *)info;
-(void)removeInfoAtId: (NSUInteger) senderId;
-(void)setChat: (Chat *)c;
@property BOOL loading;
@end
