//
//  NBS3Uploader.m
//  Numbuster
//
//  Created by Алексей on 22.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

#import "NBS3Uploader.h"
#import "Constants.h"
#import "File.h"
#import <AFNetworking.h>
#import <GZIP.h>

@interface NBS3Uploader ()


@end

@implementation NBS3Uploader

static NBS3Uploader *uploader = nil;
+ (NBS3Uploader *)uploader
{
    
    if (uploader == nil)
    {
        uploader = [NBS3Uploader new];
        
    }

    return uploader;
}

#pragma mark - Data Unload

-(void)uploadFile:(File *)file
{
    [self uploadFile:file completion:nil];
}

-(void)processUploadAnswer: (NSData *)data error: (NSError *)error file: (File *)file completion:(void (^)(BOOL))completion
{
    if (error)
    {
        completion(NO);
        return;
    }
    
    NSDictionary* jDict = [NSJSONSerialization JSONObjectWithData:data
                                                         options:kNilOptions
                                                           error:&error];
    
    if (error)
    {
        completion(NO);
        return;
    }
    
    BOOL status = [jDict[@"status"] boolValue];
    if (!status)
    {
        completion(NO);
        return;
    }
    
    NSString *url = jDict[@"file"];
    file.url = url;
    
    NSNumber *fileSize = jDict[@"filesize"];
    if (fileSize)
        file.filesize = [fileSize intValue];
    
    [file.managedObjectContext save:nil];
    completion(YES);
    
    
    NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@\n%@", error, returnString);
    
    
//    file.url = [NSString stringWithFormat:@"https://%@.s3-eu-central-1.amazonaws.com/%@", [Constants pictureBucket], file.name];
//                [file.managedObjectContext save:nil];
//    
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    if(completion)
//                        _uploadProgressCounter += 0.142857143;//1;
//                        completion(YES);
//                });
}

-(void)uploadFile:(File *)file completion:(void (^)(BOOL))completion
{
    _uploadProgressCounter += 0.142857143;//1;
    
    
    NSString *dataType;
    NSString *fileName;
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidString = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
    CFRelease(uuid);
    switch (file.type)
    {
        case FILE_Image:
            dataType = @"image/jpeg";
            fileName = [NSString stringWithFormat:@"%@.jpg", uuidString];
            break;
        case FILE_Audio:
        case FILE_AudioVoice:
            dataType = @"audio/mp3";
            fileName = [NSString stringWithFormat:@"%@.mp3", uuidString];
            break;
        case FILE_Video:
            dataType = @"video/mp4";
            fileName = [NSString stringWithFormat:@"%@.mp4", uuidString];
            break;
            
        default:
            break;
    }
    
    NSURL *url = [NSURL URLWithString:@"https://admin.1fitchat.ru/rest/chat/upload-file"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"----WebKitFormBoundaryUGQ3gfYtu6CBRfRi";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"file"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"filekey"] dataUsingEncoding:NSUTF8StringEncoding]];
    //
    // add image data
    if (file.data)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", @"file", fileName] dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *dataTypeStr = [NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", dataType];
        [body appendData:[dataTypeStr dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *bodyStr = [[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding];
        NSLog(@"%@",  bodyStr);
        
        [body appendData:file.data];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    if ([NSThread isMainThread])
    {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            
            [self processUploadAnswer:data error:error file:file completion:completion ];
        }];
        
    }
    else
    {
        
        NSURLResponse *response;
        NSError *error;
        NSData *data = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&response
                                                         error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self processUploadAnswer:data error:error file:file  completion:completion];
        });
        
    }
    
}
    // Sends file data to Amazaon SS3 cloud server.
    //AWSS3PutObjectRequest *req = [AWSS3PutObjectRequest new];
//    req = [AWSS3PutObjectRequest new];
//    req.key = file.name;
//    req.bucket = [Constants pictureBucket];
//    req.contentType = contentType;
//    req.ACL = AWSS3BucketCannedACLPublicRead;
//    
//    if (needCompress)
//    {
//        NSData *compressedData = [file.data gzippedDataWithCompressionLevel:1.0];
//        req.contentLength = [NSNumber numberWithInteger:[compressedData length]];
//        req.contentEncoding = @"gzip";
//        req.body = compressedData;
//        
//        ////NSLog(@"GZipCompression - original: %li compressed: %li win: %li", [file.data length], [compressedData length], [compressedData length] - [file.data length]);
//    }
//    else
//    {
//        req.body = file.data;
//        req.contentLength = [NSNumber numberWithInteger:[file.data length]];
//    }
//    
//    _uploadProgressCounter += 0.142857143;//1;
//    
//    AWSS3 *s3 = [AWSS3 S3ForKey:@"EUCentral1"];
//    [[s3 putObject:req] continueWithBlock:^id(AWSTask *task)
//    {
//        if (task.error)
//        {
//            if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain])
//            {
//                switch (task.error.code)
//                {
//                    case AWSS3TransferManagerErrorCancelled:
//                    case AWSS3TransferManagerErrorPaused:
//                        //NSLog(@"AWSS3 Transfer CANCELLED or PAUSED.");
//                        break;
//
//                    default:
//                        //NSLog(@"Upload failed: [%@]", task.error);
//                        break;
//                }
//            }
//            else
//            {
//                //NSLog(@"Upload failed: [%@]", task.error);
//            }
//            dispatch_async(dispatch_get_main_queue(), ^{
//                if(completion)
//                    _uploadProgressCounter += 0.142857143;//1;
//                completion(NO);
//            });
//        }
//        else if (task.result)
//        {
//            //NSLog(@"Transfer SUCCESS: %@", task.result);
//            
//            file.url = [NSString stringWithFormat:@"https://%@.s3-eu-central-1.amazonaws.com/%@", [Constants pictureBucket], file.name];
//            [file.managedObjectContext save:nil];
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                if(completion)
//                    _uploadProgressCounter += 0.142857143;//1;
//                    completion(YES);
//            });
//        }
//        
//        return nil;
//    }];
//    

//}

-(void)cancelUpload
{
//    [req cancel];
    //NSLog(@"Cancelled req:\n%@", req);
}


#pragma mark - Data Download

-(void)exportFile:(File *)file
{
    [self exportFile:file completion:nil];
}

-(void)exportFile:(File *)file completion:(void (^)(BOOL))completion
{
    [self exportFile:file completion:completion progress:NULL];
}

- (void)exportFile:(File *)file completion:(void (^)(BOOL succeess))completion progress:(NBS3UploaderProgress)progress
{
   
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:file.url]];
    
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        ////NSLog(@"Response: %@", responseObject);

        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSData *data;
            if([responseObject isGzippedData])
                 data = [responseObject gunzippedData];
            else
                 data = responseObject;
            
                file.data = data;
                [[RBase shared].moc save:nil];
                
                if(completion)
                    completion(YES);
        });
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@, %s", error, __PRETTY_FUNCTION__);
        
        if(completion)
            completion(NO);
        if(operation.response.statusCode == 403)
        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Произошла ошибка при отправке файла. Попробуйте ещё раз" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
        }
    }];
    
    if (progress)
    {
        [requestOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            progress(totalBytesExpectedToRead, totalBytesRead);
        }];
    }
    
//    requestOperation setd
    
    [requestOperation start];
}


@end
