//
//  MessageFactory.m
//  FitnessChat
//
//  Created by Михаил Кузеванов on 27.04.16.
//  Copyright © 2016 Алексей. All rights reserved.
//

#import "MessageFactory.h"
#import "File.h"
#import "NSData+Compression.h"

#define UD_KEY_ID @"MessageFactoryId"

@implementation MessageFactory

+(Message *)createMessageAtText:(NSString *)text andChat:(Chat *)chat
{
    
    Message *message    = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:[RBase shared].moc];
    message.chat        = chat;
    message.text        = text;
    message.type        = MESSAGE_Text;
    message.date        = [NSDate date];
    message.userId      = [User currentUser].userId;
    message.sending     = YES;
    message.localId = (int)[MessageFactory generateId];
    [[RBase shared] saveMain];
    
    return message;
}

+(Message *)createMessageAtAudioData:(NSData *)audioData andChat:(Chat *)chat andTime:(NSInteger)time
{
    File *file  = [NSEntityDescription insertNewObjectForEntityForName:@"File" inManagedObjectContext:[RBase shared].moc]; // As soon as this managed object is created it needs to be in a container. A named area called context, which in this case is 'RBase'.
    file.type   = FILE_Audio;
    file.data   = audioData;
    file.info   = [NSString stringWithFormat:@"%ld", (long)time];
    
    Message *message    = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:[RBase shared].moc];
    message.chat        = chat;
    message.file        = file;
    message.type        = MESSAGE_Audio;
    message.date        = [NSDate date];
    message.userId      = [User currentUser].userId;
    message.sending     = YES;
    message.localId = (int)[MessageFactory generateId];
    [[RBase shared] saveMain];
    
    return message;
}

+(Message *)createMessageAtVideoData:(NSData *)videoData andChat:(Chat *)chat
{
    File *file = [NSEntityDescription insertNewObjectForEntityForName:@"File" inManagedObjectContext:[RBase shared].moc];
    file.type = FILE_Video;
    file.data = videoData;
    
    
    Message *message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:[RBase shared].moc];
    message.chat = chat;
    message.file = file;
    message.type = MESSAGE_Video;
    message.date = [NSDate date];
    message.userId = [User currentUser].userId;
    message.sending = YES;
    message.localId = (int)[MessageFactory generateId];
    [[RBase shared] saveMain];
    
    return message;
}

+(Message *)createMessageAtImageData:(NSData *)imageData andChat:(Chat *)chat
{
    File *file = [NSEntityDescription insertNewObjectForEntityForName:@"File" inManagedObjectContext:[RBase shared].moc];
    file.type = FILE_Image;
    
    file.data = imageData;
    ////NSLog(@"original: %li compressed: %li", [file.data length], [compressedImage length]);
    
    Message *message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:[RBase shared].moc];
    message.chat = chat;
    message.file = file;
    message.type = MESSAGE_Image;
    message.date = [NSDate date];
    message.userId = [User currentUser].userId;
    message.sending = YES;
    message.localId = (int)[MessageFactory generateId];
    [[RBase shared] saveMain];

    
    return message;
}

+(NSInteger)generateId
{
    NSUserDefaults *uDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger generatedId = [uDefaults integerForKey:UD_KEY_ID];
    generatedId++;
    [uDefaults setInteger:generatedId forKey:UD_KEY_ID];
    [uDefaults synchronize];
    return generatedId;
}

@end
