//
//  FRequestPaymentServices.m
//  FitnessChat
//
//  Created by owel on 07/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import "FRequestPaymentServices.h"

@implementation FRequestPaymentServices

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"rest/payment/services?%@",[self accessTokenString]];
}

- (NSString *)body
{
    return nil;
}

- (NSString *)method
{
    return @"GET";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}

@end
