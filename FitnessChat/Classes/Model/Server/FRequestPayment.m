//
//  FRequestPayment.m
//  FitnessChat
//
//  Created by owel on 09/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import "FRequestPayment.h"
#import "Common.h"

@implementation FRequestPayment

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"rest/payment/payment"];
}

//- (NSString *)body
//{
////    NSString *escapedCryptogram = [self.cryptogram stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    
//    //NSLog(@"cryptogram :\n%@", self.cryptogram);
//    
//    return [NSString stringWithFormat:@"%@&service_id=%@&cryptogram=%@", [self accessTokenString], self.serviceId, self.cryptogram];
//
//}

//-(NSString *)authString
//{
//    NSString *authStr = [NSString stringWithFormat:@"%@:%@", CLOUD_PAYMENT_PUBLIC_ID, CLOUD_PAYMENT_SECRET_ID];
//    NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
//    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData initWithBase64EncodedData:authData options:0]];
//    
//    return authValue;
//}

-(NSData *)bodyData
{
    NSDictionary *dict = @{ @"token"        : [RCore shared].token,
                            @"service_id"   : self.serviceId,
                            @"cryptogram"   : self.cryptogram };
    
    NSError *error;
    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:&error];
    
    return data;
}

- (NSString *)method
{
    return @"POST";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}

@end
