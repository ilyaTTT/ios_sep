//
//  NWServer.m
//  NewToNewTemp
//
//  Created by Алексей on 24.04.14.
//  Copyright (c) 2014 Алексей. All rights reserved.
//

#import "NWServer.h"
#import "NWRequest.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "BackgroundSender.h"



@interface NSURLRequest (IgnoreSSL)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host;
@end

@implementation NSURLRequest (IgnoreSSL)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host
{
    return YES;
}

@end

@interface NWServer ()
{
    Reachability *internetReachable;
    Reachability *hostReachable;
}
@property (strong, nonatomic, readonly) NSOperationQueue *requestQueue;
@end

@implementation NWServer
@synthesize requestQueue = _requestQueue;

+(NWServer *)server
{
    static NWServer *server = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        if (server == nil){
            server = [NWServer new];
        }
    });
    
    return server;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
        
        internetReachable = [Reachability reachabilityForInternetConnection];
        [internetReachable startNotifier];
        
        hostReachable = [Reachability reachabilityWithHostName:@"www.apple.com"];
        [hostReachable startNotifier];
    }
    
    return self;
}

- (void) performRequest:(NWRequest *)myRequest
{
    NSString *body = myRequest.body;
    NSData *postData = myRequest.bodyData;
    if (postData == nil)
    {
        postData = [body dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    }
    
    NSString *postLength = [NSString stringWithFormat:@"%ld", (long)[postData length]];
    NSURL* url = [NSURL URLWithString:[myRequest.urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:myRequest.method];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    [request setTimeoutInterval:40];
    
    if (myRequest.authString)
    {
        [request setValue:myRequest.authString forHTTPHeaderField:@"Authorization"];
    }
    
    ////NSLog(@"Send request : %@",myRequest.urlString);
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:self.requestQueue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError){
                               
                               if (connectionError)
                               {
                                   [myRequest setError:connectionError];
                                   BackgroundSenderInfo *info = [myRequest createBackgroundSenderInfo];
                                   if (info && myRequest.backgroundSenderId == 0)
                                       [[BackgroundSender shared] queueInfo:info];
                               }
                               else if (data == nil)
                               {
                                   [myRequest setError:[myRequest errorWithMessage:@"Пустой ответ от сервера."]];
                               }
                               else
                               {
                                   NSError *error = nil;
                                   NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   
                                   if (error)
                                   {
                                       NSLog(@"%@",[NSString stringWithUTF8String:[data bytes]]);
                                       [myRequest setError:[myRequest errorWithMessage:[NSString stringWithUTF8String:[data bytes]]]];
                                   }
                                   else
                                   {
                                       if (myRequest.backgroundSenderId > 0)
                                           [[BackgroundSender shared] removeInfoAtId:myRequest.backgroundSenderId];
                                       [myRequest setResponse:dict];
                                   }
                               }
                           }];
}

#pragma - mark private

- (NSOperationQueue *)requestQueue
{
    if (_requestQueue == nil)
    {
        _requestQueue = [NSOperationQueue new];
        [_requestQueue setMaxConcurrentOperationCount:100];
    }
    
    return _requestQueue;
}

#pragma mark - network status

- (void)checkNetworkStatus:(NSNotification *)notice
{
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            ////NSLog(@"The internet is down.");
            self.internetActive = NO;
            self.wifiActive = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            ////NSLog(@"The internet is working via WIFI.");
            self.internetActive = YES;
            self.wifiActive = YES;
            break;
            
        }
        case ReachableViaWWAN:
        {
            ////NSLog(@"The internet is working via WWAN.");
            self.internetActive = YES;
            self.wifiActive = NO;
            break;
            
        }
    }
    
    NetworkStatus hostStatus = [hostReachable currentReachabilityStatus];
    switch (hostStatus)
    
    {
        case NotReachable:
        {
            ////NSLog(@"A gateway to the host server is down.");
            self.hostActive = NO;
            
            break;
            
        }
        case ReachableViaWiFi:
        {
            ////NSLog(@"A gateway to the host server is working via WIFI.");
            self.hostActive = YES;
            break;
            
        }
        case ReachableViaWWAN:
        {
            ////NSLog(@"A gateway to the host server is working via WWAN.");
            self.hostActive = YES;
            break;
            
        }
    }
    
}

@end
