//
//  FRequestUserGroup.m
//  FitnessChat
//
//  Created by user on 16.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestUserGroup.h"

@implementation FRequestUserGroup

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"rest/base/group-of-user?%@",[self accessTokenString]];
}

- (NSString *)body
{
    return nil;
}

- (NSString *)method
{
    return @"GET";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}

@end
