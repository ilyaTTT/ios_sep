//
//  FRequestPayment.h
//  FitnessChat
//
//  Created by owel on 09/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import "NWRequest.h"

@interface FRequestPayment : NWRequest

@property (strong, nonatomic) NSString  *serviceId;
@property (strong, nonatomic) NSString  *cryptogram;

@end
