//
//  FRequestConfirm.h
//  FitnessChat
//
//  Created by user on 10.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "NWRequest.h"

@interface FRequestConfirm : NWRequest
@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *phone;
@end
