//
//  NWRequest.h
//  NewToNewTemp
//
//  Created by Arcanite LLC on 24.04.14.
//  Copyright (c) 2014 Arcanite LLC. All rights reserved.
//

#import "NWRequestDelegate.h"
#import "BackgroundSenderInfo.h"

#define SERVER_URL @"http://admin.1fitchat.ru/" // PRODUCTION: @"https://fitconsultant.ru/" // DEVELOPMENT: @"https://dev.fitconsultant.ru/"

NSString* encodeToPercentEscapeString(NSString *string);


@interface NWRequest : NSObject

@property (retain, nonatomic, readonly) NSString *urlString;
@property (retain, nonatomic, readonly) NSString *body;
@property (strong, nonatomic)           NSData *bodyData;
@property (retain, nonatomic, readonly) NSString *method;
@property (retain, nonatomic, readonly) NSString *accessTokenString;
@property (strong, nonatomic, readonly) NSString *requestName;
@property (strong, nonatomic)           NSData *authData;
@property (strong, nonatomic)           NSString *authString;
@property (weak, nonatomic, readwrite) id <NWRequestDelegate> parent;


- (void)setResponse:(NSDictionary*)response;
- (void)setError:(NSError *)error;
- (void)callbackWithObject:(NSDictionary*)dict error:(NSError*)error;
- (void)start;

- (NSError *)errorWithMessage:(NSString *)message;

- (NSString *)securityKeyforString:(NSString *)string;
- (NSString *)appendString;

-(BackgroundSenderInfo *)createBackgroundSenderInfo;
@property NSInteger backgroundSenderId;

@end
