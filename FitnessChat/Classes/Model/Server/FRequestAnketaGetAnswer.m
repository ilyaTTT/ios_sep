//
//  FRequestAnketaGetAnswer.m
//  FitnessChat
//
//  Created by Алексей on 30.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestAnketaGetAnswer.h"

@implementation FRequestAnketaGetAnswer

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"/rest/base/replied-user-quests?%@&user_id=%@",[self accessTokenString],[User currentUser].userId];
}

- (NSString *)body
{
    return nil;
}

- (NSString *)method
{
    return @"GET";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}

@end
