//
//  FRequestAnketaSendAnswer.m
//  FitnessChat
//
//  Created by Алексей on 30.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestAnketaSendAnswer.h"
#import "Anketa.h"
#import "Question.h"
#import "Replies.h"

@implementation FRequestAnketaSendAnswer

- (NSString *)urlString
{
    
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"/rest/base/send-questionnaire?quest_id=%@&%@",_anketa.anketaId,[self accessTokenString]];
}


- (NSData *)bodyData
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    NSMutableDictionary *questions = [NSMutableDictionary new];
    
    for (Question *quest in _anketa.questions)
    {
        NSMutableDictionary *questDict = [NSMutableDictionary new];
        [questDict setObject:quest.type forKey:@"type"];
        if ([quest.type isEqualToString:@"text"])
        {
            NSString *answer = quest.createAnswer;
            if (answer != nil)
            {
                [questDict setObject:answer forKey:@"text"];
            }
        }
        else
            if ([quest.type isEqualToString:@"select_one"])
            {
                Replies *reply = [quest checkedReplies].lastObject;
                if (reply != nil)
                {
                    [questDict setObject:reply.repliesId forKey:@"reply_id"];
                }
            }
            else
            {
                NSMutableArray *repliesIds = [NSMutableArray new];
                NSArray *replies = [quest checkedReplies];
                for (Replies *reply in replies)
                {
                    if (reply != nil)
                    {
                        [repliesIds addObject:reply.repliesId];
                    }
                }
                [questDict setObject:repliesIds forKey:@"reply_ids"];
            }
        [questions setObject:questDict forKey:[quest.questionId stringValue]];
    }
    
    [dict setObject:questions forKey:@"questions_json"];
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:questions options:0 error:nil];

    return data;
}

- (NSString *)body
{
    return nil;
    NSMutableDictionary *dict = [NSMutableDictionary new];
    NSMutableDictionary *questions = [NSMutableDictionary new];
    
    for (Question *quest in _anketa.questions)
    {
        NSMutableDictionary *questDict = [NSMutableDictionary new];
        [questDict setObject:quest.type forKey:@"type"];
        if ([quest.type isEqualToString:@"text"])
        {
            [questDict setObject:quest.createAnswer forKey:@"text"];
        }
        else
            if ([quest.type isEqualToString:@"select_one"])
            {
                Replies *reply = [quest checkedReplies].lastObject;
                if (reply != nil)
                {
                    [questDict setObject:reply.repliesId forKey:@"reply_id"];
                }
            }
            else
            {
                NSMutableArray *repliesIds = [NSMutableArray new];
                NSArray *replies = [quest checkedReplies];
                for (Replies *reply in replies)
                {
                    if (reply != nil)
                    {
                        [repliesIds addObject:reply.repliesId];
                    }
                }
                [questDict setObject:repliesIds forKey:@"reply_ids"];
            }
        [questions setObject:questDict forKey:[quest.questionId stringValue]];
    }
    
    [dict setObject:questions forKey:@"questions_json"];
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:questions options:0 error:nil];

    NSString *json = [NSString stringWithUTF8String:[data bytes]];
    if (json == nil)
    {
        //NSLog(@"ooohh");
    }
    return [NSString stringWithFormat:@"questions_json=%@",json];
}

- (NSString *)method
{
    return @"POST";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}


@end
