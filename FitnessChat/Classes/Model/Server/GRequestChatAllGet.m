//
//  GRequestChatAllGet.m
//  globby
//
//  Created by Алексей on 30.03.15.
//  Copyright (c) 2015 bnet. All rights reserved.
//

#import "GRequestChatAllGet.h"

@implementation GRequestChatAllGet

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"?action=getChats"];
}

- (NSString *)body
{
    return [NSString stringWithFormat:@"%@",[self appendString]];
}

- (NSString *)method
{
    return @"POST";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}


@end
