//
//  FRequestMessageFileSend.h
//  FitnessChat
//
//  Created by Алексей on 30.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "NWRequest.h"


@interface FRequestMessageFileSend : NWRequest

@property (strong, nonatomic) Message *message;

@end
