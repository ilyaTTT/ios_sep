//
//  FRequestRegister.h
//  FitnessChat
//
//  Created by user on 10.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "NWRequest.h"
@class File;

@interface FRequestRegister : NWRequest
@property (strong, nonatomic) NSString *name;
@property (nonatomic) BOOL isMale;
@property (strong, nonatomic) NSDate *birthday;
@property (strong, nonatomic) File *smallImage;
@property (strong, nonatomic) File *bigImage;

@end
