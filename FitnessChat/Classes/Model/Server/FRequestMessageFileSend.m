//
//  FRequestMessageFileSend.m
//  FitnessChat
//
//  Created by Алексей on 30.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestMessageFileSend.h"
#import "File.h"

@implementation FRequestMessageFileSend

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"/rest/chat/send-message"];
}

- (NSString *)body
{
    NSString *type = @"image";
    if (_message.type == MESSAGE_Video)
    {
        type = @"video";
    }
    else
        if (_message.type == MESSAGE_Audio)
    {
        type = @"audio";
    }
    return [NSString stringWithFormat:@"%@&room_id=%@&type=%@&url=%@&local_id=%d",[self accessTokenString],_message.chat.chatId,type,_message.file.url, _message.localId];
}

- (NSString *)method
{
    return @"POST";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}


@end
