//
//  FRequestAnketaSendAnswer.h
//  FitnessChat
//
//  Created by Алексей on 30.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "NWRequest.h"
@class Anketa;
@interface FRequestAnketaSendAnswer : NWRequest
@property (strong, nonatomic) Anketa *anketa;

@end
