//
//  NWRequest.m
//  NewToNewTemp
//
//  Created by Алексей on 24.04.14.
//  Copyright (c) 2014 Алексей. All rights reserved.
//

#import "NWRequest.h"
#import "NWServer.h"

NSString* encodeToPercentEscapeString(NSString *string) {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

@implementation NWRequest

- (NSString *)urlString
{
    return SERVER_URL;
}

- (NSString *)accessTokenString
{
    return [NSString stringWithFormat:@"token=%@",[RCore shared].token];
}

- (void) callbackWithObject:(NSDictionary *)dict error:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.parent respondsToSelector:@selector(requestCompletion:withObject:andError:)])
        {
            [self.parent requestCompletion:self withObject:dict andError:error];
        }
    });
}

- (void)setResponse:(NSDictionary*)response
{
    [self callbackWithObject:response error:nil];
}

- (void)setError:(NSError *)error
{
    [self callbackWithObject:nil error:error];
}

- (void)start
{
    [[NWServer server] performRequest:self];
}

- (NSString *)requestName
{
    return [NSString stringWithFormat:@"%@", [self class]];
}

- (NSError *)errorWithMessage:(NSString *)message
{
    NSDictionary *dict = [NSDictionary dictionaryWithObject:message forKey:NSLocalizedDescriptionKey];
    NSError *error = [NSError errorWithDomain:self.requestName code:0 userInfo:dict];
    return error;
}


- (NSString *)securityKeyforString:(NSString *)string
{
    //Unused method...
    return string;
}


- (NSString *)appendString
{
    NSString *aString;
    return aString;
}


-(BackgroundSenderInfo *)createBackgroundSenderInfo
{
    return NULL;
}

@end
