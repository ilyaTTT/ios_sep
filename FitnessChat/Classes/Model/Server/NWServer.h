//
//  NWServer.h
//  NewToNewTemp
//
//  Created by Алексей on 24.04.14.
//  Copyright (c) 2014 Алексей. All rights reserved.
//
#import "NWRequest.h"

@interface NWServer : NSObject
+ (NWServer *)server;

@property (assign, nonatomic, readwrite) BOOL hostActive;
@property (assign ,nonatomic,readwrite) BOOL wifiActive;
@property (assign, nonatomic, readwrite) BOOL internetActive;

- (void)performRequest:(NWRequest *)myRequest;
@end
