//
//  FRequestAnkets.m
//  FitnessChat
//
//  Created by user on 17.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestAnkets.h"

@implementation FRequestAnkets

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"rest/base/questionnaires?%@",[self accessTokenString]];
}

- (NSString *)body
{
    return nil;
}

- (NSString *)method
{
    return @"GET";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}

@end