//
//  FRequestConfirm.m
//  FitnessChat
//
//  Created by user on 10.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestConfirm.h"

@implementation FRequestConfirm

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"/rest/auth"];
}


- (NSString *)body
{
    return [NSString stringWithFormat:@"phone=%@&password=%@", _phone, _code];
}


- (NSString *)method
{
    return @"POST";
}


- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}


@end
