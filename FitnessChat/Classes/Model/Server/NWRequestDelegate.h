//
//  NWRequestDelegate.h
//  NewToNewTemp
//
//  Created by Алексей on 24.04.14.
//  Copyright (c) 2014 Алексей. All rights reserved.
//

@class NWRequest;

@protocol NWRequestDelegate <NSObject>

@optional
- (void)requestCompletion:(NWRequest *)request
               withObject:(NSDictionary*)dict
                 andError:(NSError*)error;

@end
