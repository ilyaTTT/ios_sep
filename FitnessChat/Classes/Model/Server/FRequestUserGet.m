//
//  FRequestUserGet.m
//  FitnessChat
//
//  Created by user on 16.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestUserGet.h"

@implementation FRequestUserGet
- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"rest/base/user-profile?%@&id=%@",[self accessTokenString],_userId];
}

- (NSString *)body
{
    return nil;
}

- (NSString *)method
{
    return @"GET";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}

@end
