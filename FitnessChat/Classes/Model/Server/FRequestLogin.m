//
//  FRequestLogin.m
//  FitnessChat
//
//  Created by user on 10.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestLogin.h"

@implementation FRequestLogin

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"/rest/register"];
}

- (NSString *)body
{
    return [NSString stringWithFormat:@"phone=%@",_phone];
}

- (NSString *)method
{
    return @"POST";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}


@end
