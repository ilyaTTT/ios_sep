//
//  FRequestPushTokenSave.m
//  FitnessChat
//
//  Created by Алексей on 05.08.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestPushTokenSave.h"

@implementation FRequestPushTokenSave

- (NSString *)urlString
{
    NSString *string = [super urlString];
    NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"kPushToken"];
    return [string  stringByAppendingFormat:@"/rest/base/save-device-token-ios?%@&device_token=%@",[self accessTokenString],pushToken];
}

- (NSString *)body
{
    return nil;
}

- (NSString *)method
{
    return @"POST";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}

- (void)start
{
    NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"kPushToken"];
    //NSLog(@"/rest/base/save-device-token-ios?%@&device_token=%@",[self accessTokenString],pushToken);
    if ([RCore shared].token.length > 0 && pushToken.length > 0)
    {
        [super start];
    }
}

@end
