//
//  FRequestRegister.m
//  FitnessChat
//
//  Created by user on 10.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestRegister.h"
#import "File.h"

@implementation FRequestRegister

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"/rest/base/update-profile"];
}

- (NSString *)body
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSString *dateString = [formatter stringFromDate:_birthday];

    NSString *male = @"";
    if (_isMale)
    {
        male = @"male";
    }
    else
    {
        male = @"female";
    }
    
    return [NSString stringWithFormat:@"name=%@&birthday=%@&sex=%@&%@&avatar_large_url=%@&avatar_small_url=%@",_name,dateString,male,[self accessTokenString],_bigImage.url,_smallImage.url];
}

- (NSString *)method
{
    return @"POST";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}


@end
