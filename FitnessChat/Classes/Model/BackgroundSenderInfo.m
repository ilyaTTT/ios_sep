//
//  BackgroundSenderInfo.m
//  FitnessChat
//
//  Created by Михаил Кузеванов on 25.04.16.
//  Copyright © 2016 Алексей. All rights reserved.
//

#import "BackgroundSenderInfo.h"

@implementation BackgroundSenderInfo

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeInteger:_type forKey:@"_type"];
    [encoder encodeInteger:_backgroundSenderId forKey:@"_backgroundSenderId"];
}


- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if( self != nil )
    {
        _backgroundSenderId = [decoder decodeIntegerForKey:@"_backgroundSenderId"];
        _type = [decoder decodeIntegerForKey:@"_type"];
    }
    return self;
}

@end
