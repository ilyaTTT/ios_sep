//
//  BackgroundSenderInfo.h
//  FitnessChat
//
//  Created by Михаил Кузеванов on 25.04.16.
//  Copyright © 2016 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    BITMessage,
    BITImage,
    BITVideo,
    BITAudio
} BackgroundSenderInfoType;

@interface BackgroundSenderInfo : NSObject

@property BackgroundSenderInfoType type;
//@property NSData *addData;
@property NSInteger backgroundSenderId;
//@property NSInteger intTag;

@end
