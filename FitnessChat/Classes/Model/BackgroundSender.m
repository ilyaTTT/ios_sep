//
//  BackgroundSender.m
//  FitnessChat
//
//  Created by Михаил Кузеванов on 25.04.16.
//  Copyright © 2016 Алексей. All rights reserved.
//

#import "BackgroundSender.h"
#import "FRequestMessageSend.h"
#import "FChatEngine.h"
#import "MessageFactory.h"


#define UD_KEY @"BackgroundSender_key_1"
#define UD_KEY_ID @"BackgroundSender_key_ID"

@implementation BackgroundSender

+(BackgroundSender *)shared
{
    static BackgroundSender *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        queueList = [NSMutableArray new];
        [self restore];
        [self initReachability];
        [self send];
    }
    return self;
}

-(void)setChat:(Chat *)c
{
    chat = c;
    [self send];
}

-(void)initReachability
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
}

- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    if ([curReach isKindOfClass:[Reachability class]] && curReach == reachability && curReach.currentReachabilityStatus != NotReachable)
        [self send];
}

-(void)queueInfo:(BackgroundSenderInfo *)info
{
//    info.backgroundSenderId = [self generateId];
    [queueList addObject:info];
    [self store];
    [self send];
}

-(void)removeInfoAtId:(NSUInteger)senderId
{
    BackgroundSenderInfo *infoForRemove;
    for (BackgroundSenderInfo *info in queueList)
        if (info.backgroundSenderId == senderId)
        {
            infoForRemove = info;
            break;
        }
    if (infoForRemove)
    {
        [queueList removeObject:infoForRemove];
        [self store];
    }
}

-(NSInteger)generateId
{
    NSUserDefaults *uDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger generatedId = [uDefaults integerForKey:UD_KEY_ID];
    generatedId++;
    [uDefaults setInteger:generatedId forKey:UD_KEY_ID];
    [uDefaults synchronize];
    return generatedId;
}


-(void)send
{
    if (!chat)
        return;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @synchronized(self) {
            //NSLog(@"BackgroundSender: START LOADING");
            _loading = YES;
            NSArray *sendList = [NSArray arrayWithArray:queueList];
            
            for (BackgroundSenderInfo *info in sendList)
            {
                switch (info.type)
                {
                    case BITMessage:
                    {
                        Message *message = [[FChatEngine shared] messageWithId:@""
                                                                       localId:info.backgroundSenderId
                                                                       context:[RBase shared].moc];
                        FRequestMessageSend *request = [FRequestMessageSend new];
                        request.backgroundSenderId = info.backgroundSenderId;
                        request.chat = chat;
                        request.text = message.text;
                        request.parent = [FChatEngine shared];
                        [request start];
                        break;
                    }
                    case BITImage:
                    case BITVideo:
                    case BITAudio:
                    {
                        Message *message = [[FChatEngine shared] messageWithId:@""
                                                                       localId:info.backgroundSenderId
                                                                       context:[RBase shared].moc];
                        [[FChatEngine shared] sendFileMessage:message
                                                   completion:^(BOOL success) {
                                                       if (success)
                                                           [self removeInfoAtId:info.backgroundSenderId];
                                                   }];
                        break;
                    }
                }
            }
            //NSLog(@"BackgroundSender: END LOADING");
            _loading = NO;
            [self store];
            
        }
    });
}

-(void)store
{
    NSUserDefaults *uDefaults = [NSUserDefaults standardUserDefaults];
    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:queueList];
    [uDefaults setObject:myEncodedObject forKey:UD_KEY];
    [uDefaults synchronize];
}

-(void)restore
{
    NSUserDefaults *uDefaults = [NSUserDefaults standardUserDefaults];
    NSData *myDecodedObject = [uDefaults objectForKey: UD_KEY];
    if (myDecodedObject)
    {
        NSArray *tmp = [NSKeyedUnarchiver unarchiveObjectWithData: myDecodedObject];
        queueList = [NSMutableArray arrayWithArray:tmp];
    }
    
}

-(void)dealloc
{
    [self store];
    [reachability stopNotifier];
}

@end
