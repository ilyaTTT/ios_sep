//
//  User.m
//  
//
//  Created by user on 16.07.15.
//
//

#import "User.h"
#import "File.h"

@implementation User

@dynamic name;
@dynamic sex;
@dynamic birthday;
@dynamic urlLarge;
@dynamic urlSmall;
@dynamic userId;
@dynamic role;
@dynamic info;
@dynamic phone;
@dynamic paidAccess;
@dynamic paidUntil;

static User *user;
+ (User *)currentUser
{
    if (user == nil)
    {
        NSString *string = [self currUserId];
        //NSLog(@"%@",[self currUserId]);
        if ([self currUserId].length > 0)
        {
            user = [self userWithId:[self currUserId] context:[RBase shared].moc];
        }
    }
    return user;
}

+ (void)clean
{
    user = nil;
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"kUserId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (User *)userWithId:(NSString *)userId context:(NSManagedObjectContext *)context
{
    return [[RBase shared] objectsForEntity:@"User" withPredicate:[NSPredicate predicateWithFormat:@"userId=%@",userId] context:context].lastObject;
}

+ (void)setCurrUser:(NSDictionary *)dict
{
    NSManagedObjectContext *context = [RBase shared].moc;
    User *user = [User currentUser];
    
    [self setCurrUserId:dict[@"id"]];
    if (user == nil)
    {
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    }
    [user fillWithDict:dict];
    
    [[RBase shared] saveMain];
}

- (void)fillWithDict:(NSDictionary *)dict
{
    NSManagedObjectContext *context = [RBase shared].moc;
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    self.userId = stringValue(dict[@"id"]);
    self.birthday = [formatter dateFromString:dict[@"birthday"]];
    self.urlLarge = dict[@"avatar_large_url"];
    self.urlSmall = dict[@"avatar_small_url"];
    self.sex = dict[@"sex"];
    self.name = dict[@"name"];
    
    if (dict[@"role"])
        self.role = dict[@"role"];
    
    if (dict[@"info"])
        self.info = dict[@"info"];
    
    if (dict[@"phone"])
        self.phone = dict[@"phone"];
    
    if (dict[@"paid_access"])
        self.paidAccess = [dict[@"paid_access"] boolValue];
    
    if (dict[@"paid_untill"])
        self.paidUntil = [formatter dateFromString:dict[@"paid_untill"]];
    
    if (self.urlLarge.length > 0)
    {
        if (self.photo == nil)
        {
            self.photo = [NSEntityDescription insertNewObjectForEntityForName:@"File" inManagedObjectContext:context];
            self.photo.url = self.urlLarge;
        }
        else
            if (![self.photo.url isEqualToString:user.urlLarge])
            {
                self.photo.url = self.urlLarge;
                [self.photo clear];
            }
    }
    if (self.urlSmall.length > 0)
    {
        if (self.smallPhoto == nil)
        {
            self.smallPhoto = [NSEntityDescription insertNewObjectForEntityForName:@"File" inManagedObjectContext:context];
            self.smallPhoto.url = self.urlSmall;
        }
        else
            if (![user.smallPhoto.url isEqualToString:user.urlSmall])
            {
                self.smallPhoto.url = self.urlSmall;
                [self.smallPhoto clear];
            }
    }

}

+ (void)setCurrUserId:(NSString *)token
{
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"kUserId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)currUserId
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"kUserId"];
}


@end
