//
//  Question.m
//  FitnessChat
//
//  Created by user on 17.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "Question.h"
#import "Anketa.h"
#import "Replies.h"


@implementation Question

@dynamic questionId;
@dynamic type;
@dynamic name;
@dynamic answer;
@dynamic anketa;
@dynamic replies;

- (NSString *)createAnswer
{
    if ([self.type isEqualToString:@"text"])
    {
        return self.answer;
    }
    else
    {
        NSArray *replies = [self checkedReplies];
        NSMutableString *res = [NSMutableString new];
        for (Replies *reply in replies)
        {
            [res appendFormat:@"%@\n",reply.text];
        }
        return res;
    }
}

- (Replies *)replyForId:(NSString *)replyId
{
    for (Replies *reply in self.replies)
    {
        if ([reply.repliesId isEqualToString:replyId])
        {
            return reply;
        }
    }
    return nil;
}
- (NSArray *)checkedReplies
{
    NSArray *comp = [self.answer componentsSeparatedByString:@";"];
    NSMutableArray *res = [NSMutableArray new];
    for (NSString *replyId in comp)
    {
        if (replyId.length > 0)
        {
            Replies *reply = [self replyForId:replyId];
            if (reply)
            {
                [res addObject:reply];
            }
        }
    }
    return res;

}
@end
