//
//  Anketa.h
//  FitnessChat
//
//  Created by user on 17.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Question;

@interface Anketa : NSManagedObject

@property (nonatomic, retain) NSNumber * anketaId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *questions;

@end


@interface Anketa (CoreDataGeneratedAccessors)

- (void)addQuestionsObject:(Question *)value;
- (void)removeQuestionsObject:(Question *)value;
- (void)addQuestions:(NSSet *)values;
- (void)removeQuestions:(NSSet *)values;

- (void)fillWithDict:(NSDictionary *)dict;
@end
