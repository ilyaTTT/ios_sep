//
//  PaymentService.h
//  FitnessChat
//
//  Created by owel on 07/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Subscription : NSObject

@property (strong, nonatomic) NSString  *subscriptionId;
@property (strong, nonatomic) NSString  *subscriptionDescription;
@property (assign, nonatomic) NSInteger subscriptionCost;
@property (strong, nonatomic) NSString  *subscriptionDuration;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;


@end
