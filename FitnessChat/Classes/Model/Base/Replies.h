//
//  Replies.h
//  FitnessChat
//
//  Created by user on 17.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Question;

@interface Replies : NSManagedObject

@property (nonatomic, retain) NSString * repliesId;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) Question *question;

@end
