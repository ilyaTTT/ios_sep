//
//  RBase.m
//  RGS
//
//  Created by Алексей on 29.04.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "RBase.h"
#import "Anketa.h"
#import "Replies.h"
#import "Question.h"


@implementation RBase

+ (RBase *)shared
{
    static RBase *base = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        if (base == nil)
        {
            base = [RBase new];
        }
    });
    
    return base;
}

- (void)fillReplies:(NSDictionary *)dict
{
    NSManagedObjectContext *context = [[RBase shared] backgroundMOC];
    
    [context performBlock:^{
        NSDictionary *anketasDict = dict[@"user_replies"];
        
        if (![anketasDict isKindOfClass:[NSDictionary class]]) return ;
        
        [anketasDict enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSDictionary *anketaDict, BOOL *stop)
        {
            NSNumber *anketaId = numberValue(key);
            Anketa *anketa = [self objectsForEntity:@"Anketa" withPredicate:[NSPredicate predicateWithFormat:@"anketaId=%@",anketaId] context:context].lastObject;
            if (anketa != nil)
            {
                [anketaDict enumerateKeysAndObjectsUsingBlock:^(NSString *kkey, NSDictionary *questDict, BOOL *stop)
                {
                    NSNumber *questId = numberValue(kkey);
                    Question *quest = [self objectsForEntity:@"Question" withPredicate:[NSPredicate predicateWithFormat:@"questionId=%@ and anketa=%@",questId,anketa] context:context].lastObject;
                    if (quest != nil)
                    {
                        if ([quest.type isEqualToString:@"text"])
                        {
                            quest.answer = questDict[@"text"];
                        }
                        else
                            if ([quest.type isEqualToString:@"select_one"])
                            {
                                quest.answer = stringValue(questDict[@"reply_id"]);
                            }
                        else
                        {
                            NSArray *replies = questDict[@"reply_ids"];
                            NSMutableString *string = [NSMutableString new];
                            for (NSString *replyId in replies)
                            {
                                [string appendString:replyId];
                                [string appendString:@";"];
                            }
                            quest.answer = string;
                        }
                    }
                    
                }];
                
            }
        }];
        
        [context save:nil];
    }];
}


- (NSManagedObjectContext *)moc
{
    return [AppDelegate shared].managedObjectContext;
}

- (NSManagedObjectContext *)backgroundMOC
{
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc]
                                   initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    moc.parentContext = self.moc;

    return moc;
}

- (NSArray *)objectsForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:entity];
    request.predicate = predicate;
    
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:request error:&error];
    if (error == nil)
    {
        return result;
    }
    else
    {
        return nil;
    }
}

- (void)saveContext:(NSManagedObjectContext *)context
{
    NSError *error;
    if (![context save:&error])
    {
        //NSLog(@"Base ERRRORR !!! %@",error);
    }
    
    [self.moc performBlock:^{
        NSError *error;
        if (![self.moc save:&error])
        {
            //NSLog(@"Main Base ERRRORR !!! %@",error);
        }
    }];
    
}

- (void)saveMain
{
    [self.moc performBlock:^{
        NSError *error;
        if (![self.moc save:&error])
        {
            //NSLog(@"Main Base ERROR! %@",error);
        }
    }];
}
@end

id stringValue(id value)
{
    if ([value isKindOfClass:[NSNull class]])
    {
        return nil;
    }
    else
        if ([value isKindOfClass:[NSString class]])
        {
            return value;
        }
    else
    {
        return [value stringValue];
    }
}

BOOL boolValue(id value)
{
    if ([value isKindOfClass:[NSNull class]])
    {
        return NO;
    }
    else
    {
        return [value boolValue];
    }
}

NSNumber *numberValue(id value)
{
    if ([value isKindOfClass:[NSNull class]])
    {
        return [NSNumber numberWithInt:0];
    }
    else
    {
        return [NSNumber numberWithInt:[value intValue]];
    }
}

NSNumber *numberFloatValue(id value)
{
    if ([value isKindOfClass:[NSNull class]])
    {
        return [NSNumber numberWithFloat:0];
    }
    else
    {
        return [NSNumber numberWithFloat:[value floatValue]];
    }
}
