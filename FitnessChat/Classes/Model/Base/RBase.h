//
//  RBase.h
//  RGS
//
//  Created by Алексей on 29.04.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>


NSString *stringValue(id value);
BOOL boolValue(id value);
NSNumber *numberValue(id value);
NSNumber *numberFloatValue(id value);

@interface RBase : NSObject
+ (RBase *)shared;

- (void)fillReplies:(NSDictionary *)dict;

- (NSArray *)objectsForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context;

- (NSManagedObjectContext *)backgroundMOC;
- (NSManagedObjectContext *)moc;

- (void)saveMain;
- (void)saveContext:(NSManagedObjectContext *)context;
@end
