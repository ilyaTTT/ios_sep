//
//  User.h
//  
//
//  Created by user on 16.07.15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class File;

@interface User : NSManagedObject

+ (User *)currentUser;
+ (void)setCurrUser:(NSDictionary *)dict;
+ (void)clean;
- (void)fillWithDict:(NSDictionary *)dict;

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * sex;
@property (nonatomic, retain) NSDate * birthday;
@property (nonatomic, retain) NSString * urlLarge;
@property (nonatomic, retain) NSString * urlSmall;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * role;
@property (nonatomic, retain) NSString * info;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) File * photo;
@property (nonatomic, retain) File * smallPhoto;
@property (nonatomic, assign) BOOL      paidAccess;
@property (nonatomic, retain) NSDate   * paidUntil;
 
@end
