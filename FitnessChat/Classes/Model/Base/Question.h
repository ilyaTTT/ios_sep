//
//  Question.h
//  FitnessChat
//
//  Created by user on 17.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Anketa, Replies;

@interface Question : NSManagedObject

@property (nonatomic, retain) NSNumber * questionId;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * answer;
@property (nonatomic, retain) Anketa *anketa;
@property (nonatomic, retain) NSSet *replies;

- (NSString *)createAnswer;
- (NSArray *)checkedReplies;
@end

@interface Question (CoreDataGeneratedAccessors)

- (void)addRepliesObject:(Replies *)value;
- (void)removeRepliesObject:(Replies *)value;
- (void)addReplies:(NSSet *)values;
- (void)removeReplies:(NSSet *)values;

@end
