//
//  PaymentService.m
//  FitnessChat
//
//  Created by owel on 07/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import "Subscription.h"


@implementation Subscription

-(instancetype) initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    
    self.subscriptionId             = dictionary[@"id"];
    self.subscriptionDescription    = dictionary[@"desc"];
    self.subscriptionCost           = [dictionary[@"price"] integerValue];
    self.subscriptionDuration       = dictionary[@"duration_text"];
    
    return self;
}

@end
