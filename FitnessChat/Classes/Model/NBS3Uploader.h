//
//  NBS3Uploader.h
//  Numbuster
//
//  Created by Алексей on 22.11.13.
//  Copyright (c) 2013 Алексей. All rights reserved.
//

@class File;

typedef void(^NBS3UploaderProgress)(NSInteger total, NSInteger current);
@interface NBS3Uploader : NSObject

@property (nonatomic) float uploadProgressCounter;

+ (NBS3Uploader *)uploader;
- (void)uploadFile:(File *)file;
- (void)uploadFile:(File *)file completion:(void (^)(BOOL success))completion;
-(void)cancelUpload;

- (void)exportFile:(File *)file;
- (void)exportFile:(File *)file completion:(void (^)(BOOL succeess))completion;
- (void)exportFile:(File *)file completion:(void (^)(BOOL succeess))completion progress: (NBS3UploaderProgress)progress;

@end
