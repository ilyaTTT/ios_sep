//
//  RCore.m
//  RGS
//
//  Created by Алексей on 29.04.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "RCore.h"
#import "FRequestUserGet.h"


NSString *stringTime(int time)
{
    int min = (int)time / 60;
    int sec = (int)time % 60;
    NSString *minStr;
    NSString *secStr;
    if (min < 10)
    {
        minStr = [NSString stringWithFormat:@"0%d",min];
    }
    else
    {
        minStr = [NSString stringWithFormat:@"%d",min];
    }
    
    if (sec < 10)
    {
        secStr = [NSString stringWithFormat:@"0%d",sec];
    }
    else
    {
        secStr = [NSString stringWithFormat:@"%d",sec];
    }
    
    return [NSString stringWithFormat:@"%@:%@",minStr,secStr];
}


@interface RCore () <NWRequestDelegate>

@property (strong, nonatomic) NSMutableDictionary *queue;

@end


@implementation RCore

+ (RCore *)shared
{
    static RCore *core = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        if (core == nil){
            core = [RCore new];
            core.queue = [NSMutableDictionary new];
        }
    });
    
    return core;
}

- (void)setToken:(NSString *)token
{
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"kToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)token
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"kToken"];
}

- (void)loadUser:(NSString *)userId withCompletion:(void (^)(User *user))completion
{
    User *user = [[[RBase shared] objectsForEntity:@"User" withPredicate:[NSPredicate predicateWithFormat:@"userId=%@",userId] context:[RBase shared].moc] lastObject];
    if (user != nil)
    {
        completion(user);
    }
    else
    {
        if (_queue[userId] != nil)
        {
            NSMutableArray *arr = _queue[userId];
            [arr addObject:completion];
            [_queue setObject:arr forKey:userId];
        }
        else
        {
            NSMutableArray *arr = [NSMutableArray new];
            [arr addObject:completion];
            [_queue setObject:arr forKey:userId];
            
            FRequestUserGet *request = [FRequestUserGet new];
            request.userId = userId;
            request.parent = self;
            [request start];
        }
    }
}


- (void)requestCompletion:(NWRequest *)request withObject:(NSDictionary *)dict andError:(NSError *)error
{
    if ([request isKindOfClass:[FRequestUserGet class]])
    {
        NSString *userId = ((FRequestUserGet *)request).userId;
        User *user;
        NSArray *arr = _queue[userId];
        [_queue removeObjectForKey:userId];
        
        if ([dict[@"result"] isEqualToString:@"ok"])
        {
            
            user = [[[RBase shared] objectsForEntity:@"User" withPredicate:[NSPredicate predicateWithFormat:@"userId=%@",userId] context:[RBase shared].moc] lastObject];
            if (user == nil)
            {
                user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:[RBase shared].moc];
                user.userId = userId;
            }
            [user fillWithDict:dict[@"userInfo"]];
            [[RBase shared] saveMain];
            for (void(^completion)(User *user) in arr)
            {
                completion(user);
            }
        }
    }

}
@end
