//
//  MessageFactory.h
//  FitnessChat
//
//  Created by Михаил Кузеванов on 27.04.16.
//  Copyright © 2016 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageFactory : NSObject

+(Message *)createMessageAtImageData:(NSData *)imageDara andChat: (Chat *)chat;
+(Message *)createMessageAtText:(NSString *)text andChat: (Chat *)chat;
+(Message *)createMessageAtVideoData:(NSData *)videoData andChat:(Chat *)chat;
+(Message *)createMessageAtAudioData:(NSData *)audioData andChat:(Chat *)chat andTime: (NSInteger)time;
@end
