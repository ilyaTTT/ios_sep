//
//  XActionSheet.m
//  lookChecker
//
//  Created by x2 on 3/3/14.
//  Copyright (c) 2014 5craft. All rights reserved.
//

#import "XActionSheet.h"


@implementation XActionSheet

- (void)setStrongDelegate:(XActionSheetDelegate *)strongDelegate
{
    _strongDelegate = strongDelegate;
    self.delegate = strongDelegate;
}

@end
