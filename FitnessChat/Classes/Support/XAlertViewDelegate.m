//
//  XAlertViewDelegate.m
//  lookChecker
//
//  Created by x2 on 3/3/14.
//  Copyright (c) 2014 5craft. All rights reserved.
//

#import "XAlertViewDelegate.h"

@implementation XAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if (self.successTextBlock) self.successTextBlock([alertView textFieldAtIndex:0].text);
        return;
    }
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    if (alertView.alertViewStyle != UIAlertViewStylePlainTextInput) return YES;
    return self.validateBlock?self.validateBlock([alertView textFieldAtIndex:0].text):YES;
}

@end
