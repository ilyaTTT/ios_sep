//
//  SAditional.h
//  Selfie
//
//  Created by Рудаков Алексей Михайлович on 24.07.14.
//  Copyright (c) 2014 Alexei Rudakov. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString * LString(NSString * key);
NSString * LStringForNumber(NSString * key, int number);


UIImage * cropScaleImageToSize(UIImage * image, CGSize size);

@interface UIButton (Additional)
- (void)addTarget:(id)target action:(SEL)selector;
- (void)setTitle:(NSString *)title;
- (void)setTitleColor:(UIColor *)color;
- (void)setBackgroundImage:(UIImage *)image;
- (void)setImageForAllNames:(NSString *)name;
- (void)setImage:(UIImage *)image;


- (void)setBackgroundAllimageWithName:(NSString *)name;

- (void)centerVerticallyWithPadding:(float)padding;
- (void)centerVertically;
- (void)centerVerticallyWithPaddingInverse:(float)padding;
- (void)centerVerticallyInverse;

@end

@interface UITextField (Additional)
- (void)setPlaceholder:(NSString *)placeholder withColor:(UIColor *)color;
- (void)addLeftIndent:(CGFloat)leftIndent;

@end

@interface UILabel (Additional)

-(void) adjustFontSizeToFillItsContents;
//- (id)new;

@end


