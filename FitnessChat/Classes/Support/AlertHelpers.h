//
//  AlertHelpers.h
//  lookChecker
//
//  Created by x2 on 3/3/14.
//  Copyright (c) 2014 5craft. All rights reserved.
//

#import "Common.h"

typedef void (^FailureImageBlock)();
typedef void (^SuccessBlock)();
typedef void (^SuccessVideoBlock)(NSData * video);
typedef void (^SuccessImageBlock)(UIImage * image);
typedef void (^SuccessTextBlock)(NSString * text);
typedef void (^SuccessIntBlock)(NSInteger option);
typedef BOOL (^ValidateBlock)(NSString * text);
typedef void (^SuccessSocialBlock)(NSInteger network);

@interface AlertHelpers : NSObject

+ (void)showTextAlertViewWithTitle:(NSString *)title defaultText:(NSString *)defaultText onSuccess:(SuccessTextBlock)onSuccess validate:(ValidateBlock)validate;
+ (void)showMailAlertViewWithTitle:(NSString *)title defaultText:(NSString *)defaultText onSuccess:(SuccessTextBlock)onSuccess validate:(ValidateBlock)validate;
+ (void)showPassAlertViewWithTitle:(NSString *)title defaultText:(NSString *)defaultText onSuccess:(SuccessTextBlock)onSuccess validate:(ValidateBlock)validate;
+ (void)showTextCreationAlertViewWithTitle:(NSString *)title onSuccess:(SuccessTextBlock)onSuccess validate:(ValidateBlock)validate;

+ (void)showImagePickerFromVC:(UIViewController *)vc usingTabbar:(BOOL)useTabbar onSuccess:(SuccessImageBlock)onSuccess;
+ (void)showImagePickerFromVC:(UIViewController *)vc usingTabbar:(BOOL)useTabbar onSuccess:(SuccessImageBlock)onSuccess onFailure:(FailureImageBlock)onFailure;
+ (void)showSocialSelectorWithTitle:(NSString *)title fromVC:(UIViewController *)vc usingTabbar:(BOOL)useTabbar onSuccess:(SuccessSocialBlock)onSuccess;
+ (void)showAbuseActionSheetFromVC:(UIViewController *)vc onSuccess:(SuccessBlock)onSuccess;

@end
