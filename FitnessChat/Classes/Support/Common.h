//
//  Common.h
//  gingy2
//
//  Created by x2 on 10/24/12.
//
//

#ifndef APP_CONFIGURED

#define APP_CONFIGURED

#define DEBUGGING true
#define DEBUG_API true

#define CLOUD_PAYMENT_PUBLIC_ID @"pk_0fe41ec381ff9747fb9a0283b8d14"
#define CLOUD_PAYMENT_SECRET_ID @"7b7d01a729ff418da09ee396e7abcca4"

#define NDM_NOTIFICATION_UPDATE_PROFILE     @"NDM_NOTIFICATION_UPDATE_PROFILE"
#define NDM_NOTIFICATION_RELOAD_CHAT        @"NDM_NOTIFICATION_RELOAD_CHAT"
#define NDM_NOTIFICATION_READED_CHANGED      @"NDM_NOTIFICATION_READED_CHANGED"

#define DAY_IN_SECONDS 86400

#define USER_IMAGE_SIZE     CGSizeMake(320,320)
#define USER_BG_IMAGE_SIZE  CGSizeMake(640,576)
#define LOOK_IMAGE_SIZE     CGSizeMake(640,640)

#ifdef DEBUG
#define DBGLog(...) //NSLog(__VA_ARGS__)
#else
#define DBGLog(...) do {} while(0)
#endif

#endif


BOOL isIPad();
BOOL isIOS7();

NSString * stringFromCountString(NSString * string, NSInteger count);
NSString * stringFromStringsString(NSString * string);

NSString * dateCuteStringForDate(NSDate * date);

NSDate * getModificationDateForFileAtPath(NSString * path);

NSString * hexStringFromData(NSData * data);

BOOL isEmailValid(NSString * email);

NSString * safeString(__unsafe_unretained id object);
NSNumber * safeNumber(__unsafe_unretained id object);
NSArray * safeArray(__unsafe_unretained id object);
NSDictionary * safeDictionary(__unsafe_unretained id object);

UIImage * cropNScaleImageToSize(UIImage * image, CGSize size);
