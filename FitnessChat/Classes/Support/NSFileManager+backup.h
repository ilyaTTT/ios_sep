//
//  NSFileManager+backup.h
//  CocktailApp
//
//  Created by user on 31.08.15.
//  Copyright (c) 2015 bnet.su. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (backup)
+ (void)addSkipBackupAttributeToAllAtItemPath:(NSString *)path;
+ (BOOL)addSkipBackupAttributeToItemPath:(NSString *)path;

@end
