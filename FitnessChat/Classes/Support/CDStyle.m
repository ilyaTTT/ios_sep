//
//  CDStyle.m
//  CookerDemo
//
//  Created by Рудаков Алексей Михайлович on 08.07.14.
//  Copyright (c) 2014 Алексей. All rights reserved.
//

#import "CDStyle.h"

@implementation CDStyle

+ (NSAttributedString *)buildAttributedStrings:(NSArray *)strings fonts:(NSArray *)fonts
{
    NSMutableAttributedString *string = [NSMutableAttributedString new];
    [strings enumerateObjectsUsingBlock:^(NSString *str, NSUInteger idx, BOOL *stop) {
        UIFont *font = fonts[idx];
        NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName:font}];
        [string appendAttributedString:attrStr];
    }];
    
    return string;
}

+ (NSAttributedString *)buildAttributedStrings:(NSArray *)strings fonts:(NSArray *)fonts cololrs:(NSArray *)colors
{
    NSMutableAttributedString *string = [NSMutableAttributedString new];
    [strings enumerateObjectsUsingBlock:^(NSString *str, NSUInteger idx, BOOL *stop) {
        UIFont *font = fonts[idx];
        UIColor *color = colors[idx];
        NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName:font, NSForegroundColorAttributeName:color}];
        [string appendAttributedString:attrStr];
    }];

    return string;
}

@end

@implementation UIColor (RTheme)
+ (UIColor *)blue
{
    return [UIColor colorWith256Red:87 green:161 blue:184];
}
+ (UIColor *)gray
{
    return [UIColor colorWithWhite:103/255.0 alpha:1];
}
+ (UIColor *)black
{
    return [UIColor blackColor];
}
+ (UIColor *)red
{
    return [UIColor colorWith256Red:171 green:0 blue:0];
}
+ (UIColor *)patternColor
{
    return [UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar_pattern"]];
}
+ (UIColor *)darkOrange
{
    return [UIColor colorWithRed:255/255.0f green:131/255.0f blue:70/255.0f alpha:1.0f];
}
+ (UIColor *)grayNavBar
{
    return [UIColor colorWithRed:217/255.0f green:217/255.0f blue:217/255.0f alpha:1.0f];
}
+ (UIColor *)blueNavBarText
{
    return [UIColor colorWithRed:84/255.0f green:110/255.0f blue:127/255.0f alpha:1.0f];
}
+ (UIColor *)lightColor
{
    return [UIColor colorWithRed:242/255.0f green:242/255.0f blue:242/255.0f alpha:0.9f];
}
+ (UIColor *)lightColor2
{
    return [UIColor colorWithRed:229/255.0f green:230/255.0f blue:232/255.0f alpha:1.0f];
}
+ (UIColor *)lightColor3
{
    return [UIColor colorWithRed:202/255.0f green:206/255.0f blue:209/255.0f alpha:1.0f];
}
+ (UIColor *)blueTextColor
{
    return [UIColor colorWithRed:41/255.0f green:81/255.0f blue:106/255.0f alpha:1.0f];
}
+ (UIColor *)blueDeepNavBar
{
    return [UIColor colorWithRed:11/255.0f green:76/255.0f blue:159/255.0f alpha:1.0f];
}
+ (UIColor *)blueDarkColor
{
    return [UIColor colorWithRed:4/255.0f green:51/255.0f blue:111/255.0f alpha:1.0f];
}
+ (UIColor *)blueGradient1
{
    return [UIColor colorWithRed:32/255.0f green:104/255.0f blue:194/255.0f alpha:1.0f];
}
+ (UIColor *)blueGradient2
{
    return [UIColor colorWithRed:16/255.0f green:91/255.0f blue:183/255.0f alpha:1.0f];
}
+ (UIColor *)blueGradient3
{
    return [UIColor colorWithRed:5/255.0f green:66/255.0f blue:145/255.0f alpha:1.0f];
}
+ (UIColor *)blueGradient4
{
    return [UIColor colorWithRed:7/255.0f green:62/255.0f blue:135/255.0f alpha:1.0f];
}
+ (UIColor *)blueGradient5
{
    return [UIColor colorWithRed:16/255.0f green:53/255.0f blue:105/255.0f alpha:1.0f];
}
+ (UIColor *)unsleepPhaseBg
{
    return [UIColor colorWithRed:3/255.0f green:40/255.0f blue:87/255.0f alpha:1.0f];
}
+ (UIColor *)lightPhaseBg
{
    return [UIColor colorWithRed:253/255.0f green:250/255.0f blue:39/255.0f alpha:1.0f];
}
+ (UIColor *)deepPhaseBg
{
    return [UIColor colorWithRed:40/255.0f green:177/255.0f blue:255/255.0f alpha:1.0f];
}
+ (UIColor *)blueGrayColor
{
    return [UIColor colorWithRed:118/255.0f green:156/255.0f blue:188/255.0f alpha:1.0f];
}
+ (UIColor *)blueActiveColor
{
    return [UIColor colorWithRed:61/255.0f green:124/255.0f blue:189/255.0f alpha:1.0f];
}

+ (UIColor *)grayTop
{
    return [UIColor colorWithRed:233/255.0f green:236/255.0f blue:231/255.0f alpha:1.0f];
}

+ (UIColor *)grayBottom
{
    return [UIColor colorWithRed:216/255.0f green:223/255.0f blue:239/255.0f alpha:1.0f];
}

+ (UIColor *)azure
{
    return [UIColor colorWithRed:64/255.0f green:178/255.0f blue:238/255.0f alpha:1.0f];
}
+ (UIColor *)brown
{
    return [UIColor colorWithRed:202/255.0f green:154/255.0f blue:116/255.0f alpha:1.0f];
}
@end

@implementation UIFont (RTheme)

+ (UIFont *)thin17
{
    return [UIFont fontWithName:@"HelveticaNeue-Thin" size:17];
}
+ (UIFont *)thin10
{
    return [UIFont fontWithName:@"HelveticaNeue-Thin" size:10];
}

+ (UIFont *)thin36
{
    return [UIFont fontWithName:@"PFDinTextCondPro-Thin" size:18];
}

+ (UIFont *)thin24
{
    return [UIFont fontWithName:@"HelveticaNeue-Thin" size:24];
}



+ (UIFont *)light:(int)size
{
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:size];
}

+ (UIFont *)medium:(int)size
{
    return [UIFont fontWithName:@"HelveticaNeue-Medium" size:size];
}

+ (UIFont *)bold:(int)size
{
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:size];
}

+ (UIFont *)regular:(int)size
{
    return [UIFont fontWithName:@"HelveticaNeue" size:size];
}


+ (UIFont *)mini
{
    return [UIFont fontWithName:@"PFDinTextCondPro" size:10];
}

+ (UIFont *)micro
{
    return [UIFont fontWithName:@"PFDinTextCondPro" size:8];
}

+ (UIFont *)mimicro
{
    return [UIFont fontWithName:@"Helvetica" size:7];
}

+ (UIFont *)normal
{
    return [UIFont fontWithName:@"HelveticaNeue" size:17];
}

+ (UIFont *)big
{
    return [UIFont fontWithName:@"PFDinTextCondPro" size:16];
}

+ (UIFont *)normalBold
{
    return [UIFont fontWithName:@"PFDinTextCondPro-Bold" size:12];
}

+ (UIFont *)miniBold
{
    return [UIFont fontWithName:@"PFDinTextCondPro-Bold" size:10];
}

+ (UIFont *)bigBold
{
    return [UIFont fontWithName:@"PFDinTextCondPro-Bold" size:16];
}

+ (UIFont *)bigThin
{
    return [UIFont fontWithName:@"PFDinTextCondPro-Light" size:16];
}

+ (UIFont *)regular24
{
    return [UIFont fontWithName:@"PFDinTextCondPro-Regular" size:12];
}

+ (UIFont *)regular32
{
    return [UIFont fontWithName:@"PFDinTextCondPro-Regular" size:16];
}

+ (UIFont *)regular72
{
    return [UIFont fontWithName:@"PFDinTextCondPro-Regular" size:36];
}

+ (UIFont *)header
{
    return [UIFont fontWithName:@"PFDinTextCondPro-Regular" size:24];
}
@end

@implementation  UIImage (Additional)

+ (UIImage *)imageActiveNamed:(NSString *)name
{
    return [UIImage imageNamed:[NSString stringWithFormat:@"%@_active",name]];
}

+ (UIImage *)imageHighlitedNamed:(NSString *)name
{
    return [UIImage imageNamed:[NSString stringWithFormat:@"%@_highlited",name]];
}

+ (UIImage *)imageHighlitedNormalNamed:(NSString *)name
{
    return [UIImage imageNamed:[NSString stringWithFormat:@"%@_highlited_normal",name]];
}

+ (UIImage *)imageDisabledNamed:(NSString *)name
{
    return [UIImage imageNamed:[NSString stringWithFormat:@"%@_disabled",name]];
}

+ (UIImage *)imageCrossNamed:(NSString *)name
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        return [UIImage imageNamed:[NSString stringWithFormat:@"%@_ipad",name]];
    }
    return [UIImage imageNamed:name];
}

@end