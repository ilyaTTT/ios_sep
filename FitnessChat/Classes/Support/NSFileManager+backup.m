//
//  NSFileManager+backup.m
//  CocktailApp
//
//  Created by user on 31.08.15.
//  Copyright (c) 2015 bnet.su. All rights reserved.
//

#import "NSFileManager+backup.h"

@implementation NSFileManager (backup)

+ (void)addSkipBackupAttributeToAllAtItemPath:(NSString *)path
{
    
    NSArray  *yourFolderContents = [[NSFileManager defaultManager]
                                    contentsOfDirectoryAtPath:path error:nil];
    
    for (NSString *p in yourFolderContents)
    {
        [self addSkipBackupAttributeToItemPath:[path stringByAppendingPathComponent:p]];
    }
}

+ (BOOL)addSkipBackupAttributeToItemPath:(NSString *)path
{
    NSURL *URL = [NSURL fileURLWithPath:path];//[NSURL URLWithString:path];
    //    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        //NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

@end
