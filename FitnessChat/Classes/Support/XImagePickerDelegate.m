//
//  XImagePickerDelegate.m
//  lookChecker
//
//  Created by x2 on 3/3/14.
//  Copyright (c) 2014 5craft. All rights reserved.
//

#import "XImagePickerDelegate.h"

@implementation XImagePickerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.failureImageBlock();
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage * backButtonImage = [UIImage imageNamed: @"back_button"];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage: backButtonImage forState: UIControlStateNormal barMetrics: UIBarMetricsDefault];

    
    if (self.successImageBlock) self.successImageBlock([info objectForKey:UIImagePickerControllerEditedImage]);
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

@end
