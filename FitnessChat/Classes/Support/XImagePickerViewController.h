//
//  XImagePickerViewController.h
//  lookChecker
//
//  Created by x2 on 3/3/14.
//  Copyright (c) 2014 5craft. All rights reserved.
//

#import "XImagePickerDelegate.h"

@interface XImagePickerViewController : UIImagePickerController

@property (nonatomic, strong) XImagePickerDelegate * strongDelegate;

@end
