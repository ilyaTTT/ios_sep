//
//  XImagePickerViewController.m
//  lookChecker
//
//  Created by x2 on 3/3/14.
//  Copyright (c) 2014 5craft. All rights reserved.
//

#import "XImagePickerViewController.h"

@implementation XImagePickerViewController

- (void)setStrongDelegate:(XImagePickerDelegate *)strongDelegate
{
    _strongDelegate = strongDelegate;
    self.delegate = strongDelegate;
}

@end
