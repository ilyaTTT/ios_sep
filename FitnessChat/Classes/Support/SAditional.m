//
//  SAditional.m
//  Selfie
//
//  Created by Рудаков Алексей Михайлович on 24.07.14.
//  Copyright (c) 2014 Alexei Rudakov. All rights reserved.
//

#import "SAditional.h"



@implementation UIButton (Additional)

- (void)addTarget:(id)target action:(SEL)selector
{
    [self addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
}

- (void)setTitle:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
}

- (void)setTitleColor:(UIColor *)color
{
    [self setTitleColor:color forState:UIControlStateNormal];
}

- (void)setBackgroundImage:(UIImage *)image
{
    [self setBackgroundImage:image forState:UIControlStateNormal];
}

- (void)setBackgroundAllimageWithName:(NSString *)name
{
    [self setBackgroundImage:[UIImage imageNamed:name]];
    [self setBackgroundImage:[UIImage imageActiveNamed:name] forState:UIControlStateSelected];
    [self setBackgroundImage:[UIImage imageHighlitedNamed:name] forState:UIControlStateHighlighted | UIControlStateSelected];
    [self setBackgroundImage:[UIImage imageHighlitedNormalNamed:name] forState:UIControlStateHighlighted | UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageDisabledNamed:name] forState:UIControlStateDisabled];
}

- (void)setImageForAllNames:(NSString *)name
{
    [self setImage:[UIImage imageNamed:name]];
    [self setImage:[UIImage imageActiveNamed:name] forState:UIControlStateSelected];
    [self setImage:[UIImage imageHighlitedNamed:name] forState:UIControlStateHighlighted];
    [self setImage:[UIImage imageDisabledNamed:name] forState:UIControlStateDisabled];
    [self setImage:[UIImage imageHighlitedNamed:name] forState:UIControlStateHighlighted | UIControlStateSelected];
}

- (void)setImage:(UIImage *)image
{
    [self setImage:image forState:UIControlStateNormal];
}


- (void)centerVerticallyWithPadding:(float)padding
{
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = self.titleLabel.frame.size;
    
    CGFloat totalHeight = (imageSize.height + titleSize.height + padding);
    
    self.imageEdgeInsets = UIEdgeInsetsMake(- (totalHeight - imageSize.height),
                                            0.0f,
                                            0.0f,
                                            - titleSize.width);
    
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0f,
                                            - imageSize.width,
                                            - (totalHeight - titleSize.height),
                                            0.0f);
    
}


- (void)centerVertically
{
    const CGFloat kDefaultPadding = 6.0f;
    
    [self centerVerticallyWithPadding:kDefaultPadding];
}

- (void)centerVerticallyWithPaddingInverse:(float)padding
{
//    [self sizeToFit];
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = self.titleLabel.frame.size;
    
    CGFloat totalHeight = (imageSize.height + titleSize.height + padding);
    
//    self.width = MAX(imageSize.width,titleSize.width);
//    self.height = totalHeight;
    
    self.imageEdgeInsets = UIEdgeInsetsMake(0,
                                            0.0f,
                                            - (totalHeight - imageSize.height),
                                            - titleSize.width);
    
    self.titleEdgeInsets = UIEdgeInsetsMake(- (totalHeight - titleSize.height),
                                            - imageSize.width,
                                            0,
                                            0.0f);
}


- (void)centerVerticallyInverse
{
    const CGFloat kDefaultPadding = 6.0f;
    
    [self centerVerticallyWithPaddingInverse:kDefaultPadding];
}

@end


#define CATEGORY_DYNAMIC_FONT_SIZE_MAXIMUM_VALUE 35
#define CATEGORY_DYNAMIC_FONT_SIZE_MINIMUM_VALUE 3

@implementation UILabel (Additional)
- (void)adjustFontSizeToFillItsContents
{
    NSString* text = self.text;
    
    for (int i = CATEGORY_DYNAMIC_FONT_SIZE_MAXIMUM_VALUE; i>CATEGORY_DYNAMIC_FONT_SIZE_MINIMUM_VALUE; i--) {
        
        UIFont *font = [UIFont fontWithName:self.font.fontName size:(CGFloat)i];
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: font}];
        
        CGRect rectSize = [attributedText boundingRectWithSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        
        if (rectSize.size.height <= self.frame.size.height) {
            self.font = [UIFont fontWithName:self.font.fontName size:(CGFloat)i];
            break;
        }
    }
    
}

@end

@implementation UITextField (Additional)
- (void)setPlaceholder:(NSString *)placeholder withColor:(UIColor *)color
{
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: color}];
}

- (void)addLeftIndent:(CGFloat)leftIndent
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, leftIndent, self.height)];
    self.leftView = view;
    self.leftViewMode = UITextFieldViewModeAlways;
}

@end

 NSString * LString(NSString * key)
{
    return NSLocalizedString(key, nil);
}

 NSString * LStringForNumber(NSString * key, int number)
{
    NSInteger iNumber = number % 100;
    NSString* res;
    if (iNumber >= 11 && iNumber <= 19)
    {
        res = @"other";
    }
    else
    {
        switch (number % 10)
        {
            case (1):
                res = @"one";
                break;
                
            case (2):
            case (3):
            case (4):
                res = @"few";
                break;
                
            default:
                res = @"other";
        }
    }
    
    return LString([NSString stringWithFormat:@"%@<%@>", key, res]);
}

UIImage * cropScaleImageToSize(UIImage * image, CGSize size)
{
    CGFloat k = MAX( (size.width / image.size.width), (size.height / image.size.height) );
    CGRect drawRect = CGRectMake((size.width-image.size.width*k)/2, (size.height-image.size.height*k)/2, image.size.width*k, image.size.height*k);
    
    UIGraphicsBeginImageContext(size);
    [image drawInRect:drawRect];
    UIImage * result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}