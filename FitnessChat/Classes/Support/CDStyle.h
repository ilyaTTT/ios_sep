//
//  CDStyle.h
//  CookerDemo
//
//  Created by Рудаков Алексей Михайлович on 08.07.14.
//  Copyright (c) 2014 Алексей. All rights reserved.
//

@interface CDStyle : NSObject
+ (NSAttributedString *)buildAttributedStrings:(NSArray *)strings fonts:(NSArray *)fonts;
+ (NSAttributedString *)buildAttributedStrings:(NSArray *)strings fonts:(NSArray *)fonts cololrs:(NSArray *)colors;

@end

@interface UIColor (RTheme)
+ (UIColor *)gray;
+ (UIColor *)black;
+ (UIColor *)red;
+ (UIColor *)blue;
+ (UIColor *)patternColor;
+ (UIColor *)darkOrange;
+ (UIColor *)grayNavBar;
+ (UIColor *)blueNavBarText;
+ (UIColor *)lightColor;
+ (UIColor *)lightColor2;
+ (UIColor *)lightColor3;
+ (UIColor *)blueTextColor;
+ (UIColor *)blueDeepNavBar;
+ (UIColor *)blueDarkColor;
+ (UIColor *)blueGradient1;
+ (UIColor *)blueGradient2;
+ (UIColor *)blueGradient3;
+ (UIColor *)blueGradient4;
+ (UIColor *)blueGradient5;
+ (UIColor *)unsleepPhaseBg;
+ (UIColor *)lightPhaseBg;
+ (UIColor *)deepPhaseBg;
+ (UIColor *)blueGrayColor;
+ (UIColor *)blueActiveColor;
+ (UIColor *)grayTop;
+ (UIColor *)grayBottom;
+ (UIColor *)azure;
+ (UIColor *)brown;
@end

@interface UIFont (RTheme)
+ (UIFont *)mimicro;
+ (UIFont *)mini;
+ (UIFont *)micro;
+ (UIFont *)normal;
+ (UIFont *)big;

+ (UIFont *)normalBold;
+ (UIFont *)miniBold;
+ (UIFont *)bigBold;

+ (UIFont *)bigThin;

+ (UIFont *)thin10;
+ (UIFont *)thin17;
+ (UIFont *)thin24;
+ (UIFont *)thin36;


+ (UIFont *)light:(int)size;
+ (UIFont *)medium:(int)size;
+ (UIFont *)bold:(int)size;
+ (UIFont *)regular:(int)size;

+ (UIFont *)regular24;
+ (UIFont *)regular32;
+ (UIFont *)regular72;

+ (UIFont *)header;
@end

@interface UIImage (Additional)
+ (UIImage *)imageActiveNamed:(NSString *)name;
+ (UIImage *)imageHighlitedNamed:(NSString *)name;
+ (UIImage *)imageHighlitedNormalNamed:(NSString *)name;
+ (UIImage *)imageDisabledNamed:(NSString *)name;

+ (UIImage *)imageCrossNamed:(NSString *)name;

@end
