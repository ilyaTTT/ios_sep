//
//  Common.m
//  airdodge2
//
//  Created by x2 on 10/24/12.
//
//

#import "Common.h"

BOOL _isSettedUp;
BOOL _isIPad;
BOOL _isIOS7;

NSString * stringFromCountString(NSString * string, NSInteger count)
{
    NSArray * parts = [string componentsSeparatedByString:@"|"];
    if (parts.count > 2)//Russian-way
    {
        if (count%10 == 1 && count%100 != 11) return [parts objectAtIndex:0];
        if (count%10 > 1 && count%10 < 5 && ((count%100 > 15)||(count%100 < 10))) return [parts objectAtIndex:1];
        return parts.lastObject;
    }
    else//English-way
    {
        if (count == 1) return [parts objectAtIndex:0];
        return parts.lastObject;
    }
}

NSString * stringFromStringsString(NSString * string)
{
    NSArray * parts = [string componentsSeparatedByString:@"|"];
    return [parts objectAtIndex:rand()%parts.count];
}

NSString * dateCuteStringForDate(NSDate * date)
{
    NSTimeInterval interval = -[date timeIntervalSinceNow];
    
    if (interval < DAY_IN_SECONDS) return @"сегодня";
    if (interval < DAY_IN_SECONDS+DAY_IN_SECONDS) return @"вчера";
    
    return [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];
}


NSDate * getModificationDateForFileAtPath(NSString * path)
{
    NSFileManager* fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:path]) return nil;
    
    NSDictionary* attrs = [fm attributesOfItemAtPath:path error:nil];
    if (!attrs) return nil;
    
    return (NSDate*)[attrs objectForKey: NSFileModificationDate];
}

void _setup()
{
    if (_isSettedUp) return;
    _isSettedUp = YES;
    _isIPad = UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad;
    _isIOS7 = UIDevice.currentDevice.systemVersion.floatValue >= 7;
}

BOOL isIPad()
{
    _setup();
    return _isIPad;
}

BOOL isIOS7()
{
    _setup();
    return _isIOS7;
}


NSString * hexStringFromData(NSData * data)
{
    unichar* hexChars = (unichar*)malloc(sizeof(unichar) * (data.length*2));
    unsigned char* bytes = (unsigned char*)data.bytes;
    for (NSUInteger i = 0; i < data.length; i++) {
        unichar c = bytes[i] / 16;
        if (c < 10) c += '0';
        else c += 'A' - 10;
        hexChars[i*2] = c;
        c = bytes[i] % 16;
        if (c < 10) c += '0';
        else c += 'A' - 10;
        hexChars[i*2+1] = c;
    }
    NSString* retVal = [[NSString alloc] initWithCharactersNoCopy:hexChars length:data.length*2 freeWhenDone:YES];
    return retVal;
}

BOOL isEmailValid(NSString * email)
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];

    
//    NSString * pattern = @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
//    NSRegularExpression * expression = [[NSRegularExpression alloc] initWithPattern:pattern options:0 error:nil];
//    return [expression numberOfMatchesInString:email options:NSMatchingAnchored range:NSMakeRange(0, email.length)] > 0;
}

static inline id safeObject(__unsafe_unretained Class class, __unsafe_unretained id object)
{
	if (object && ![object isKindOfClass:class]) return nil;
	return object;
}

NSString * safeString(__unsafe_unretained id object)
{
	return safeObject([NSString class], object);
}

NSNumber * safeNumber(__unsafe_unretained id object)
{
	return safeObject([NSNumber class], object);
}

NSArray * safeArray(__unsafe_unretained id object)
{
	return safeObject([NSArray class], object);
}

NSDictionary * safeDictionary(__unsafe_unretained id object)
{
	return safeObject([NSDictionary class], object);
}

UIImage * cropNScaleImageToSize(UIImage * image, CGSize size)
{
    CGFloat k = MAX( (size.width / image.size.width), (size.height / image.size.height) );
    CGRect drawRect = CGRectMake((size.width-image.size.width*k)/2, (size.height-image.size.height*k)/2, image.size.width*k, image.size.height*k);
    
    UIGraphicsBeginImageContext(size);
    [image drawInRect:drawRect];
    UIImage * result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

