//
//  XImagePickerDelegate.h
//  lookChecker
//
//  Created by x2 on 3/3/14.
//  Copyright (c) 2014 5craft. All rights reserved.
//

#import "AlertHelpers.h"

@interface XImagePickerDelegate : NSObject <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (copy) SuccessImageBlock successImageBlock;
@property (copy) FailureImageBlock failureImageBlock;

@end
