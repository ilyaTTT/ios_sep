//
//  AlertHelpers.m
//  lookChecker
//
//  Created by x2 on 3/3/14.
//  Copyright (c) 2014 5craft. All rights reserved.
//

#import "AlertHelpers.h"
#import "XAlertView.h"
#import "XImagePickerViewController.h"
#import "XActionSheet.h"
#import "AppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>


#import <MobileCoreServices/UTCoreTypes.h>

@implementation AlertHelpers

+ (XAlertView *)prepareAlertViewWithTitle:(NSString *)title defaultText:(NSString *)defaultText buttonTitles:(NSArray *)buttonTitles
                                onSuccess:(SuccessTextBlock)onSuccess validate:(ValidateBlock)validate
{
    NSAssert(buttonTitles.count > 0, @"You better have at least one button");
    
    XAlertViewDelegate * helperDelegate = [XAlertViewDelegate new];
    helperDelegate.successTextBlock = onSuccess;
    helperDelegate.validateBlock = validate;
    
    XAlertView * alert = [XAlertView new];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.title = title;
    for (NSString * buttonTitle in buttonTitles) [alert addButtonWithTitle:buttonTitle];
    [alert setCancelButtonIndex:0];
    alert.strongDelegate = helperDelegate;
    [[alert textFieldAtIndex:0] setText:defaultText];
    return alert;
}

+ (void)showTextAlertViewWithTitle:(NSString *)title defaultText:(NSString *)defaultText onSuccess:(SuccessTextBlock)onSuccess validate:(ValidateBlock)validate
{
    XAlertView * alert = [self prepareAlertViewWithTitle:title defaultText:defaultText buttonTitles:@[ @"Отменить", @"Изменить" ]
                                               onSuccess:onSuccess validate:validate];
    [[alert textFieldAtIndex:0] setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    [alert show];
}

+ (void)showMailAlertViewWithTitle:(NSString *)title defaultText:(NSString *)defaultText onSuccess:(SuccessTextBlock)onSuccess validate:(ValidateBlock)validate
{
    XAlertView * alert = [self prepareAlertViewWithTitle:title defaultText:defaultText buttonTitles:@[ @"Отменить", @"Изменить" ]
                                               onSuccess:onSuccess validate:validate];
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeEmailAddress];
    [alert show];
}

+ (void)showPassAlertViewWithTitle:(NSString *)title defaultText:(NSString *)defaultText onSuccess:(SuccessTextBlock)onSuccess validate:(ValidateBlock)validate
{
    XAlertView * alert = [self prepareAlertViewWithTitle:title defaultText:defaultText buttonTitles:@[ @"Отменить", @"Изменить" ]
                                               onSuccess:onSuccess validate:validate];
    alert.alertViewStyle = UIAlertViewStyleSecureTextInput;
    [alert show];
}

+ (void)showTextCreationAlertViewWithTitle:(NSString *)title onSuccess:(SuccessTextBlock)onSuccess validate:(ValidateBlock)validate
{
    XAlertView * alert = [self prepareAlertViewWithTitle:title defaultText:@"" buttonTitles:@[ @"Отменить", @"Создать" ]
                                               onSuccess:onSuccess validate:validate];
    [[alert textFieldAtIndex:0] setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    [alert show];
}

+ (void)showImagePickerFromVC:(UIViewController *)vc usingTabbar:(BOOL)useTabbar onSuccess:(SuccessImageBlock)onSuccess onFailure:(FailureImageBlock)onFailure
{
    XActionSheetDelegate * actionSheetDelegate = [XActionSheetDelegate new];
    actionSheetDelegate.successIntBlock = ^(NSInteger option) {
        if (option == 2)
        {
            onFailure();
            return;//cancel
        }
        
        
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:nil forState: UIControlStateNormal barMetrics: UIBarMetricsDefault];
        XImagePickerDelegate * helperDelegate = [XImagePickerDelegate new];
        helperDelegate.successImageBlock = onSuccess;
        helperDelegate.failureImageBlock = onFailure;
        
        XImagePickerViewController * imagePicker = [XImagePickerViewController new];
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        if (option == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.strongDelegate = helperDelegate;
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [vc presentViewController:imagePicker animated:YES completion:nil];
        }];
    };
    
    XActionSheet * actionSheet = [XActionSheet new];
    actionSheet.strongDelegate = actionSheetDelegate;
    [actionSheet setTitle:@"Выберите источник"];
    [actionSheet addButtonWithTitle:@"Снять фото"];
    [actionSheet addButtonWithTitle:@"Выбрать фото"];
    [actionSheet addButtonWithTitle:@"Отменить"];
    [actionSheet setCancelButtonIndex:2];
    
    [actionSheet showInView:vc.view];
}

+ (void)showImagePickerFromVC:(UIViewController *)vc usingTabbar:(BOOL)useTabbar onSuccess:(SuccessImageBlock)onSuccess
{
    XActionSheetDelegate * actionSheetDelegate = [XActionSheetDelegate new];
    actionSheetDelegate.successIntBlock = ^(NSInteger option) {
        if (option == 2) return;//cancel
        
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:nil forState: UIControlStateNormal barMetrics: UIBarMetricsDefault];
        XImagePickerDelegate * helperDelegate = [XImagePickerDelegate new];
        helperDelegate.successImageBlock = onSuccess;
        
        XImagePickerViewController * imagePicker = [XImagePickerViewController new];
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        if (option == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.strongDelegate = helperDelegate;
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [vc presentViewController:imagePicker animated:YES completion:nil];
        }];
    };
    
    XActionSheet * actionSheet = [XActionSheet new];
    actionSheet.strongDelegate = actionSheetDelegate;
    [actionSheet setTitle:@"Выберите источник"];
    [actionSheet addButtonWithTitle:@"Снять фото"];
    [actionSheet addButtonWithTitle:@"Выбрать фото"];
    [actionSheet addButtonWithTitle:@"Отменить"];
    [actionSheet setCancelButtonIndex:2];

     [actionSheet showInView:vc.view];
}


+ (void)showSocialSelectorWithTitle:(NSString *)title fromVC:(UIViewController *)vc usingTabbar:(BOOL)useTabbar onSuccess:(SuccessSocialBlock)onSuccess
{
    //Unused method...
}


+ (void)showAbuseActionSheetFromVC:(UIViewController *)vc onSuccess:(SuccessBlock)onSuccess
{
    XActionSheetDelegate * actionSheetDelegate = [XActionSheetDelegate new];
    actionSheetDelegate.successIntBlock = ^(NSInteger option) {
        if (option == 1) return;//cancel
        if (option == 0 && onSuccess) onSuccess();
    };
    
    XActionSheet * actionSheet = [[XActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:@"Отменить" destructiveButtonTitle:@"Пожаловаться" otherButtonTitles:nil];
    actionSheet.strongDelegate = actionSheetDelegate;
    [actionSheet showInView:vc.view];
}

@end
