//
//  XActionSheetDelegate.m
//  lookChecker
//
//  Created by x2 on 3/3/14.
//  Copyright (c) 2014 5craft. All rights reserved.
//

#import "XActionSheetDelegate.h"


@implementation XActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.successIntBlock) self.successIntBlock(buttonIndex);
}

@end
