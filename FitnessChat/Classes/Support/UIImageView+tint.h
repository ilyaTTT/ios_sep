//
//  UIImageView+tint.h
//  centrallo
//
//  Created by Kirill Pyulzyu on 26.02.14.
//  Copyright (c) 2014 Centrallo, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (tint)

-(void)tintWithColor:(UIColor *)color;
@end

@interface UIButton (tint)
-(void)tintWithColor:(UIColor *)color;

@end
