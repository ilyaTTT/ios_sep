//
//  UIImageView+tint.m
//  centrallo
//
//  Created by Kirill Pyulzyu on 26.02.14.
//  Copyright (c) 2014 Centrallo, LLC. All rights reserved.
//

#import "UIImageView+tint.h"

@implementation UIImageView (tint)

-(void)tintWithColor:(UIColor *)color
{
    UIImage *image;
    if (color)
    {
        image = [self.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.image = image;
        [self setTintColor:color];
    }
    else
    {
        image = [self.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.image = image;
        [self setTintColor:nil];
    }
}

@end

@implementation UIButton (tint)

-(void)tintWithColor:(UIColor *)color
{
    UIImage *image;
    if (color)
    {
        image = [self.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self setTintColor:color];
    }
    else
    {
        image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [self setTintColor:nil];
    }
    [self setImage:image forState:UIControlStateNormal];
    
}

@end
