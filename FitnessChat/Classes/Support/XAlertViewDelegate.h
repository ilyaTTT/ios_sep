//
//  XAlertViewDelegate.h
//  lookChecker
//
//  Created by x2 on 3/3/14.
//  Copyright (c) 2014 5craft. All rights reserved.
//

#import "AlertHelpers.h"

@interface XAlertViewDelegate : NSObject <UIAlertViewDelegate>

@property (copy) SuccessTextBlock successTextBlock;
//@property (copy) CancelBlock cancelBlock;
@property (copy) ValidateBlock validateBlock;

@end
