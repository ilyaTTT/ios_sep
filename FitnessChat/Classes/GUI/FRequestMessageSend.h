//
//  FRequestMessageSend.h
//  FitnessChat
//
//  Created by user on 24.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "NWRequest.h"

@interface FRequestMessageSend : NWRequest
@property (strong, nonatomic) Chat *chat;
@property (strong, nonatomic) NSString *text;

@end
