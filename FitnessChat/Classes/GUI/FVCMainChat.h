//
//  FVCMainChat.h
//  FitnessChat
//
//  Created by user on 14.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVC.h"


@interface FVCMainChat : FVC

@property (nonatomic) BOOL isConsultant;
@property (nonatomic) BOOL openChat;

@end
