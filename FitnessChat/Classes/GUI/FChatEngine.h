//
//  FChatEngine.h
//  FitnessChat
//
//  Created by user on 22.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//


#import "NWRequestDelegate.h"


typedef void (^XBlock)();

@interface FChatEngine : NSObject <NWRequestDelegate>

+ (FChatEngine *)shared;
- (void)loadMessagesForChat:(Chat *)chat withCompletion:(XBlock)completion;
- (void)sendFileMessage:(Message *)message;

- (Message *)messageWithId:(NSString *)messageId localId: (NSInteger)localId context:(NSManagedObjectContext *)moc;

@property BOOL shouldQuitLoop;
- (void)sendFileMessage:(Message *)message completion:(void (^)(BOOL success))completion;

@end
