
//
//  FChatEngine.m
//  FitnessChat
//
//  Created by user on 22.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FChatEngine.h"
#import "FRequestMessages.h"
#import "FRequestMessageSend.h"
#import "FRequestMessageFileSend.h"
#import "FRequestUpdate.h"
#import "File.h"
#import "FVCContainer.h"
#import "Common.h"


@interface FChatEngine ()

@property (nonatomic) int timestamp;

@end


@implementation FChatEngine

+ (FChatEngine *)shared
{
    static FChatEngine *engine = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        if (engine == nil)
        {
            engine = [FChatEngine new];
            [engine start];
        }
    });
    
    return engine;
}


- (void)start
{
    FRequestUpdate *request = [FRequestUpdate new];
    request.parent = self;
    [request start];
}

/** Send message. */
- (void)sendFileMessage:(Message *)message
{
    //NSLog(@"1 - File BEING uploaded.");
    [[NBS3Uploader uploader] uploadFile:message.file completion:^(BOOL success) {
        //NSLog(@"2 - File uploaded!");
        FRequestMessageFileSend *request = [FRequestMessageFileSend new];
        request.parent = self;
        request.message = message;
        [request start];
    }];
}

/** Just like the method 'sendFileMessage', this method sends a file, however it also contains a completion handler. */
-(void)sendFileMessage:(Message *)message completion:(void (^)(BOOL success))completion
{
    _shouldQuitLoop = NO;
    [NBS3Uploader uploader].uploadProgressCounter += 0.142857143;//1;
    [[NBS3Uploader uploader] uploadFile:message.file completion:^(BOOL success) {
        if (_shouldQuitLoop == NO)
        {
            //NSLog(@"_shouldQuitLoop %i", _shouldQuitLoop);
            FRequestMessageFileSend *request = [FRequestMessageFileSend new];
            request.parent = self;
            request.message = message;
            [request start];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if(completion)
                    [NBS3Uploader uploader].uploadProgressCounter += 0.142857143;//1;
                completion(success);
            });
        }
        else
        {
            //NSLog(@"_shouldQuitLoop %i", _shouldQuitLoop);
            dispatch_async(dispatch_get_main_queue(), ^{
                if(completion)
                    completion(success);
            });
        }
        
    }];
}

/** Load message. */
- (void)loadMessagesForChat:(Chat *)chat withCompletion:(XBlock)completion
{
    FRequestMessages *request = [FRequestMessages new];
    request.parent = self;
    request.chat = chat;
    [request start];
}


/** ACCEPTS SERVER DATA & places them into Core Data objects. But the changes HAVEN'T been saved yet! */
- (void)requestCompletion:(NWRequest *)request withObject:(NSDictionary *)dict andError:(NSError *)error
{
    if ([request isKindOfClass:[FRequestMessageFileSend class]])
    {
        Message *message = ((FRequestMessageFileSend *)request).message;
        if ([dict[@"result"] isEqualToString:@"ok"])
        {
            message.messageId   = dict[@"updates"][0][@"body"][@"id"];
            message.sending     = NO;
            message.file.url    = dict[@"updates"][0][@"body"][@"url"];
            [[RBase shared] saveMain];
            [[NSNotificationCenter defaultCenter] postNotificationName:NDM_NOTIFICATION_READED_CHANGED
                                                                object:message.messageId];
        }
        else
        {
//            [[RBase shared].moc deleteObject:message];
            [[RBase shared] saveMain];
            if (![request isKindOfClass:[FRequestUpdate class]])
                [AppDelegate.shared showErrors:dict[@"errors"]];
        }
        return;
    }
    else
    if ([request isKindOfClass:[FRequestMessageSend class]])
    {
        if ([dict[@"result"] isEqualToString:@"ok"])
        {
            NSDictionary *updates = dict[@"updates"][0];
            NSDictionary *body = updates[@"body"];
            NSString *messageId = body[@"id"];
            [self fillMessage:updates context:[RBase shared].moc];
            [[RBase shared] saveMain]; //Saves
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:NDM_NOTIFICATION_READED_CHANGED
                                                                object:messageId];
        }
        else
        {
            [AppDelegate.shared showErrors:dict[@"errors"]];
        }
        return;
    }
    
    if ([dict[@"result"] isEqualToString:@"ok"])
    {
       
    }
    else
    {
        if (![request isKindOfClass:[FRequestUpdate class]])
        {
            [AppDelegate.shared showErrors:dict[@"errors"]];
            return;
        }
        
    }

    NSManagedObjectContext *moc = [[RBase shared] backgroundMOC];
    
    if ([request isKindOfClass:[FRequestMessages class]])
    {
        NSArray *messageDicts = dict[@"updates"];
        
        [moc performBlock:^{
            for (NSDictionary *messageDict in messageDicts)
            {
                if (![messageDict[@"body"][@"type"] isEqualToString:@"system"])
                {
                    [self fillMessage:messageDict context:moc];
                }
                else if ([messageDict[@"body"][@"command"] isEqualToString:@"goto_payment"])
                {
                    //NSLog(@"System message! Hey ho!");
                    [self fillGotoMessage:messageDict context:moc];
                }
                else {
                    ;
                }
            }
            
            [[RBase shared] saveContext:moc]; //Saves Core Data managed objects.
        }];
    }
    else
        if ([request isKindOfClass:[FRequestUpdate class]])
        {
            NSArray *readedMessages = dict[@"readedMessages"];
            for (NSDictionary *dictReadedMessage in readedMessages)
            {
                BOOL readed = [dictReadedMessage[@"readed"] boolValue];
                NSString *messageId = dictReadedMessage[@"_id"];
                if (readed)
                    [[NSNotificationCenter defaultCenter] postNotificationName:NDM_NOTIFICATION_READED_CHANGED
                                                                        object:messageId];
            }
            NSArray *messageDicts = dict[@"updates"];
            
            NSManagedObjectContext *moc = [RBase shared].moc; //Saves Core Data managed objects.
            [moc performBlockAndWait:^{
                
                NSString *lastConsultantId;
                
                for (NSDictionary *messageDict in messageDicts)
                {
                    if (![messageDict[@"body"][@"type"] isEqualToString:@"system"])
                    {
                        [self fillMessage:messageDict context:moc];
                    }
                    else if ([messageDict[@"body"][@"command"] isEqualToString:@"goto_payment"])
                    {
//                        //NSLog(@"System message! Hey ho!");
                        [self fillGotoMessage:messageDict context:moc];
                    }
                    else if ([messageDict[@"body"][@"command"] isEqualToString:@"consultant_change"]){
//                        [[FVCContainer shared] reloadData];
//                        [[FVCContainer shared] updateConsultant:messageDict[@"body"][@"room_id"]];
                        NSString *consultantId = messageDict[@"body"][@"consultant_id"];
                        if (consultantId != nil)
                            lastConsultantId = consultantId;
//                        //NSLog(@"%@", messageDict);
                    }
                }
                
                if (lastConsultantId != nil)
                    [[NSNotificationCenter defaultCenter] postNotificationName:NDM_NOTIFICATION_RELOAD_CHAT object:lastConsultantId];
                
                if (!error)
                {
                    [[RBase shared] saveMain];
                    [[RBase shared] saveContext:moc]; //Saves Core Data managed objects.
                }
                 //WHEN SAVED CORE DATA EXCEPTION APPEARS.
            }];

//            //NSLog(@"UPDATE : %@",dict);
            FRequestUpdate *request = [FRequestUpdate new];
            request.timestamp       = self.timestamp;
            request.parent          = self;
            [request start];
        }
}


- (void)fillGotoMessage:(NSDictionary *)dict context:(NSManagedObjectContext *)moc
{
    NSDictionary *body  = dict[@"body"];
    //NSLog(@"\nfillGotoMessage body:\n%@\n", body);
    NSString *messageId = body[@"id"];
    NSString *chatId    = body[@"room_id"];
    
    Chat *chat = [self chatWithId:chatId context:moc];
    Message *message = [self messageWithId:messageId localId:0 context:moc];
    if (message.chat == nil)
    {
        if (!message.deleted)
        {
            [chat addMessagesObject:message];
        }
    }
    int timestamp   = [dict[@"timestamp"] intValue];
    self.timestamp  = timestamp;
    message.date    = [NSDate dateWithTimeIntervalSince1970:timestamp];
    message.userId  = body[@"user_id"];
//    if ([body[@"type"] isEqualToString:@"text"])
    if ([body[@"type"] isEqualToString:@"system"])
    {
        if ([body[@"command"] isEqualToString:@"goto_payment"])
        {
            message.paymentAmount = [body[@"payment_amount"] intValue]; //integerValue];
            
            NSString *mainText = body[@"text"];
//            NSString *subText = [NSString stringWithFormat:@"\nОплата подписки\n%@ ₽", dict[@"body"][@"payment_amount"]];
//            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[mainText stringByAppendingString:subText]];
            
            message.text = mainText;
            message.type = MESSAGE_GOTO;
        }
    }
}

- (void)fillMessage:(NSDictionary *)dict context:(NSManagedObjectContext *)moc
{
    NSDictionary *body = dict[@"body"];
    NSString *messageId = body[@"id"];
    NSInteger localId = [body[@"local_id"] integerValue];
    NSString *chatId = body[@"room_id"];
    
    //NSLog(@"\nfillMessage body:\n%@\n", body);
    
    Chat *chat = [self chatWithId:chatId context:moc];
    Message *message = [self messageWithId:messageId localId:localId context:moc];
    if (message.chat == nil)
    {
        if (!message.deleted)
        {
            [chat addMessagesObject:message];
        }
    }
    
    message.messageId = messageId;
    message.sending = NO;
    int timestamp = [dict[@"timestamp"] intValue];
    self.timestamp = timestamp;
    
    message.readed = [dict[@"readed"] boolValue];
    message.date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    message.userId = body[@"user_id"];
    if ([body[@"type"] isEqualToString:@"text"])
    {
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.apple.com"]];
        //NSString *str = body[@"text"];
        message.text = body[@"text"];
        message.type = MESSAGE_Text;
    }
    else //if ([body[@"type"] isEqualToString:@"image"])
    {
        File *file = message.file;
        if (file == nil)
        {
            file = [NSEntityDescription insertNewObjectForEntityForName:@"File" inManagedObjectContext:moc];
            NSString *url = body[@"url"];
            file.url = url;
            message.file = file;
        }
        
        if (body[@"filesize"] && ![body[@"filesize"] isKindOfClass:[NSNull class]])
            file.filesize = [body[@"filesize"] intValue];
        
        if ([body[@"type"] isEqualToString:@"image"])
        {
            message.file.type = FILE_Image;
            message.type = MESSAGE_Image;
        }
        else
            if ([body[@"type"] isEqualToString:@"video"])
            {
                message.file.type = FILE_Video;
                message.type = MESSAGE_Video;
            }
            else
                if ([body[@"type"] isEqualToString:@"audio"])
                {
                    message.file.type = FILE_Audio;
                    message.type = MESSAGE_Audio;
                }
                else
                    if ([body[@"type"] isEqualToString:@"audio_voice"])
                    {
                        message.file.type = FILE_AudioVoice;
                        message.type = MESSAGE_Audio_Voice;
                    }
    }
    
}


- (Chat *)chatWithId:(NSString *)chatId context:(NSManagedObjectContext *)moc
{
    Chat *chat = [[[RBase shared] objectsForEntity:@"Chat" withPredicate:[NSPredicate predicateWithFormat:@"chatId=%@",chatId] context:moc] lastObject];
    if (chat == nil)
    {
        chat = [NSEntityDescription insertNewObjectForEntityForName:@"Chat" inManagedObjectContext:moc];
        chat.chatId = chatId;
    }
    return chat;
}


- (Message *)messageWithId:(NSString *)messageId localId: (NSInteger)localId context:(NSManagedObjectContext *)moc
{
    Message *message;
    if (localId > 0)
    {
        message = [[[RBase shared] objectsForEntity:@"Message" withPredicate:[NSPredicate predicateWithFormat:@"localId=%d", localId] context:moc] lastObject];
    }
    if (!message)
        message = [[[RBase shared] objectsForEntity:@"Message" withPredicate:[NSPredicate predicateWithFormat:@"messageId=%@",messageId] context:moc] lastObject];
    
    if (message == nil)
    {
        message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:moc];
        message.messageId = messageId;
        message.localId = (int)localId;
    }
    return message;
}


static int _timestamp = -1;

- (int)timestamp
{
    if (_timestamp == -1)
    {
        _timestamp = [[[NSUserDefaults standardUserDefaults] objectForKey:@"kTimestamp"] intValue];
    }
    return _timestamp;
}

- (void)setTimestamp:(int)timestamp
{
    if (timestamp > _timestamp)
    {
        _timestamp = timestamp;
        [[NSUserDefaults standardUserDefaults] setObject:@(timestamp) forKey:@"kTimestamp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

@end
