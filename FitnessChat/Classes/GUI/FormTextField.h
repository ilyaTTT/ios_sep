//
//  FormTextField.h
//  RuRu
//
//  Created by Алексей on 21.05.14.
//  Copyright (c) 2014 Work. All rights reserved.
//

#import "AppearResignProtocol.h"

@class FormTextField;

static NSString *formPhoneMask = @"+7 (___) ___-__-__";
static NSString *formPhoneMask7 = @"+7 (___) ___-__-__";
static NSString *formPhoneMask375 = @"+375 (___) ___-__-__";
static NSString *formPhoneMask380 = @"+380 (___) ___-__-__";
static NSString *formCardMask = @"____ ____ ____ ____";
static NSString *formCardDateMask = @"__/__";
static NSString *formCardCVVMask = @"___";

@protocol FormTextFieldProtocol

-(void)formTextfieldFilled:(FormTextField *)textField;

@end


@interface FormTextField : UIView <AppearElementProtocol>

@property (strong, nonatomic) UITextField *textfield;
@property (assign, nonatomic) UIKeyboardType keyboardType;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *description;
@property (assign, nonatomic) NSInteger maxLength;
@property (strong, nonatomic) NSString *captchaUrlString;
@property (assign, nonatomic) id <FormTextFieldProtocol> textfieldDelegate;
@property (assign, nonatomic) id <AppearResignProtocol> appearDelegate;
@property (retain, nonatomic) NSString *prefix;
@property (assign, nonatomic) BOOL horizontalMode;
@property (strong, nonatomic) NSString *mask;
@property (strong, nonatomic) UILabel *titleLabel;
@property (assign, nonatomic) BOOL hasCardDetector;
@property (strong, nonatomic) UIView *bottomSeparator;

- (void)setCardDate:(NSString *)date;
- (BOOL)isFill;
- (CGFloat)heightForTextField;
- (void)setBottomSeparatorShow:(BOOL)isShow;

@end
