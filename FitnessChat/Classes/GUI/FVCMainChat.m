//
//  FVCMainChat.m
//  FitnessChat
//
//  Created by user on 14.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVCMainChat.h"
#import "FTabbar.h"
#import "FRequestUserConsiltant.h"
#import "FRequestUserGroup.h"
#import "FVCChat.h"

@interface FVCMainChat ()
{
    UIView *_photoBG;
    UIImageView *_photoView;
    UILabel *_titleLabel;
    UILabel *_descLabel;
    UIButton *_button;
    NSString *_chatId;
    NSString *_name;
    UILabel *_nLabel;
    UIView *_bottomView;
    bool once;
}
@end

@implementation FVCMainChat

- (void)viewDidLoad
{
    once = YES;
    [super viewDidLoad];
    
    CGFloat topHeight = 290;
    CGFloat bottomHeight = 165;
    CGFloat photoBGHeight = topHeight - 2 * 20;
    CGFloat photoHeight = photoBGHeight - 20;
    CGPoint photoCenter = CGPointMake(self.view.width / 2, topHeight / 2);
    
    _photoBG = [[UIView alloc] initWithFrame:CGRectMake(0, 0, photoBGHeight, photoBGHeight)];
    _photoBG.backgroundColor = [UIColor whiteColor];
    _photoBG.center = photoCenter;
    _photoBG.layer.cornerRadius = photoBGHeight / 2;
    _photoBG.layer.masksToBounds = YES;

    [self.view addSubview:_photoBG];
    
    _photoView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, photoHeight, photoHeight)];
    _photoView.center = photoCenter;
    _photoView.layer.cornerRadius = photoHeight / 2;
    _photoView.layer.masksToBounds = YES;
    _photoView.backgroundColor = [UIColor clearColor];

    _nLabel = [[UILabel alloc] initWithFrame:_photoView.frame];
    _nLabel.textAlignment = NSTextAlignmentCenter;
    _nLabel.textColor = [UIColor blue];
    _nLabel.font = [UIFont bold:80];
    [self.view addSubview:_nLabel];
    [self.view addSubview:_photoView];
    
    _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, topHeight, self.view.width, bottomHeight)];
    _bottomView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:_bottomView];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 20, _bottomView.width - 10, 25)];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.textColor = [UIColor blue];
    _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:24];
    [_bottomView addSubview:_titleLabel];
    
    _descLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, _titleLabel.bottom + 10, _bottomView.width - 10, 40)];
    _descLabel.textColor = [UIColor gray];
    _descLabel.numberOfLines = 2;
    _descLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _descLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    _descLabel.textAlignment = NSTextAlignmentCenter;
    [_bottomView addSubview:_descLabel];
    
    
    _button = [self roundedButton];
    [_button addTarget:self action:@selector(onChat)];
    _button.centerX = self.view.width / 2;
    _button.top = _descLabel.bottom + 15;
    [_button setTitle:@"Перейти в чат"];
    [_bottomView addSubview:_button];
    
    [self layout];
    
    [self setImage];
}

- (void)layout
{
    _descLabel.frame = CGRectMake(5, 0, self.view.width - 10, 70);
    
    _titleLabel.width = self.view.width - 30;
    [_titleLabel sizeToFit];
    [_descLabel sizeToFit];

    _titleLabel.top = 18;
    
    _descLabel.top = _titleLabel.bottom;
    
    _button.top = 96;
    _titleLabel.centerX = self.view.width / 2;
    _descLabel.centerX = self.view.width / 2;
}

- (void)setOpenChat:(BOOL)openChat
{
    return;
    NSDictionary *info = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"k%@",[self name]]];
    _chatId = info[@"chat_id"];
    NSManagedObjectContext *context = [RBase shared].moc;
    Chat *chat = [[[RBase shared] objectsForEntity:@"Chat" withPredicate:[NSPredicate predicateWithFormat:@"chatId=%@",_chatId] context:context] lastObject];
    if (chat == nil)
    {
        chat = [NSEntityDescription insertNewObjectForEntityForName:@"Chat" inManagedObjectContext:context];
        chat.chatId = _chatId;
        [context save:nil];
    }
    
    if (_isConsultant)
    {
        FVCChat *vc = [FVCChat new];
        vc.hidesBottomBarWhenPushed = YES;
        vc.title =_name;
        vc.chat = chat;
        [self.navigationController pushViewController:vc animated:NO];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadData];
    if (once)
    {
        once = NO;
        _bottomView.bottom = self.view.height - [FTabbar shared].tabBar.height;
    }
    
    // owel's temporary
    
}

- (void)reloadData
{
    [[AppDelegate shared] showActivity];
    
    if (_isConsultant)
    {
        FRequestUserConsiltant *request = [FRequestUserConsiltant new];
        request.parent = self;
        [request start];
    }
    else
    {
        FRequestUserGroup *request = [FRequestUserGroup new];
        request.parent = self;
        [request start];
    }
}

- (void)requestCompletion:(NWRequest *)request withObject:(NSDictionary *)dict andError:(NSError *)error
{
    NSDictionary *info;
    if (_isConsultant)
    {
        info = dict[@"consultantInfo"];
    }
    else
    {
        info = dict[@"groupInfo"];
    }
    if (![dict[@"result"] isEqualToString:@"ok"])
    {
        info = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"k%@",[self name]]];
    }
    if (info)
    {
        if (_isConsultant)
        {
            
            _chatId = info[@"chat_id"];
        }
        else
        {
            _chatId = info[@"id"];
        }
        _name = info[@"name"];
        _titleLabel.text = info[@"name"];
        _descLabel.text = info[@"info"];
        
        NSArray *comp = [_name componentsSeparatedByString:@" "];
        NSString *res = @"";
        for (int i = 0; i < comp.count; i ++)
        {
            NSString *c = comp[i];
            if (c.length > 0)
            {
                res = [res stringByAppendingString:[c substringToIndex:1]];
            }
        }
        _nLabel.text = res;
        
        NSString *url = stringValue(info[@"avatar_large_url"]);
        if (url.length > 0)
        {
            [self setImage:url];
        }
        else
        {
            _photoView.image = [UIImage imageNamed:@"temp"];
        }
        
        [self layout];
        _button.hidden = NO;
        _photoBG.hidden = NO;
        if (![info[@"avatar_large_url"] isKindOfClass:[NSString class]])
        {
            info = @{@"id":info[@"id"], @"name":info[@"name"],@"info":info[@"info"],@"avatar_large_url":@""};
        }
        [[NSUserDefaults standardUserDefaults] setObject:info forKey:[NSString stringWithFormat:@"k%@",[self name]]];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        _button.hidden = YES;
        _photoBG.hidden = YES;
        _titleLabel.text = @"Сообщество";
        _descLabel.text = @"Через некоторое время Ваш консультант добавит Вас в групповой чат";
        [self layout];
//        [AppDelegate.shared showErrors:dict[@"errors"]];
    }

//    [self onChat];     // owel's temporary (fixme)
    
    [[AppDelegate shared] hideActivity];
}

- (void)onChat
{
    NSManagedObjectContext *context = [RBase shared].moc;
    Chat *chat = [[[RBase shared] objectsForEntity:@"Chat" withPredicate:[NSPredicate predicateWithFormat:@"chatId=%@",_chatId] context:context] lastObject];
    if (chat == nil)
    {
        chat = [NSEntityDescription insertNewObjectForEntityForName:@"Chat" inManagedObjectContext:context];
        chat.chatId = _chatId;
        [context save:nil];
    }

    if (_isConsultant)
    {
        FVCChat *vc = [FVCChat new];
        vc.hidesBottomBarWhenPushed = YES;
        vc.title =_name;
        vc.chat = chat;
        [self.navigationController pushViewController:vc animated:NO];
    }
    else
    {
        FVCChatGroup *vc = [FVCChatGroup new];
        vc.hidesBottomBarWhenPushed = YES;
        vc.title = _name;
        vc.chat = chat;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (NSString *)name
{
    if (_isConsultant)
    {
        return @"consultant";
    }
    else
    {
        return @"group";
    }
}

- (void)setImage:(NSString *)newUrl
{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:newUrl]];
            if (data != nil)
            {
                [self saveImageData:data withName:[self name]];

                [[NSUserDefaults standardUserDefaults] synchronize];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self setImage];
                });
            }
        });
}

- (void)setImage
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *data = [self loadImageWithName:[self name]];
        if (data != nil)
        {
            UIImage *image = [UIImage imageWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{
                _photoView.image = image;
            });
        }
    });
}

- (void)saveImageData:(NSData*)data withName:(NSString *)name
{
    if (data != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        //        //NSLog(@"%@",documentsDirectory);
        NSString* path = [documentsDirectory stringByAppendingPathComponent:
                          name];

        if(![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory isDirectory:nil])
        {
            NSError * error = nil;
            [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                                      withIntermediateDirectories:YES
                                                       attributes:nil
                                                            error:&error];
            if (error != nil) {
                //NSLog(@"error creating directory: %@", error);
            }
        }
        
        if ([data writeToFile:path atomically:YES])
        {
            [NSFileManager addSkipBackupAttributeToItemPath:path];   
        }
    }
}

- (NSData *)loadImageWithName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      name];
    
    return [NSData dataWithContentsOfFile:path];
}

@end
