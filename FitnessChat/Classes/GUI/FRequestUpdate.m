//
//  FRequestUpdate.m
//  FitnessChat
//
//  Created by user on 24.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestUpdate.h"

@implementation FRequestUpdate

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"/rest/chat/online-updates?%@&from_timestamp=%d",[self accessTokenString],_timestamp];
}

- (NSString *)body
{
    return nil;
}

- (NSString *)method
{
    return @"GET";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}


@end
