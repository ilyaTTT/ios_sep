//
//  FVChatField.h
//  FitnessChat
//
//  Created by user on 24.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//
//  Creates the UI of the bottom bar: where the microphone, text field & photo/video button are displayed.

#import <UIKit/UIKit.h>


@protocol FVChatFieldProtocol

- (void)chatFieldMesageSend;
- (void)attachPressed;
- (void)dataRecored:(NSData *)data withTime:(int)time;
- (void)heightChangedIsEditing:(BOOL)editing;

@end


@interface FVChatField : UIView

@property (strong, nonatomic) NSString *text;
@property (weak, nonatomic) id <FVChatFieldProtocol>delegate;

@property (strong, nonatomic) UIButton      *sendButton;
@property (strong, nonatomic) UIView        *voice;
@property (strong, nonatomic) UIImageView   *voiceImageView;
@property (strong, nonatomic) UIButton      *attachButton; // '+' button that is located in bottom left (inside text field) - it allows the user to select a photo/video.

- (void)clean;

@end
