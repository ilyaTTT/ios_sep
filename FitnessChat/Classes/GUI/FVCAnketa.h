//
//  FVCAnketa.h
//  FitnessChat
//
//  Created by user on 21.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVC.h"

@class Anketa;

@interface FVCAnketa : FVC
@property (strong, nonatomic) Anketa *anketa;
@end
