//
//  FVCSubscriptionSuccess.m
//  FitnessChat
//
//  Created by owel on 09/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import "FVCSubscriptionSuccess.h"
#import "Common.h"


@interface FVCSubscriptionSuccess ()

//@property (strong, nonatomic) UIScrollView  *scrollView;
@property (strong, nonatomic) UIView        *mainView;
@property (strong, nonatomic) UIImageView   *topImage;
@property (strong, nonatomic) UILabel       *greetingsCaption;
@property (strong, nonatomic) UILabel       *greetingsText;
@property (strong, nonatomic) UIButton      *okButton;

@end


@implementation FVCSubscriptionSuccess

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.backgroundName = @"back_bg"; //@"back_register";
    
    _mainView = [UIView new];
    _mainView.width = self.view.width - 20;
    _mainView.centerX = self.view.width / 2;
    _mainView.top = 30;
    _mainView.backgroundColor = [UIColor whiteColor];
    _mainView.layer.cornerRadius = 10;
//    _mainView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    
    _topImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"big_checkmark"]];
    [_topImage sizeToFit];
    _topImage.centerX = _mainView.width / 2;
    _topImage.top = 20;
    
    _greetingsCaption = [UILabel new];
    _greetingsCaption.font = [UIFont bold:20];
    _greetingsCaption.textColor = [UIColor gray];
    _greetingsCaption.text = @"Поздравляем!";
    [_greetingsCaption sizeToFit];
    _greetingsCaption.top = _topImage.bottom + 20;;
    _greetingsCaption.centerX = _mainView.width / 2;
    
    _greetingsText              = [UILabel new];
    _greetingsText.font         = [UIFont normal];
    _greetingsText.textColor    = [UIColor gray];
    _greetingsText.text = [NSString stringWithFormat:@"Вы успешно приобрели подписку на наши услуги. Срок подписки: %@. Через некоторое время наш специалист напишет вам в чате.\n\nА пока вы можете ответить на вопросы анкеты.", self.subscription.subscriptionDuration];
    _greetingsText.numberOfLines = 0;
    _greetingsText.width = _mainView.width - 30;;
    _greetingsText.textAlignment = NSTextAlignmentLeft;
    [_greetingsText sizeToFit];
    _greetingsText.top = _greetingsCaption.bottom + 30;
    _greetingsText.centerX = _mainView.width / 2;
    
    _mainView.height = _greetingsText.bottom + 40;
    
    _okButton = [self roundedButton];
    [_okButton setTitle:@"Заполнить анкету"];
    [_okButton addTarget:self action:@selector(okButtonPressed:)];
    _okButton.top = _mainView.bottom + 30;
    _okButton.centerX = self.view.width / 2;
//    _okButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    
    [_mainView addSubview:_topImage];
    [_mainView addSubview:_greetingsCaption];
    [_mainView addSubview:_greetingsText];
    
    [self.view addSubview:_mainView];
    [self.view addSubview:_okButton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:[UIView new]];
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NDM_NOTIFICATION_UPDATE_PROFILE object:nil];
}

#pragma mark - button

-(void) okButtonPressed: (id)sender
{
//    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
