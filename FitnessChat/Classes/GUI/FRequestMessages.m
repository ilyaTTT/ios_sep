//
//  FRequestMessages.m
//  FitnessChat
//
//  Created by user on 22.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestMessages.h"

@implementation FRequestMessages

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"/rest/chat/room-messages?%@&room_id=%@",[self accessTokenString],_chat.chatId];
}

- (NSString *)body
{
    return nil;
}

- (NSString *)method
{
    return @"GET";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}



@end
