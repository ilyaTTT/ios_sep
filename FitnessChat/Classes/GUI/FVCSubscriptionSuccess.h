//
//  FVCSubscriptionSuccess.h
//  FitnessChat
//
//  Created by owel on 09/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import "FVC.h"
#import "Subscription.h"

@interface FVCSubscriptionSuccess : FVC

//@property (strong, nonatomic) NSString *subscriptionDescription;
@property (strong, nonatomic) Subscription *subscription;

@end
