//
//  FVCMovie.m
//  FitnessChat
//
//  Created by Victort on 09/09/15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVCMovie.h"
#import "FVC.h"
#import "AppDelegate.h"

@interface FVCMovie ()

@end

@implementation FVCMovie

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.allowedOrientation = UIInterfaceOrientationMaskAll;
}

-(void)viewWillDisappear:(BOOL)animated
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.allowedOrientation = UIInterfaceOrientationMaskPortrait;
    
    [UIViewController attemptRotationToDeviceOrientation];
    
    [super viewWillDisappear:animated];
}


-(BOOL)shouldAutorotate
{
    [super shouldAutorotate];
    
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    [super supportedInterfaceOrientations];
    
    return UIInterfaceOrientationMaskAll;
}

@end
