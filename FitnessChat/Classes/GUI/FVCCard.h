//
//  FVCCard.h
//  FitnessChat
//
//  Created by owel on 07/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import "FVC.h"
#import "Subscription.h"


@interface FVCCard : FVC

@property (strong, nonatomic) Subscription *subscription;

@end
