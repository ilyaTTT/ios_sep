//
//  FVCConfirm.m
//  FitnessChat
//
//  Created by user on 10.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVCConfirm.h"
#import "FRequestConfirm.h"
#import "FVCRegister.h"
#import "FTabbar.h"
#import "FRequestPushTokenSave.h"
#import "FVCContainer.h"


@interface FVCConfirm ()

//@property (strong, nonatomic) FormTextField *tfCode;
@property (strong, nonatomic) UITextField *tfCode;
@property (strong, nonatomic) UIButton *nextButton;

@end


@implementation FVCConfirm

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Вход";
    self.backgroundName = @"back_bg"; //@"back_confirm";

    UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 45, self.view.width - 30, 40)];
    topLabel.textColor = [UIColor gray];
    topLabel.font  = [UIFont normal];
    topLabel.lineBreakMode = NSLineBreakByWordWrapping;
    topLabel.numberOfLines = 0;
    topLabel.text = [NSString stringWithFormat:@"На номер %@ отправлен код подтверждения",_fullPhone];
    topLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:topLabel];
    
    //_tfCode = [self codeTF];
//    _tfCode.maxLength = 10;
    _tfCode = [[UITextField alloc] initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 60)];
    _tfCode.textColor = [UIColor gray];
    _tfCode.top = topLabel.bottom + 35;
    [_tfCode setText: @""];
    _tfCode.backgroundColor = [UIColor whiteColor];
    _tfCode.textAlignment = NSTextAlignmentCenter;
    _tfCode.keyboardType = UIKeyboardTypeNumberPad;
    _tfCode.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _tfCode.layer.borderWidth = 0.5;
    [self.view addSubview:_tfCode];
    
    _nextButton = [self roundedButton];
    _nextButton.top = _tfCode.bottom + 35;
    [_nextButton addTarget:self action:@selector(onNext)];
    [_nextButton setTitle:@"Войти"];
    [self.view addSubview:_nextButton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    // One pixel line above the keyboard.
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 1.0 / [UIScreen mainScreen].scale)];
    separatorView.backgroundColor = [UIColor lightGrayColor];
    _tfCode.inputAccessoryView = separatorView;
    
    [_tfCode becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    return;
    [self onNext];
}

- (void)onNext
{

    [AppDelegate.shared showActivity];
    
    FRequestConfirm *request = [FRequestConfirm new];
    request.phone = _phone;
    request.code = _tfCode.text;
    request.parent = self;
    [request start];
}

- (void)requestCompletion:(NWRequest *)request withObject:(NSDictionary *)dict andError:(NSError *)error
{
    [AppDelegate.shared hideActivity];
    if ([dict[@"result"] isEqualToString:@"ok"])
    {
        [User setCurrUser:dict[@"userInfo"]];
        RCore.shared.token = dict[@"userInfo"][@"token"];
        
        if (_isLogin)
        {
//            [self presentViewController:[FTabbar shared] animated:YES completion:nil];
//            [self presentViewController:[FVCContainer shared] animated:YES completion:nil];
//            [self dismissViewControllerAnimated:YES completion:nil];
            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            appDelegate.window.rootViewController = [FVCContainer shared];
            [appDelegate.window makeKeyAndVisible];
        }
        else
        {
        FVCRegister *vc = [FVCRegister new];

        [self.navigationController pushViewController:vc animated:true];
        }
        [[FRequestPushTokenSave new] start];
    }
    else
    {
        [AppDelegate.shared showErrors:dict[@"errors"]];
    }
}

@end
