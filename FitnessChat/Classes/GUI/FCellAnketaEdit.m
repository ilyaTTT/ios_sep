//
//  FCellAnketa.m
//  FitnessChat
//
//  Created by user on 21.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FCellAnketaEdit.h"
#import "Question.h"
#import "Replies.h"

#define indent 10
#define elHeight 55

@interface FCellAnketaEdit ()
{
    UIView *_answerView;
    NSMutableArray *_labels;
    NSMutableArray *_switchs;
}
@end

@implementation FCellAnketaEdit

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.contentView.backgroundColor = [UIColor colorWith256Red:244 green:246 blue:245];
        self.textLabel.font = [UIFont light:15];
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.textLabel.numberOfLines = 0;
        self.detailTextLabel.font = [UIFont light:15];
        self.detailTextLabel.numberOfLines = 0;
        self.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return self;
}

- (UIEdgeInsets)layoutMargins
{
    return UIEdgeInsetsZero;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.textLabel.width = self.width - 2 *indent;
    _answerView.width = self.width;

    
    [self.textLabel sizeToFit];
    self.textLabel.left = indent;
    self.textLabel.top = indent;

    _answerView.left = 0;
    _answerView.top = self.textLabel.bottom + indent;
    
    if ([_question.type isEqualToString:@"text"])
    {
        _answerView.height = 50;
        _textView.left = indent;
        _textView.width = _answerView.width - 2 * indent;
    }
    else
    {
        _answerView.height = _question.replies.count * elHeight;
        CGFloat cy = elHeight / 2;
        for (int i = 0; i< _question.replies.count; i++)
        {
            UILabel *label = _labels[i];
            UISwitch *sw = _switchs[i];
            label.width = self.width - sw.width - indent * 3;
            [label sizeToFit];
            label.left = indent;
            label.centerY = cy;
            [sw sizeToFit];
            sw.centerY = cy;
            sw.right = _answerView.width - 2 * indent;
            cy += elHeight;
        }
    }
}

- (void)switchValueChanged:(UISwitch *)sw
{
    if ([_question.type isEqualToString:@"select_one"])
    {
        for (UISwitch *sws in _switchs)
        {
            if (sw != sws)
            {
                [sws setOn:NO animated:YES];
            }
        }
    }
}

- (NSString *)answer
{
    if ([_question.type isEqualToString:@"text"])
    {
        return _textView.text;
    }
    else
        {
            NSMutableString *res = [NSMutableString new];
            for (int i = 0; i< _question.replies.count; i++)
            {
                BOOL isOn = ((UISwitch *)_switchs[i]).isOn;
                if (isOn)
                {
                    [res appendString:((Replies *)_question.replies.allObjects[i]).repliesId];
                    [res appendString:@";"];
                }
            }
            return res;
        }
}

- (void)setQuestion:(Question *)question
{
    _question = question;
    self.textLabel.text = question.name;
    
    [_answerView removeFromSuperview];
    _answerView = [UIView new];
    [self addSubview:_answerView];
    
    if ([_question.type isEqualToString:@"text"])
    {
        _textView = [UITextView new];
        _textView.backgroundColor = [UIColor whiteColor];
        _textView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _textView.text = _question.answer;
        _textView.font = [UIFont light:14];
        _textView.textColor = [UIColor colorWith256Red:111 green:117 blue:123];
        _answerView.backgroundColor = [UIColor whiteColor];
        [_answerView addSubview:_textView];
    }
    else
    {
        _labels = [NSMutableArray new];
        _switchs = [NSMutableArray new];
        
        NSArray *checkedReplies = [_question checkedReplies];
        for (Replies *reply in _question.replies)
        {
            UILabel *label = [UILabel new];
            label.numberOfLines = 0;
            label.lineBreakMode = NSLineBreakByWordWrapping;
            label.textColor = [UIColor colorWith256Red:111 green:117 blue:123];
            label.font = [UIFont light:14];
            label.text = reply.text;
            UISwitch *sw = [UISwitch new];
            [sw addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
            
            if ([checkedReplies containsObject:reply])
            {
                sw.on = YES;
            }
            [_answerView addSubview:label];
            [_answerView addSubview:sw];
            [_switchs addObject:sw];
            [_labels addObject:label];
        }
    }
}

+ (CGFloat)heightForQuestion:(Question *)question
{
    static UILabel *label;
    if (label == nil)
    {
        label = [UILabel new];
        label.font = [UIFont light:15];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
    }
    label.width = [UIScreen mainScreen].bounds.size.width - indent * 2;
    label.text = question.name;
    [label sizeToFit];
    CGFloat height = 50;
    if (![question.type isEqualToString:@"text"])
    {
        height = elHeight * question.replies.count;
    }
    
    return height + label.height + indent * 3;
}

@end
