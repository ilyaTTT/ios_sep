//
//  FImageView.h
//  FitnessChat
//
//  Created by user on 31.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FImageView : UIScrollView
@property (strong, nonatomic) UIImage *image;
- (void)show;
@end
