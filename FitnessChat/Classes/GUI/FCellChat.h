//
//  FCellChat.h
//  FitnessChat
//
//  Created by Алексей on 26.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FCellChat;

@protocol FCellChatProtocol
- (void)cell: (FCellChat *)cell chatPlay:(Message *)message;
- (void)cellChatShowImage:(Message *)message;
- (void)cellOnDelete:(Message *)message;

@end

@interface FCellChat : UITableViewCell{
    UIView *viewSending;
}
+ (CGFloat)heightForMessage:(Message *)message;


@property (strong, nonatomic) Message * message;
@property (nonatomic) BOOL loading;
@property (weak, nonatomic) id <FCellChatProtocol> delegate;

@end

@interface FCellChatGroup : FCellChat

@end
