//
//  FVCAnketa.m
//  FitnessChat
//
//  Created by user on 21.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVCAnketa.h"
#import "Anketa.h"
#import "Question.h"
#import "FCellAnketa.h"
#import "FVCAnketaEdit.h"

@interface FVCAnketa ()<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedController;


@end

@implementation FVCAnketa

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = _anketa.name;
    self.backgroundName = @"back_bg"; //@"back_consultant";
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    _tableView.backgroundColor = self.view.backgroundColor;
    
    [self.view addSubview:_tableView];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"edit"] style:UIBarButtonItemStylePlain target:self action:@selector(onEdit)];
}

- (void)onEdit
{
    FVCAnketaEdit *vc = [FVCAnketaEdit new];
    vc.anketa = _anketa;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([self.fetchedController performFetch:nil])
    {
        [_tableView reloadData];
    }
    _tableView.height = self.view.height - [FTabbar shared].tabBar.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Question *quest = [self.fetchedController objectAtIndexPath:indexPath];
    return [FCellAnketa heightForQuestion:quest];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.fetchedController fetchedObjects].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FCellAnketa *cell = [tableView dequeueReusableCellWithIdentifier:@"kCell"];
    
    if (cell == nil)
    {
        cell = [[FCellAnketa alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"kCell"];
    }
    Question *quest = [self.fetchedController objectAtIndexPath:indexPath];
    cell.question = quest;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - fetchedController delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [_tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                      withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [_tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                      withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            
            break;
            
        case NSFetchedResultsChangeMove:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
        {
            newIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row inSection:2];
            [_tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete:
            newIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row inSection:2];
            [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
            
        case NSFetchedResultsChangeUpdate:
        {
            [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
            break;
            
        case NSFetchedResultsChangeMove:
        {
            newIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row inSection:2];
            indexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:2];
            [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [_tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}

- (NSFetchedResultsController *)fetchedController
{
    if (_fetchedController == nil)
    {
        NSManagedObjectContext *context = [RBase shared].moc;
        
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Question"];
        request.predicate = [NSPredicate predicateWithFormat:@"anketa=%@",_anketa];
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        NSArray *sortDescriptors = @[sortDescriptor1];
        [request setSortDescriptors:sortDescriptors];
        
        _fetchedController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        _fetchedController.delegate = self;
    }
    
    return _fetchedController;
}
@end
