//
//  FPlayerManager.m
//  FitnessChat
//
//  Created by Алексей on 30.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FPlayerManager.h"
#import <AVFoundation/AVFoundation.h>
#import "File.h"

@interface FPlayerManager () <AVAudioPlayerDelegate>
{
    AVAudioPlayer * _player;
    File *_file;
    __weak id <FPlayerManagerProtocol>_delegate;
    NSTimer *_timer;
}

@end

@implementation FPlayerManager

+(FPlayerManager *)shared
{
    static FPlayerManager *engine = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        if (engine == nil)
        {
            engine = [FPlayerManager new];
        }
    });
    
    return engine;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(proximityChanged:)
                                                     name:UIDeviceProximityStateDidChangeNotification
                                                   object:[UIDevice currentDevice]];
    }
    return self;
}


- (void) proximityChanged:(NSNotification *)notification {
    UIDevice *device = [notification object];
    NSLog(@"In proximity: %i", device.proximityState);
    
    NSError *error;
    
//    if(device.proximityState == 0)
//        [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
//    else
//        [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
    if (device.proximityState)
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord
                                               error:&error];
    else
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
                                               error:&error];
    
    if (error)
        NSLog(@"proximityChanged error: %@", error);
    
}

//Plays file. /*If a sound file was recorded on one phone then the same user uses this app on another phone, then that sound file illustration will appear in the chat (as it should), but it won't play because the actual data DOESN'T load into that sound cell. I am not sure why the server doesn't send that sound data over to the second phone (for the same user).*/
-(void)playFile:(File *)file forTarget:(id<FPlayerManagerProtocol>)target
{
    
    if (_file == file)
    {
        if (_player.playing)
        {
            [UIDevice currentDevice].proximityMonitoringEnabled = NO;
            [_player pause];
            [_timer invalidate];
        }
        else
        {
            [UIDevice currentDevice].proximityMonitoringEnabled = YES;
            [_player play];
            _timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(tick) userInfo:nil repeats:YES];
        }
        [self tick];
        
        //NSLog(@"Method 'playFile' is doing something here...");
    }
    else
    {
        [UIDevice currentDevice].proximityMonitoringEnabled = YES;
        _delegate   = target;
        _file       = file;
        NSError *audioSessionError = nil;

        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
                                               error:&audioSessionError];

        
        
        if (audioSessionError != nil) {
            
            //NSLog (@"Error setting audio session category: %@",audioSessionError);
//            return;
        }
        

        _player             = [[AVAudioPlayer alloc] initWithContentsOfURL:_file.localUrl error:nil]; //Gets sound file from server.
        _player.delegate    = self;
        
        [_player play]; //Actually starts playing the loaded sound file.
        
        file.info = [NSString stringWithFormat:@"%d",(int)_player.duration];
        [_timer invalidate];
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(tick) userInfo:nil repeats:YES];
        
        //NSLog(@"Method 'playFile' began playing sound file.");
    }
    
    //NSLog(@"Method 'playFile' finished.");
}


//Only using a separate method to play the sound file because 'performSelectorInBackground' is calling upon it.
//-(void)playSoundFile
//{
//    [_player play]; //Actually starts playing the loaded sound file.
//}


-(void)tick
{
    if (_player.duration == 0)
    {
        [_delegate updateTime:_player.currentTime progress:0 isPlaying:_player.playing];
    }
    else
    {
        [_delegate updateTime:_player.currentTime progress:_player.currentTime / _player.duration isPlaying:_player.playing];
    }
}


//What does this do?
-(void)prooveFile:(File *)file forTarget:(id<FPlayerManagerProtocol>)target
{
    if ([_file.name isEqualToString:file.name])
    {
        _delegate = target;
        [self tick];
        
        //NSLog(@"Prooving something");
    }
}


-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [_timer invalidate];
    _timer = nil;
    [_delegate updateTime:_player.duration progress:0 isPlaying:NO];
    [UIDevice currentDevice].proximityMonitoringEnabled = NO;
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
