
//  FVCChat.m
//  FitnessChat
//
//  Created by user on 22.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVCChat.h"
#import "FRequestMessages.h"
#import "FRequestMessageSend.h"
#import "FVChatField.h"
#import "FChatEngine.h"
#import "FCellChat.h"
#import "File.h"
#import "AlertHelpers.h"
#import "FVCMovie.h"
#import "FVCContainer.h"
#import "BackgroundSender.h"
#import "NSData+Compression.h"
#import "MessageFactory.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

#import <UCZProgressView/UCZProgressView.h> // ProgressView

#import <MobileCoreServices/UTCoreTypes.h>

//#import <QuartzCore/QuartzCore.h> //Allows to use the zPosition property of the view's layer (it's a CALayer object) to change the z-index of the view.


@interface FVCChat () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, FVChatFieldProtocol,UIImagePickerControllerDelegate, UINavigationControllerDelegate, FCellChatProtocol, UIActionSheetDelegate>
{
    MPMoviePlayerViewController * _moviewPlayer;
    CGFloat _defaultFieldBottom;
    CGFloat _defaultFieldHeight;
    
    NSTimer *progressTimer;
}

@property (strong, nonatomic) UITableView                   *tableView;
@property (strong, nonatomic) NSFetchedResultsController    *fetchedController;
@property (strong, nonatomic) FVChatField                   *chatField;

@property (assign, nonatomic) BOOL                          isEnabled;
//@property (strong, nonatomic) UIColor                       *oldSendColor;

@property (nonatomic) UCZProgressView *progressView;
@property (nonatomic) UILabel *instructionLabel;

//
@property (strong, nonatomic) UIView        *tableHeader;
@property (strong, nonatomic) UIImageView   *photoView;
@property (strong, nonatomic) UILabel       *nLabel;
@property (strong, nonatomic) UIImageView   *titleIcon;

@end


@implementation FVCChat

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWith256Red:244 green:246 blue:245];
    
    _tableView                      = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    _tableView.autoresizingMask     = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _tableView.height               -= self.chatField.height;
    _tableView.delegate             = self;
    _tableView.dataSource           = self;
    _tableView.backgroundColor      = [UIColor clearColor];
    _tableView.keyboardDismissMode  = UIScrollViewKeyboardDismissModeOnDrag;
    _tableView.sectionFooterHeight  = 0;
    _tableView.separatorColor       = [UIColor clearColor];
    [self.view addSubview:_tableView];
    
    //Header.
    CGFloat topHeight       = 290;
    CGFloat bottomHeight    = 165; //<-- Unused variable 'bottomHeight'.
    CGFloat photoBGHeight   = topHeight - 2 * 20;
    CGFloat photoHeight     = photoBGHeight - 20;
    CGPoint photoCenter     = CGPointMake(self.view.width / 2, topHeight / 2);
    
    UIView *photoBG             = [[UIView alloc] initWithFrame:CGRectMake(0, 0, photoBGHeight, photoBGHeight)];
    photoBG.backgroundColor     = [UIColor whiteColor];
    photoBG.center              = photoCenter;
    photoBG.layer.cornerRadius  = photoBGHeight / 2;
    photoBG.layer.masksToBounds = YES;
    
    _photoView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, photoHeight, photoHeight)];
    _photoView.center = photoCenter;
    _photoView.layer.cornerRadius = photoHeight / 2;
    _photoView.layer.masksToBounds = YES;
    _photoView.backgroundColor = [UIColor clearColor];
    
    _nLabel                 = [[UILabel alloc] initWithFrame:_photoView.frame];
    _nLabel.textAlignment   = NSTextAlignmentCenter;
    _nLabel.textColor       = [UIColor blue];
    _nLabel.font            = [UIFont bold:20];
    _nLabel.text            = self.info;
    _nLabel.numberOfLines   = 0;
    _nLabel.width           = self.view.width - 30;
    [_nLabel sizeToFit];
    _nLabel.centerX         = self.view.width / 2;
    _nLabel.top             = photoBG.bottom + 40;
    
    _tableHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.width, _tableView.height - 100)];
    _tableHeader.clipsToBounds = YES;
    [_tableHeader addSubview:photoBG];
    [_tableHeader addSubview:_photoView];
    [_tableHeader addSubview:_nLabel];
    
    _tableView.tableHeaderView = _tableHeader;
    
    if (self.consultantImageUrlString)
        [self setImage:self.consultantImageUrlString];
    else
        _photoView.image = self.consultnatImage;
    //
    
    self.chatField.bottom = self.view.height;
    _defaultFieldBottom = self.chatField.bottom;
    [self.view addSubview:self.chatField];

    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)]];
    //[self.view addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress)]];
    
    //Network connection check.
    _isEnabled = YES;
    
    [[BackgroundSender shared] setChat:_chat];
    _chatField.voice.userInteractionEnabled = YES;
}


/** For long press gesture recognizer. */
-(void)longPress
{
    
}


/** The one statement inside this method is specifically for dismissing/hiding the keyboard. */
-(void)onTap
{
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    
    self.tabBarController.tabBar.translucent = NO;
    [self reloadData];
    
    [_tableView scrollRectToVisible:CGRectMake(0, _tableView.contentSize.height - 1, _tableView.width, 1) animated:NO];
    
    // navigation bar
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:[UIView new]];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"user"] style:UIBarButtonItemStylePlain target:self action:@selector(settingsPressed:)];
    self.navigationItem.rightBarButtonItem = settingsButton;
    
    CGFloat titleIconHeight = self.navigationController.navigationBar.height / 10 * 8;
    
    _titleIcon = [[UIImageView alloc] initWithImage:_photoView.image];
    _titleIcon.layer.cornerRadius = titleIconHeight / 2;
    _titleIcon.layer.masksToBounds = YES;
    _titleIcon.frame = CGRectMake(0, 0, titleIconHeight, titleIconHeight);
    
    UILabel *titleLabel         = [UILabel new];
    titleLabel.text             = _name;
    titleLabel.font             = [UIFont medium:15];
    titleLabel.textColor        = [UIColor whiteColor];
    titleLabel.numberOfLines    = 1;
    [titleLabel sizeToFit];
    titleLabel.left             = _titleIcon.right + 10;
    titleLabel.top              = _titleIcon.top;
    
    UILabel *secondTitleLabel = [UILabel new];
    
//    secondTitleLabel.text = ([User currentUser].paidAccess) ? @"Консультант" : @"Администратор";
    if ([self.consultantRole isEqualToString:@"consultant"])
        secondTitleLabel.text = @"Консультант";
    else
        secondTitleLabel.text = @"Администратор";
    
    secondTitleLabel.font           = [UIFont medium:13];
    secondTitleLabel.textColor      = [UIColor whiteColor];
    secondTitleLabel.numberOfLines  = 1;
    [secondTitleLabel sizeToFit];
    secondTitleLabel.left           = titleLabel.left;
    secondTitleLabel.top            = titleLabel.bottom;
    
    UIView *titleView   = [[UIView alloc] init];
    titleView.frame     = CGRectMake(0, 0, MAX(titleLabel.right, secondTitleLabel.right), _titleIcon.height);
    
    [titleView addSubview:_titleIcon];
    [titleView addSubview:titleLabel];
    [titleView addSubview:secondTitleLabel];
    
    self.navigationItem.titleView = titleView;
    
    //Listens for commands to either show, hide or change the keyboard.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChange:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self onTap]; //Hides the keyboard. The method [self.view endEditing:YES] does the exact same thing.

    //Listening to notifications.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)reloadData
{
    [[FChatEngine shared] loadMessagesForChat:_chat withCompletion:nil];
    if ([self.fetchedController performFetch:nil])
    {
        [_tableView reloadData];
    }
}

//Moves the scroll view down to show new messages.
-(void)moveDown
{
    //Added this dispatch here.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        int section = [[self.fetchedController sections] count] - 1;
        if (section < 0)
        {
            return;
        }
        id <NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedController] sections] objectAtIndex:section];
        
        int row =[sectionInfo numberOfObjects] - 1;
        if (row < 0)
        {
            return;
        }
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });
}


#pragma mark - Buttons

-(void) settingsPressed: (id)sender
{
    [[FVCContainer shared] switchToChild:1];
}


#pragma mark - Chat protocol

- (void)chatFieldMesageSend
{
    Message *message = [MessageFactory createMessageAtText:_chatField.text andChat:_chat];
    
    BackgroundSenderInfo *info = [BackgroundSenderInfo new];
    info.type = BITMessage;
//    info.addData = data;
    info.backgroundSenderId = message.localId;
    [[BackgroundSender shared] queueInfo:info];
//    FRequestMessageSend *request = [FRequestMessageSend new];
//    request.chat = _chat;
//    request.text = _chatField.text;
//    request.parent = [FChatEngine shared];
//    [request start];
    
    [_chatField clean];
}


#pragma mark cell protocol



- (void)cell:(FCellChat *)cell chatPlay:(Message *)message
{
    if (message.type == MESSAGE_Video)
    {
        if (cell.loading)
        {
            [[[UIAlertView alloc] initWithTitle:@"Видео загружается"
                                       message:NULL
                                      delegate:NULL
                             cancelButtonTitle:@"OK"
                              otherButtonTitles: nil] show];
            return;
        }
        
        
        [self createUCZProgressView];
        self.progressView.showsText = YES;
        self.progressView.indeterminate = YES;
        [message.file loadDataIfNeddedWithCompletion:^(NSData *data) {
            [self.progressView removeFromSuperview];
            
            AVAudioSession *session = [AVAudioSession sharedInstance];
            [session setCategory:AVAudioSessionCategoryPlayback error:nil];
            
            NSURL *url = message.file.localUrl;
            _moviewPlayer = [[FVCMovie alloc] initWithContentURL:url];
            [self presentMoviePlayerViewControllerAnimated:_moviewPlayer];
            [_moviewPlayer.moviePlayer play];
        }
         progress:^(NSInteger total, NSInteger current) {
             NSLog(@"%ld-%ld", (long)total, (long)current);
             self.progressView.progress = [@(current)floatValue]/[@(total)floatValue];
         }];
        
    }
}

- (void)cellChatShowImage:(Message *)message
{
    [[AppDelegate shared] showImage:[UIImage imageWithData:message.file.data]];
}

- (void)cellOnDelete:(Message *)message
{
    message.deleted = YES;
    message.chat    = nil;
    [message.managedObjectContext save:nil];
}


#pragma mark - table header view

- (void)setImage:(NSString *)newUrl
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:newUrl]]; //'NSData dataWithContentsOfURL:' expected behaviour is to block the data until it is downloaded. Then it displays/plays.
        if (data != nil)
        {
            [self saveImageData:data withName:[self name]];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setImage];
        });
    });
}

- (void)setImage
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *data = [self loadImageWithName:[self name]];
        if (data != nil)
        {
            UIImage *image = [UIImage imageWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{
                _photoView.image = image;
                _titleIcon.image = image;
            });
        }
    });
}

- (void)saveImageData:(NSData*)data withName:(NSString *)name
{
    if (data != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        //        //NSLog(@"%@",documentsDirectory);
        NSString* path = [documentsDirectory stringByAppendingPathComponent:
                          name];
        
        if(![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory isDirectory:nil])
        {
            NSError * error = nil;
            [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                                      withIntermediateDirectories:YES
                                                       attributes:nil
                                                            error:&error];
            if (error != nil) {
                //NSLog(@"error creating directory: %@", error);
            }
        }
        
        if ([data writeToFile:path atomically:YES])
        {
            [NSFileManager addSkipBackupAttributeToItemPath:path];
        }
    }
}

- (NSData *)loadImageWithName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      name];
    
    return [NSData dataWithContentsOfFile:path];
}


#pragma mark - tableView delegate datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = [FCellChat heightForMessage:[self.fetchedController objectAtIndexPath:indexPath]];
    return height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedController] sections] objectAtIndex:section]; //[[self fetchedController] sections][section]; is also appropriate by the compiler.
    
    return [sectionInfo numberOfObjects];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedController] sections] objectAtIndex:section];
    
    
    NSString *name = [sectionInfo name];
    
    static NSDateFormatter *formatter1;
    if (formatter1 == nil)
    {
        formatter1 = [NSDateFormatter new];
        [formatter1 setDateFormat:@"yyyy.MM.dd"];
    }
    NSDate *date = [formatter1 dateFromString:name];
    static NSDateFormatter *formatter;
    if (formatter == nil)
    {
        formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"dd MMMM"];
    }
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _tableView.width, 30)];
    label.backgroundColor = [UIColor colorWithWhite:1 alpha:0.7];
    label.font = [UIFont light:12];
    label.text = [formatter stringFromDate:date];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blue];
    
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FCellChat *cell;// = [tableView dequeueReusableCellWithIdentifier:@"kCell"];
    if (cell == nil)
    {
        cell = [[FCellChat alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"kCell"];
        cell.delegate = self;
    }
    Message *message = [self.fetchedController objectAtIndexPath:indexPath];
    cell.message = message;
    
    //NSLog(@"Message is %@", cell.message);
    
    return cell;
}


/** Displays actual keyboard, for typing in messages, when NSNotification tells it too. */
-(void)keyboardWillShow:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size; //'UIKeyboardFrameEndUserInfoKey' keys return an NSValue instance containing a CGRect that holds the position & size of the keyboard.
    //[[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] layer].zPosition = 99; //<-- Modifying keyboard zPosition crashes the app.
    
    self.chatField.bottom = self.view.height - keyboardSize.height;

    [_tableView setContentInset:UIEdgeInsetsMake(0, 0, self.view.height - self.chatField.top - _defaultFieldHeight, 0)]; //Moves tableView content up.

    [self moveDown];
    _defaultFieldBottom = self.chatField.bottom;
}

- (void)keyboardWillChange:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    float centerY = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin.y + [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height / 2;
    if (centerY < self.view.height)
    {
        CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        self.chatField.bottom = self.view.height - keyboardSize.height;
        
        [_tableView setContentInset:UIEdgeInsetsMake(0, 0, self.view.height - self.chatField.top - _defaultFieldHeight, 0)];
        
        [self moveDown];
        _defaultFieldBottom = self.chatField.bottom;
    }
}

- (void)keyboardWillHide:(NSNotification *)n
{
    [_tableView setContentInset:UIEdgeInsetsMake(0, 0,self.chatField.height - _defaultFieldHeight, 0)];

    self.chatField.top = self.view.height - self.chatField.height; //_tableView.bottom;
    _defaultFieldBottom = self.chatField.bottom;
}

- (void)heightChangedIsEditing:(BOOL)editing
{
    self.chatField.bottom = _defaultFieldBottom;
    //    _tableView.height = self.chatField.top;
    [_tableView setContentInset:UIEdgeInsetsMake(0, 0, self.view.height - self.chatField.top - _defaultFieldHeight, 0)];
    [self moveDown];
}


#pragma mark - fetchedController delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView endUpdates]; //ТУТ ОШИБКА БУДЕТ ЕСЛИ УБРАТЬ ЭТУ ФИГНЮ!!!
    
     [self moveDown];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
        {
            [_tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
        }
            break;
        case NSFetchedResultsChangeDelete:
        {
            [_tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
        }
            break;
        case NSFetchedResultsChangeMove:
            break;
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    /*
     NSFetchedResultsChangeInsert   = 1,
     NSFetchedResultsChangeDelete   = 2,
     NSFetchedResultsChangeMove     = 3,
     NSFetchedResultsChangeUpdate   = 4
     */
    
    switch(type)
    {
//        case 0:
//            break;
        case NSFetchedResultsChangeInsert:
        {
            [_tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
            break;
        case NSFetchedResultsChangeDelete:
        {
            [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
            break;
        case NSFetchedResultsChangeMove:
        {
            [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [_tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
            break;
        case NSFetchedResultsChangeUpdate:
        {
            //[_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone]; //<-- DON'T UNCOMMENT! It causes a bug where sending a text message then sending a photo will crash the application.
        }
            break;
            
//        case NSFetchedResultsChangeMove:
//        {
//            [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//            [_tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
//        }
//            break;
    }
}

/** Accesses Core Data's 'NSManagedObjectContext'. */
-(NSFetchedResultsController *)fetchedController
{
    if (_fetchedController == nil)
    {
        NSManagedObjectContext *context = [RBase shared].moc; //Loads Core Data stuff into 'context'.
        
        NSFetchRequest *request             = [[NSFetchRequest alloc] initWithEntityName:@"Message"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"chat=%@ and deleted=%d", _chat, 0];
        request.predicate = predicate;
        NSSortDescriptor *sortDescriptor1   = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES]; //Organizes the chat/table rows by date in an ascending order. That means newest row will be at the bottom.
        NSArray *sortDescriptors            = @[sortDescriptor1];
        [request setSortDescriptors:sortDescriptors];
        
        _fetchedController                  = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:@"helpDate" cacheName:nil];
        _fetchedController.delegate = self;
    }
    
    return _fetchedController;
}

- (FVChatField *)chatField
{
    if (_chatField == nil)
    {
        _chatField = [[FVChatField alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 50)];
        _defaultFieldHeight = 50;
        _chatField.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
        _chatField.delegate = self;
    }
    return _chatField;
}

/** Displays the "add photo/video" view at the bottom. */
-(void)attachPressed
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Выберите источник" delegate:self cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles:@"Снять фото/видео",@"Отправить фото/видео", nil];
    [actionSheet showInView:self.view];
    
    
    //actionSheet.layer.zPosition = 100; //Will fix the order bug (When the keyboard is out for typing a message, its view overlaps this actionSheet's view). Big values are on top. You can use any values you want, including negative values. The default value is 0. I chose 100 to make sure its zPosition is above everything else.
    //[actionSheet setBounds:CGRectMake(0, 0, 320, 485)]; //actionSheet positioning.
    //[actionSheet becomeFirstResponder];
    //[[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardWillHideNotification object:nil]; //removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [self onTap]; // Hides keyboard.
}

/** Handles the UIActionSheet button selection options. */
- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) // CAMERA.
    {
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:nil forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, (NSString *)kUTTypeImage, nil];
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else if (buttonIndex == 1) // PHOTO LIBRARY.
    {
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:nil forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, (NSString *)kUTTypeImage, nil];
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}


/** Sends AUDIO file to server. */
- (void)dataRecored:(NSData *)data withTime:(int)time
{
    // 1 - CREATES A CHAT BUBBLE FOR SENT DATA.
    
    Message *message = [MessageFactory createMessageAtAudioData:data andChat:_chat andTime:time];
    
    // 2 - SENDS DATA TO SERVER.
    //[[FChatEngine shared] sendFileMessage:message]; // Original code.
    
    [self onTap]; // Hides keyboard.
    
    BackgroundSenderInfo *info = [BackgroundSenderInfo new];
    info.type = BITAudio;
    info.backgroundSenderId = message.localId;
    [[BackgroundSender shared] queueInfo:info];
    
    [self.tableView reloadData];

    
    //NSLog(@"Sent an audio file.");
}


#pragma mark - IMAGE/VIDEO PICKER DELEGATE

/** Handles UIImagePickerController selection by returning appropriate selection from: Camera OR Library. IF anything was selected & brought back then it will be sent to the server. */
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage * backButtonImage = [UIImage imageNamed: @"back_button"];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage: backButtonImage forState: UIControlStateNormal barMetrics: UIBarMetricsDefault];
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];

    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) // VIDEO.
    {
        NSString *path = [(NSURL *)[info objectForKey:UIImagePickerControllerMediaURL] path];

        if ( UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(path))
        {
            // Copy it to the camera roll.
            UISaveVideoAtPathToSavedPhotosAlbum(path, NULL, NULL, NULL);
        }
        // 1 - CREATES A CHAT BUBBLE FOR SENT DATA.
        
        NSData *data = [NSData dataWithContentsOfFile:path];
        Message *message = [MessageFactory createMessageAtVideoData:data andChat:_chat];
        
        BackgroundSenderInfo *info = [BackgroundSenderInfo new];
        info.type = BITVideo;
        info.backgroundSenderId = message.localId;
        [[BackgroundSender shared] queueInfo:info];
        
        [self.tableView reloadData];
        //NSLog(@"Sent a video.");
    }
    else // IMAGE.
    {
        // 1 - CREATES A CHAT BUBBLE FOR SENT DATA.
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        UIImageWriteToSavedPhotosAlbum(image, NULL, NULL, NULL);
        NSData *compressedImage = [NSData compressImage:image compressRatio:0.9 maxCompressRatio:0.1];
        Message *message = [MessageFactory createMessageAtImageData:compressedImage andChat:_chat];
        
        BackgroundSenderInfo *info = [BackgroundSenderInfo new];
        info.type = BITImage;
        info.backgroundSenderId = message.localId;
        [[BackgroundSender shared] queueInfo:info];
        
        [self.tableView reloadData];
        //NSLog(@"Sent an image.");
    }
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //[self loadUCZProgressView];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    UIImage * backButtonImage = [UIImage imageNamed: @"back_button"];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage: backButtonImage forState: UIControlStateNormal barMetrics: UIBarMetricsDefault];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - HELPERS

///** Tint chat.*/
//-(void)tintScreen
//{
//    UIView *tintedView = [[UIView alloc] initWithFrame:self.view.bounds];
//    tintedView.backgroundColor = [UIColor blackColor];
//    tintedView.alpha = 0.5;
//    tintedView.tag = 222;
//    [self.view addSubview:tintedView];
//    
//    //NSLog(@"Screen tinted.");
//}

/** Progress view is loaded. */

-(void)createUCZProgressView
{
    self.progressView = [[UCZProgressView alloc] initWithFrame:self.view.bounds];
    self.progressView.translatesAutoresizingMaskIntoConstraints = NO;
    //self.progressView.blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    self.progressView.showsText = YES;
    self.progressView.tintColor = [UIColor lightGrayColor]; // Ring color.
    self.progressView.textColor = [UIColor lightGrayColor]; // Text (inside ring) color.
    self.progressView.textSize = 20;
    self.progressView.radius = 40.0;
    self.progressView.lineWidth = 3.0;
    self.progressView.progress = 0.0;
    self.progressView.backgroundView.backgroundColor = [UIColor blackColor];
    self.progressView.backgroundView.alpha = 0.75;
    //self.progressView.indeterminate = YES;
    //self.progressView.usesVibrancyEffect = NO;
    [self.view addSubview:self.progressView];
}

- (void)loadUCZProgressView
{
    
    [self createUCZProgressView];
    
    // Text label for letting the user know what they can do.
    self.instructionLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - (self.view.frame.size.width/2),
                                                                      self.view.frame.size.height/2 + 50,
                                                                      self.view.frame.size.width,
                                                                      40)];
    self.instructionLabel.backgroundColor = [UIColor clearColor];
    self.instructionLabel.textColor = [UIColor lightGrayColor];
    self.instructionLabel.textAlignment = NSTextAlignmentCenter;
    self.instructionLabel.adjustsFontSizeToFitWidth = YES;
    self.instructionLabel.text = @"Нажмите на экран для отмены загрузки";
    [self.instructionLabel setFont:[UIFont systemFontOfSize:16]];
    [self.view addSubview:self.instructionLabel];
    
    
    // Tap gesture that will cancel the progress bar.
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.progressView addGestureRecognizer:singleFingerTap];
    
    
    // Timer.
    progressTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
    
    // Animates progress (Old technique.).
//    [UIView animateWithDuration:5.0 delay:0.4 options:UIViewAnimationOptionLayoutSubviews animations:^{
//        
//        [self.view addSubview:self.progressView];
//        self.progressView.progress = 0.0;
//        
//    } completion:^(BOOL finished) {
//        
//        self.progressView.progress = 1.0;
//    
//    }];
}

/** Updated ever 0.1 seconds. */
-(void)timerFired:(NSTimer *)theTimer
{
    float progress = [NBS3Uploader uploader].uploadProgressCounter;
    if (progress < progress * 7)
    {
//        if (progress >= progress * 7)
//        {
//            progress = 1.0;
//        }
        ////NSLog(@"PROGRESS IS %f", progress);
        self.progressView.progress = progress;
    }
}

-(void)closeProgressBar
{
    [NBS3Uploader uploader].uploadProgressCounter = 0.0;
    [self.progressView removeFromSuperview];
    [self.instructionLabel removeFromSuperview];
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//    });
    
    // Closes progress view when progress is 100%.
//    self.progressView.animationDidStopBlock = ^{
//        [self.progressView removeFromSuperview];
//    };
}

/** The event handling method. */
-(void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    //CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    if (self.progressView)
    {
        [[NBS3Uploader uploader] cancelUpload];
        [FChatEngine shared].shouldQuitLoop = YES;
        [self removeLastCoreDataEntryFromEntity:@"Message"];
        [self removeLastCoreDataEntryFromEntity:@"File"];
        [self closeProgressBar];
        //NSLog(@"Tapped screen.");
    }
    
    
//    // Find tinted view & delete it.
//    for (UIView *i in self.view.subviews)
//    {
//        if ([i isKindOfClass:[UILabel class]])
//        {
//            UILabel *newLbl = (UILabel *)i;
//            if (newLbl.tag == 222)
//            {
//                /// Write your code
//                [newLbl removeFromSuperview];
//            }
//        }
//    }
}

-(void)removeLastCoreDataEntryFromEntity:(NSString *)name
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:name inManagedObjectContext:[RBase shared].moc]];
    
    NSError *error = nil;
    NSArray *results = [[RBase shared].moc executeFetchRequest:fetchRequest error:&error];
    
    [[RBase shared].moc deleteObject:[results lastObject]];
    
    [[RBase shared] saveMain];
    
    
//    NSEntityDescription *productEntity = [NSEntityDescription entityForName:@"Message" inManagedObjectContext:[RBase shared].moc];
//    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
//    [fetch setEntity:productEntity];
//    NSError *fetchError;
//    NSArray *fetchedProducts = [[RBase shared].moc executeFetchRequest:fetch error:&fetchError];
//    [[RBase shared].moc deleteObject:fetchedProducts.lastObject];
//    
//    NSEntityDescription *productEntity2 = [NSEntityDescription entityForName:@"File" inManagedObjectContext:[RBase shared].moc];
//    NSFetchRequest *fetch2 = [[NSFetchRequest alloc] init];
//    [fetch2 setEntity:productEntity2];
//    NSError *fetchError2;
//    NSArray *fetchedProducts2 = [[RBase shared].moc executeFetchRequest:fetch2 error:&fetchError2];
//    [[RBase shared].moc deleteObject:fetchedProducts2.lastObject];
    
    
    //[_tableView delete:message];
    [_tableView reloadData];
    [_tableView reloadInputViews];
}



@end


@interface FVCChatGroup ()

@end


@implementation FVCChatGroup

- (void)viewDidLoad
{
    [super viewDidLoad];
 
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [FCellChatGroup heightForMessage:[self.fetchedController objectAtIndexPath:indexPath]];
}


/** This method loads data into every cell. It doesn't seem to be used anymore. */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FCellChatGroup *cell;// = [tableView dequeueReusableCellWithIdentifier:@"kCell"];
    if (cell == nil)
    {
        cell = [[FCellChatGroup alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"kCell"];
        cell.delegate = self;
    }
    
    Message *message = [self.fetchedController objectAtIndexPath:indexPath];
    cell.message = message;
    
    return cell;
}

@end
