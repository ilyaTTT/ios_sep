//
//  FVCLogin.m
//  FitnessChat
//
//  Created by Arcanite LLC on 10.07.15.
//  Copyright (c) 2015 Arcanite LLC. All rights reserved.
//

#import "FVCLogin.h"
#import "FVCConfirm.h"
#import "FRequestLogin.h"
#import "TutorialViewController.h"
#import "SHSPhoneLibrary.h"


@interface FVCLogin () <UITextFieldDelegate>
{
    UILabel *_codeLabel;
    int _count;
}

@property (strong, nonatomic) SHSPhoneTextField *loginTextField;
@property (strong, nonatomic) UITextField *tfPhone;
@property (strong, nonatomic) UIButton *nextButton;
@property (strong, nonatomic) UIButton *cityButton;
@property BOOL needShowTutorial;

@end


@implementation FVCLogin


#pragma mark - LIFE CYCLE


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self ShowTutorialIfNeed];
    //[self setText:@""];
    //[_tfPhone becomeFirstResponder];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // Set device country code: will help with formatting the textfield depending on inputted telephone number.
    NSLocale *currentLocale = [NSLocale currentLocale];  // Get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    //NSLog(@"Device country code is %@", countryCode);
    //asYouTypeFormatter = [[NBAsYouTypeFormatter alloc] initWithRegionCode:countryCode]; //@"IN"]; // IN is for India.
    
    
    // Defaults.
    self.title = @"Вход";
    //self.view.backgroundColor = [UIColor grayColor];
    //self.view.alpha = 1.0;
    self.backgroundName = @"back_bg"; //@"back_login";
    _count = 13; //10;
    self.cityButton.top = 45;
    [self.view addSubview:self.cityButton];
    
    
    // Top label.
//    UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 45, self.view.width - 30, 40)];
//    topLabel.textColor = [UIColor gray];
//    topLabel.font = [UIFont normal];
//    topLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    topLabel.numberOfLines = 0;
//    topLabel.textAlignment = NSTextAlignmentCenter;
//    topLabel.text = [NSString stringWithFormat:@"Впишите номер телефона."];
//    [self.view addSubview:topLabel];
    
    
    // Smart auto format text field.
    _loginTextField = [[SHSPhoneTextField alloc] initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 60)];
    _loginTextField.backgroundColor = [UIColor whiteColor];
    _loginTextField.keyboardType = UIKeyboardTypeNumberPad;
    _loginTextField.textAlignment = NSTextAlignmentCenter;
    _loginTextField.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _loginTextField.layer.borderWidth = 0.5;
    
    //_loginTextField.top = topLabel.bottom + 25;
    //_loginTextField.delegate = self;

    [_loginTextField setPlaceholder:NSLocalizedString(@"Введите номер телефона", nil)];
//    [_loginTextField.formatter setDefaultOutputPattern:@"+7 (###) ###-##-##"];
    //[_loginTextField.formatter setDefaultOutputPattern:@"# (###) ### ####"];
    [_loginTextField.formatter setDefaultOutputPattern:@"+#############"];
    //_loginTextField.formatter.prefix = @"+"; //@"+7 ";
    //[_loginTextField.formatter addOutputPattern:@"+# (###) ###-##-##" forRegExp:@"^7[0-689]\\d*$" imagePath:nil];//@"flagRU"];
    //[_loginTextField.formatter addOutputPattern:@"+### (##) ###-###" forRegExp:@"^374\\d*$" imagePath:nil];//@"flagAM"];
    
    [self.view addSubview:_loginTextField];
    
    // One pixel line above the keyboard.
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 1.0 / [UIScreen mainScreen].scale)];
    separatorView.backgroundColor = [UIColor lightGrayColor];
    _loginTextField.inputAccessoryView = separatorView;
    
    [_loginTextField becomeFirstResponder];
    
#ifdef DEBUG
    _loginTextField.text = @"+79823020708";
//    _loginTextField.text = @"+79250001251";
#endif
    // Actual text field where the telephone number is inputted.
//    _tfPhone = [[UITextField alloc] initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 60)];
//    _tfPhone.delegate = self;
//    _tfPhone.userInteractionEnabled = YES;
//    _tfPhone.text = @"+";
//    _tfPhone.textAlignment = NSTextAlignmentCenter;
//    _tfPhone.keyboardType = UIKeyboardTypeNumberPad;
//    _tfPhone.top = topLabel.bottom + 25;
//    _tfPhone.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:_tfPhone];
    
    
    // Next button.
    _nextButton = [self roundedButton];
    _nextButton.top = _loginTextField.bottom + 40; //_tfPhone.bottom + 21;
    [_nextButton setTitle:@"Далее"];
    [_nextButton addTarget:self action:@selector(onNext)];
    [self.view addSubview:_nextButton];
}

-(void)ShowTutorialIfNeed
{
    if (!self.needShowTutorial)
        return;
    
    TutorialViewController *ctrl = [TutorialViewController new];
    [self presentViewController:ctrl animated:NO completion:NULL];
    self.needShowTutorial = NO;
}

-(BOOL)needShowTutorial
{
    NSUserDefaults *uDefaults = [NSUserDefaults standardUserDefaults];
    BOOL needShow = [uDefaults boolForKey:@"dontShowTutorial"];
    return !needShow;
}


-(void)setNeedShowTutorial:(BOOL)needShowTutorial
{
    NSUserDefaults *uDefaults = [NSUserDefaults standardUserDefaults];
    [uDefaults setBool:!needShowTutorial forKey:@"dontShowTutorial"];
}

#pragma mark - UITEXTFIELD DELEGATE

/** Called every time a character is inputted into the text field through the keyboard. */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField.text isEqualToString:@""])
    {
        textField.text = @"+";
    }
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > (_count + 1));
    
    return YES;
}


#pragma mark - HELPERS

- (void)selectRange:(NSRange)range
{
    UITextPosition *start = [_tfPhone positionFromPosition:[_tfPhone beginningOfDocument] offset:range.location];
    UITextPosition *end = [_tfPhone positionFromPosition:start offset:range.length];
    [_tfPhone setSelectedTextRange:[_tfPhone textRangeFromPosition:start toPosition:end]];
}

/** ROMAS TECHNIQUE: Called by UITextField every time to run through a simple algorithm of adding brackets. */
- (void)setText:(NSString *)text
{
    // Keeps a limit on character range.
    if (text.length > _count + 2)
    {
        text = [text substringToIndex:_count + 2];
    }
    
    // Organizes brackets accordingly.
    NSString *clear = [[text stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    clear = [NSString stringWithFormat:@"(%@",clear];
    NSMutableString *res = [[NSMutableString alloc] initWithString:clear];
    int d = _count - 6;
    if (clear.length > d)
    {
        [res insertString:@")" atIndex:d];
        _tfPhone.text = res;
        [self selectRange:NSMakeRange(_tfPhone.text.length, 0)];
    }
    else
    {
        [res insertString:@")" atIndex:res.length];
        _tfPhone.text = res;
        [self selectRange:NSMakeRange(_tfPhone.text.length - 1, 0)];
    }
    
    // Hides keyboard if character limit is reached.
    if (text.length == _count + 2)
    {
        [_tfPhone resignFirstResponder];
    }
}

/** Called by clicking the "next" button. */
- (void)onNext
{
    ////NSLog(@"'Next' button tapped.");
    
    [AppDelegate.shared showActivity];

    // Format number by removing unnecessary characters. Necessary for server!
    NSString *res = _loginTextField.text;
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"+()- "]; // Remove all of these characters from string.
    res = [[res componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
    
    // Controller initialization.
    FRequestLogin *request = [FRequestLogin new];
    request.parent = self;
    request.phone = res;
    //NSLog(@"onNext: request.phone %@", request.phone);
    [request start];
}

/** Send inputted number to server. */
- (void)requestCompletion:(NWRequest *)request withObject:(NSDictionary *)dict andError:(NSError *)error
{
    [AppDelegate.shared hideActivity];
    
    FVCConfirm *vc = [FVCConfirm new];
    vc.fullPhone = _loginTextField.text; //_tfPhone.text; // Just to display the inputted number in the next controller.
    vc.phone = ((FRequestLogin *)request).phone; // Contains actual neccessary value that is sent to the server.
    
    //NSLog(@"requestCompletion: vc.phone %@", vc.phone);

    
    if ([dict[@"result"] isEqualToString:@"ok"])
    {
        
    }
    else
    {
        vc.isLogin = YES;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
