//
//  RVDataPicker.m
//  RGS
//
//  Created by user on 08.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "RVDatePicker.h"


@interface RVDatePicker ()

@end


@implementation RVDatePicker

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
        
        //UIDatePicker creation.
        _picker                 = [UIDatePicker new];
        _picker.backgroundColor = [UIColor whiteColor];
        _picker.datePickerMode  = UIDatePickerModeDate;
        _picker.maximumDate     = [NSDate date];
        _picker.width           = self.width;
        
        [self addSubview:_picker];
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)]];
    }
    return self;
}


//Adds UIDatePicker as a subview to the main view.
+(RVDatePicker *)pickerInView:(UIView *)view
{
    RVDatePicker *picker    = [[RVDatePicker alloc] initWithFrame:view.bounds];
    picker.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    picker.alpha = 0;
    
    
//    CGSize pickerSize   = picker.bounds.size;
//    CGSize result       = [[UIScreen mainScreen] bounds].size;
//    if (result.width == 320.0) //iPhone's 4, 5 and 5S
//    {
//        picker.frame = CGRectMake(view.frame.origin.x, view.frame.size.height - pickerSize.height, view.frame.size.width, pickerSize.height);
//    }
//    else if (result.width == 375.0) //iPhone's 6 and 6S
//    {
//        picker.frame = CGRectMake(27.5, view.frame.size.height - pickerSize.height, view.frame.size.width, pickerSize.height);
//    }
//    else if (result.width == 414.0) //iPhone's 6+ and 6S+
//    {
//        picker.frame = CGRectMake(47, view.frame.size.height - pickerSize.height, view.frame.size.width, pickerSize.height);
//    }
    
    
    [view addSubview:picker];
    
    return picker;
    
    
    

    
//    RVDatePicker *picker    = [[RVDatePicker alloc] init]; //[[RVDatePicker alloc] initWithFrame:view.bounds];
//    picker.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth; //| UIViewAutoresizingFlexibleHeight;
//    picker.alpha            = 0;
//    
//    
//    /*UIDatePicker НЕЛЬЗЯ ВЫТИНУТЬ! UIPicker МОЖНА.*/
//    
//    
//    //Set the time picker frame.
//    CGRect screenRect = [view frame];
//    CGSize pickerSize = picker.bounds.size;
//    CGRect pickerRect;
//    
//    CGSize result = [[UIScreen mainScreen] bounds].size;
//    if (result.width == 320.0) //iPhone's 4, 5 and 5S
//    {
//        pickerRect = CGRectMake(screenRect.origin.x, screenRect.size.height - pickerSize.height, screenRect.size.width, pickerSize.height);
//    }
//    else if (result.width == 375.0) //iPhone's 6 and 6S
//    {
//        pickerRect = CGRectMake(27.5, screenRect.size.height - pickerSize.height, screenRect.size.width, pickerSize.height);
//    }
//    else if (result.width == 414.0) //iPhone's 6+ and 6S+
//    {
//        pickerRect = CGRectMake(47, screenRect.size.height - pickerSize.height, screenRect.size.width, pickerSize.height);
//    }
//    
//    [picker setFrame:pickerRect];
//    
//    [view addSubview:picker];
//    
//    return picker;
}

//Hides UIDatePicker upon touching anywhere else on the screen but the picker it self. Also saves date as the one selected in the picker wheel.
-(void)onTap
{
    [_delegate datePicker:self dateSelected:_picker.date];
    [self hide];
}


//Shows UIDatePicker.
-(void)show
{
    _picker.top = self.height;
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1;
        _picker.bottom = self.height;
    }];
}


//Hides UIDatePicker.
-(void)hide
{
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
        _picker.top = self.height;
    }];
}

@end
