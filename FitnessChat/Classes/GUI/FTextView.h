//
//  FTextView.h
//  FitnessChat
//
//  Created by user on 31.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol FTextViewProtocol  <UITextViewDelegate>

- (void)textViewTextChanged:(UITextView *)textView;

@end


@interface FTextView : UITextView

@property (strong, nonatomic) NSString *placeholder;
@property (weak, nonatomic) id<FTextViewProtocol>additionalDelegate;
- (CGFloat)measureHeight;

- (void)clean;
@end
