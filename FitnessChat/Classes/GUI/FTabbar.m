//
//  FTabbar.m
//  FitnessChat
//
//  Created by user on 14.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FTabbar.h"
#import "FVCMainChat.h"
#import "FVCProfile.h"


@interface FTabbar ()

@end

static FTabbar *tabbar = nil;

@implementation FTabbar

+ (void)clean
{
    tabbar = nil;
}

+ (FTabbar *)shared
{
    if (tabbar == nil){
        tabbar = [FTabbar new];
        tabbar.tabBar.translucent = YES;
        tabbar.tabBar.opaque = NO;
    }
    return tabbar;
}
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        FVCMainChat *consultant = [FVCMainChat new];
        consultant.isConsultant = YES;
        consultant.backgroundName = @"back_consultant";
        
        FVCMainChat *groups = [FVCMainChat new];
        groups.backgroundName = @"back_groups";
        
        NSArray *vcs = [NSArray arrayWithObjects:consultant, groups, [FVCProfile new], nil];
        NSArray *titles = [NSArray arrayWithObjects:@"Консультант",@"Сообщество",@"Настройки", nil];
        for (int i = 0; i < vcs.count; i++)
        {
            UIViewController *vc = vcs[i];
            vc.title = titles[i];
            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
            nc.navigationBar.translucent = NO;
            
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"t%d",i + 1]];
            nc.tabBarItem.image = image;
            
            nc.tabBarItem = [[UITabBarItem alloc] initWithTitle:nil image:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[image imageWithRenderingMode:UIImageRenderingModeAutomatic]];

             [nc.tabBarItem setImageInsets:UIEdgeInsetsMake(7, 0, -7, 0)];
            [self addChildViewController:nc];
        }
        

        self.tabBar.backgroundImage = [UIImage imageNamed:@"tb_bg"];
        self.tabBar.selectionIndicatorImage = [UIImage imageNamed:@"tabbar_item_active"];
        
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]
                                                 forState:UIControlStateNormal];
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], NSForegroundColorAttributeName, nil]
                                                 forState:UIControlStateSelected];
        self.tabBar.tintColor = [UIColor whiteColor];
        [self.tabBar setClipsToBounds:YES];
        [self.tabBar setBackgroundColor:[UIColor clearColor]];
        self.tabBar.width += 1;
        
        self.tabBar.hidden = YES;
    }
    return self;
}

+ (void)openChat
{
    if (tabbar != nil)
    {
        FVCMainChat *vc = ((UINavigationController *)tabbar.childViewControllers[0]).childViewControllers[0];
        vc.openChat = YES;
        tabbar.selectedIndex = 0;
    }
}


@end
