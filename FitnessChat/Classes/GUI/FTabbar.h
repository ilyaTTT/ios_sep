//
//  FTabbar.h
//  FitnessChat
//
//  Created by user on 14.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FTabbar : UITabBarController

+ (FTabbar *)shared;
+ (void)clean;
+ (void)openChat;
@end
