//
//  FImageView.m
//  FitnessChat
//
//  Created by user on 31.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FImageView.h"
#import "FVCContainer.h"

@interface FImageView () <UIScrollViewDelegate>
{
    UIImageView *_imageView;
}
@end

@implementation FImageView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.hidden = YES;
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self addSubview:_imageView];
        self.backgroundColor = [UIColor blackColor];
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)]];
        self.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        self.minimumZoomScale = 1.0f;
        self.maximumZoomScale = 3.0f;
        self.delegate = self;
        
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:[UIDevice currentDevice]];
    }
    return self;
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _imageView;
}

- (CGRect)centeredFrameForScrollView:(UIScrollView *)scroll andUIView:(UIView *)rView {
    CGSize boundsSize = scroll.bounds.size;
    CGRect frameToCenter = rView.frame;
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }
    else {
        frameToCenter.origin.x = 0;
    }
    // center vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }
    else {
        frameToCenter.origin.y = 0;
    }
    return frameToCenter;
}

-(void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    _imageView.frame = [self centeredFrameForScrollView:self andUIView:_imageView];
}

- (void)setImage:(UIImage *)image
{
    _imageView.image = image;
    [_imageView sizeToFit];
    _imageView.height = _imageView.height * self.width / _imageView.width;
    _imageView.width = self.width;
     self.contentSize = _imageView.bounds.size;
    _imageView.center = self.center;
}
- (void)layoutSubviews
{
//    _imageView.frame = self.bounds;
}

- (void)onTap
{
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        self.hidden = YES;
        
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        appDelegate.allowedOrientation = UIInterfaceOrientationMaskPortrait;
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    }];
}

- (void)show
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.allowedOrientation = UIInterfaceOrientationMaskAll;
    
    self.hidden = NO;
    self.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1;
    }];
}

-(void) orientationChanged :(NSNotification*)notification
{
    //NSLog(@"picture : orientation changed");
    //NSLog(@"window size : %f %f", [UIApplication sharedApplication].keyWindow.width, [UIApplication sharedApplication].keyWindow.height);
    //NSLog(@"container's view size : %f %f", [FVCContainer shared].view.width, [FVCContainer shared].view.height);
    self.minimumZoomScale = 1.0f;
    self.maximumZoomScale = 3.0f;
    [self setZoomScale:1.0f];
    _imageView.frame = self.bounds;
    self.contentSize = _imageView.bounds.size;
    _imageView.center = self.center;
}

@end
