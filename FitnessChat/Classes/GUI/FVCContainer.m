//
//  FVCContainer.m
//  FitnessChat
//
//  Created by Arcanite LLC on 10/10/15.
//  Copyright © 2015 Arcanite LLC. All rights reserved.
//

#import "FVCContainer.h"
#import "FVCMainChat.h"
#import "FVCChat.h"
#import "FVCProfile.h"
#import "FRequestUserConsiltant.h"
#import "FRequestUserGroup.h"
#import "Common.h"

static FVCContainer *container;
static dispatch_once_t onceT;

/// It's a singleton.
@interface FVCContainer ()

@property (assign, nonatomic) NSInteger         selectedChild;
@property (assign, nonatomic) CGRect            newChildFrame;
@property (strong, nonatomic) NSMutableArray    *children;
@property (assign, nonatomic) BOOL              isUpdatingConsultant;

//
@property (assign, nonatomic) BOOL          isConsultant;
@property (strong, nonatomic) NSString      *name;
@property (strong, nonatomic) NSString      *chatId;
@property (strong, nonatomic) NSString      *consultantImageUrlString;
@property (strong, nonatomic) UIImage       *consultantImage;
@property (strong, nonatomic) NSString      *consultantInfo;
@property (strong, nonatomic) NSString      *consultantId;
@property (strong, nonatomic) NSString      *consultantRole;

@end

@implementation FVCContainer

+(instancetype) shared
{
    if (container == nil)
    {
        @synchronized (self) {
            container = [[FVCContainer alloc] init];
        }
    }
    
    return container;
}

- (void)reset
{
    container = nil;
    onceT = 0;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    _isConsultant = YES;
    [self restore];
    
    _rootScrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    _rootScrollView.scrollEnabled = YES;
    _rootScrollView.pagingEnabled = YES;
    _rootScrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    [self.view addSubview:_rootScrollView];
    
    _children = [[NSMutableArray alloc] initWithCapacity:2];
    _selectedChild = 0;
    _newChildFrame = self.view.bounds;
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateConsultant:) name:NDM_NOTIFICATION_RELOAD_CHAT object:nil];
    
    [self reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [self switchToChild:_selectedChild]; // I don't know why I need this hack
}

#pragma mark - user defaults

#define KEY_NAME @"SETT_KEY_NAME"

-(void)store
{
    [[NSUserDefaults standardUserDefaults] setObject:_name forKey:KEY_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)restore
{
    _name = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_NAME];
    NSDictionary *info = [self infoDict];
    if (info)
        [self parseInfoAtDict:info];
}

-(NSDictionary *)infoDict
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"k%@",_name]];
}

-(void)parseInfoAtDict: (NSDictionary *)info
{
    _consultantId = info[@"id"];
    
    if (_isConsultant)
    {
        _chatId = info[@"chat_id"];
    }
    else
    {
        _chatId = info[@"id"];
    }
    _name = info[@"name"];
    _consultantInfo = info[@"info"];
    _consultantRole = info[@"role"];
    //        _titleLabel.text = info[@"name"];
    //        _descLabel.text = info[@"info"];
    
    NSArray *comp = [_name componentsSeparatedByString:@" "];
    NSString *res = @"";
    for (int i = 0; i < comp.count; i ++)
    {
        NSString *c = comp[i];
        if (c.length > 0)
        {
            res = [res stringByAppendingString:[c substringToIndex:1]];
        }
    }
    //        _nLabel.text = res;
    
    NSString *url = stringValue(info[@"avatar_large_url"]);
    if (url.length > 0)
    {
        //            [self setImage:url];
        self.consultantImageUrlString = url;
    }
    else
    {
        //            _photoView.image = [UIImage imageNamed:@"temp"];
        self.consultantImage = [UIImage imageNamed:@"temp"];
    }
    
    //        [self layout];
    //        _button.hidden = NO;
    //        _photoBG.hidden = NO;
    if (![info[@"avatar_large_url"] isKindOfClass:[NSString class]])
    {
        info = @{@"id":info[@"id"], @"name":info[@"name"],@"info":info[@"info"],@"avatar_large_url":@""};
    }
    [[NSUserDefaults standardUserDefaults] setObject:info forKey:[NSString stringWithFormat:@"k%@",[self name]]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


#pragma mark - Children

- (void)setupChildren
{
    if (!_name)
        _name = @"Консультант";
    
    // add children
    NSManagedObjectContext *context = [RBase shared].moc;
    Chat *chat = [[[RBase shared] objectsForEntity:@"Chat" withPredicate:[NSPredicate predicateWithFormat:@"chatId=%@",_chatId] context:context] lastObject];
    FVCChat *vc;
    
    if (chat == nil)
    {
        chat = [NSEntityDescription insertNewObjectForEntityForName:@"Chat" inManagedObjectContext:context];
        chat.chatId = _chatId;
        [context save:nil];
    }
    
    if (_isConsultant)
    {
        vc = [FVCChat new];
        vc.hidesBottomBarWhenPushed = YES;
        vc.title =_name;
        vc.chat = chat;
        vc.consultantRole = _consultantRole;
//        [self.navigationController pushViewController:vc animated:NO];
    }
    else
    {
        vc = [FVCChatGroup new];
        vc.hidesBottomBarWhenPushed = YES;
        vc.title = _name;
        vc.chat = chat;
//        [self.navigationController pushViewController:vc animated:YES];
    }
    
    if (self.consultantImageUrlString)
        vc.consultantImageUrlString = self.consultantImageUrlString;
    else
        vc.consultnatImage = self.consultantImage;
    vc.name = _name;
    vc.info = _consultantInfo;
    
//    FVCMainChat *consultant = [FVCMainChat new];
//    consultant.isConsultant = YES;
//    consultant.backgroundName = @"back_consultant";
    
    NSArray *vcs = [NSArray arrayWithObjects:vc, [FVCProfile new], nil];
    NSArray *titles = [NSArray arrayWithObjects:_name,@"Профиль", nil];
    for (int i = 0; i < vcs.count; i++)
    {
        UIViewController *vc = vcs[i];
        vc.title = titles[i];
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
        nc.navigationBar.translucent = NO;
        
        [self addChild:nc];
    }
}

- (void)addChild: (UIViewController *)vc
{
    [self addChildViewController:vc];
    
    vc.view.frame = _newChildFrame;
    _newChildFrame = CGRectMake(_newChildFrame.origin.x + _newChildFrame.size.width, _newChildFrame.origin.y, _newChildFrame.size.width, _newChildFrame.size.height);
    [_rootScrollView addSubview:vc.view];
    
    [_children addObject:vc];
    
    [vc didMoveToParentViewController:self];
}

- (void)switchToChild:(NSInteger)childNumber animated:(BOOL)animated
{
    if (childNumber >= self.children.count)
        ; // fixme : throw exception
    //    [self beginAppearanceTransition:YES animated:YES];
  
    _rootScrollView.contentOffset = CGPointMake(_rootScrollView.width * childNumber, _rootScrollView.contentOffset.y);
    _selectedChild = childNumber;
}

- (void)switchToChild:(NSInteger)childNumber
{
    if (childNumber >= self.children.count)
        ; // fixme : throw exception
//    [self beginAppearanceTransition:YES animated:YES];
    UIViewController *oldVc = self.children[_selectedChild];
    UIViewController *newVc = self.children[childNumber];
    
    [oldVc beginAppearanceTransition:NO animated:YES];
    [newVc beginAppearanceTransition:YES animated:YES];
    
    [UIView animateWithDuration:[CATransaction animationDuration] animations:^{
        _rootScrollView.contentOffset = CGPointMake(_rootScrollView.width * childNumber, _rootScrollView.contentOffset.y);
    } completion:^(BOOL finished) {
        [oldVc endAppearanceTransition];
        [newVc endAppearanceTransition];
        _selectedChild = childNumber;
    }];
    
//    [_rootScrollView setContentOffset:CGPointMake(_rootScrollView.width * childNumber, _rootScrollView.contentOffset.y) animated:YES];
//    _selectedChild = childNumber;
}

- (void) replaceChildAtIndex:(NSInteger)childIndex withChild:(UIViewController*)newChild
{
    UIViewController *oldChild = _children[childIndex];
    [oldChild willMoveToParentViewController:nil];
    [self addChildViewController:newChild];
    
//    newChild.view.frame = oldChild.view.frame;
    
    CGRect newViewStartFrame = CGRectMake(0, self.view.height, self.view.width, self.view.height);
    CGRect oldViewEndFrame   = CGRectMake(0, -(self.view.height), self.view.width, self.view.height);
    
    newChild.view.frame = newViewStartFrame;
    CGRect endFrame = oldViewEndFrame;
    
    
    
    [self transitionFromViewController: oldChild toViewController: newChild
                              duration: 0.25 options:0
                            animations:^{
                                // Animate the views to their final positions.
                                newChild.view.frame = oldChild.view.frame;
                                oldChild.view.frame = endFrame;
                            }
                            completion:^(BOOL finished) {
                                // Remove the old view controller and send the final
                                // notification to the new view controller.
                                [oldChild removeFromParentViewController];
                                [newChild didMoveToParentViewController:self];
                                [_children replaceObjectAtIndex:childIndex withObject:newChild];
                            }];
}

- (void) updateConsultant:(NSNotification*)notification
{
    NSString *newConsultantId = notification.object;
    if ([newConsultantId isEqualToString:_consultantId])
        return;
    
    //NSLog(@"old consultant = %@ new consultant = %@", _consultantId, newConsultantId);
    
    _isUpdatingConsultant = YES;
    [self reloadData];
}

#pragma mark - request

- (void)reloadData
{
    [[AppDelegate shared] showActivity];
    
    if (_isConsultant)
    {
        FRequestUserConsiltant *request = [FRequestUserConsiltant new];
        request.parent = self;
        [request start];
    }
    else
    {
        FRequestUserGroup *request = [FRequestUserGroup new];
        request.parent = self;
        [request start];
    }
}

- (void)reloadChat
{
    NSManagedObjectContext *context = [RBase shared].moc;
    
    //Delete old messages.
    NSFetchRequest *allMessages = [[NSFetchRequest alloc] init];
    [allMessages setEntity:[NSEntityDescription entityForName:@"Message" inManagedObjectContext:context]];
    [allMessages setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *messages = [context executeFetchRequest:allMessages error:&error];
    //error handling goes here
    for (NSManagedObject *iMessage in messages) {
        [context deleteObject:iMessage];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //more error handling here
    //
    
    Chat *chat = [[[RBase shared] objectsForEntity:@"Chat" withPredicate:[NSPredicate predicateWithFormat:@"chatId=%@",_chatId] context:context] lastObject];
    FVCChat *vc;
    
    if (chat == nil)
    {
        chat = [NSEntityDescription insertNewObjectForEntityForName:@"Chat" inManagedObjectContext:context];
        chat.chatId = _chatId;
        [context save:nil];
    }
    
    vc = [FVCChat new];
    vc.hidesBottomBarWhenPushed = YES;
    vc.title =_name;
    vc.chat = chat;
    vc.consultantRole = _consultantRole;
    
    if (self.consultantImageUrlString)
        vc.consultantImageUrlString = self.consultantImageUrlString;
    else
        vc.consultnatImage = self.consultantImage;
    vc.name = _name;
    vc.info = _consultantInfo;
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
    nc.navigationBar.translucent = NO;
    
    [self replaceChildAtIndex:0 withChild:nc];
}

- (void)requestCompletion:(NWRequest *)request withObject:(NSDictionary *)dict andError:(NSError *)error
{
    //    if (_isUpdatingConsultant)
    //    {
    //        NSDictionary *info = dict[@"consultantInfo"];
    //
    //        _consultantId = info[@"id"];
    //
    //        _name = info[@"name"];
    //        _consultantInfo = info[@"info"];
    //
    //        _isUpdatingConsultant = NO;
    //        return;
    //    }
    
    if(([request isMemberOfClass:[FRequestUserConsiltant class]] && [dict[@"result"] isEqualToString:@"fail"]) ||
       ([request isMemberOfClass:[FRequestUserConsiltant class]] && error))
    {
        if (!error)
        {
            [self reset];
            AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
            [delegate showLoginViewController];
        }
        else
        {
            [self setupChildren];
        }
        
    }
    else
    {
        NSDictionary *info;
        if (_isConsultant)
            info = dict[@"consultantInfo"];
        else
            info = dict[@"groupInfo"];
        
        if (![dict[@"result"] isEqualToString:@"ok"])
            info = [self infoDict];
        if (info)
            [self parseInfoAtDict:info];
        else
        {
            //        _button.hidden = YES;
            //        _photoBG.hidden = YES;
            //        _titleLabel.text = @"Сообщество";
            //        _descLabel.text = @"Через некоторое время Ваш консультант добавит Вас в групповой чат";
            //        [self layout];
            //        [AppDelegate.shared showErrors:dict[@"errors"]];
        }
        
        if (_isUpdatingConsultant)
        {
            [self reloadChat];
            _isUpdatingConsultant = NO;
        }
        else {
            [self setupChildren];
        }
    }
    [self store];
    [[AppDelegate shared] hideActivity];
}


@end
