//
//  FTextView.m
//  FitnessChat
//
//  Created by user on 31.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FTextView.h"


@interface FTextView () <UITextViewDelegate>

@end


@implementation FTextView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.delegate = self;
    }
    return self;
}

- (void)scrollToCaretInTextView:(UITextView *)textView animated:(BOOL)animated
{
    CGRect rect = [textView caretRectForPosition:textView.selectedTextRange.end];
    rect.size.height += textView.textContainerInset.bottom;
    [textView scrollRectToVisible:rect animated:animated];
}

- (void)textViewDidChange:(UITextView *)textView
{
    [_additionalDelegate textViewTextChanged:self];
    
    if ([textView.text hasSuffix:@"\n"])
    {
        [CATransaction setCompletionBlock:^{
            [self scrollToCaretInTextView:textView animated:NO];
        }];
        
    } else
    {
        [self scrollToCaretInTextView:textView animated:NO];
    }
}

/** Replaces text field characters with nothing. */
- (void)clean
{
    [super setText:@""];
    [self textViewDidChange:self];
}

- (NSString *)text
{
    if ([super.text isEqualToString:_placeholder])
    {
        return @"";
    }
    
    return super.text;
}

/**  */
- (void)setText:(NSString *)text
{
    if (text.length == 0)
    {
        [self showPlaceholder];
        [_additionalDelegate textViewTextChanged:self];
    }
    else
    {
        [super setText:text];
        self.textColor = [UIColor whiteColor];
    }
}

/** Sets placeholder text inside the text field. */
- (void)setPlaceholder:(NSString *)placeholder
{
    _placeholder = placeholder;
    [self showPlaceholder];
}

/** Displays placeholder text inside the text field. */
- (void)showPlaceholder
{
    [super setText:_placeholder];
    self.textColor = [UIColor colorWith256Red:169 green:177 blue:188];
}

-  (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([_additionalDelegate respondsToSelector:@selector(textViewShouldBeginEditing:)])
    {
        [_additionalDelegate textViewShouldBeginEditing:textView];
    }
    if ([super.text isEqualToString:_placeholder])
    {
        [super setText:@""];
        self.textColor = [UIColor whiteColor];
    }
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if ([_additionalDelegate respondsToSelector:@selector(textViewDidEndEditing:)])
    {
        [_additionalDelegate textViewDidEndEditing:textView];
    }
    if (self.text.length == 0)
    {
        [self showPlaceholder];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([_additionalDelegate respondsToSelector:@selector(textViewDidBeginEditing:)])
    {
        [_additionalDelegate textViewDidBeginEditing:textView];
    }
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if ([_additionalDelegate respondsToSelector:@selector(textViewShouldEndEditing:)])
    {
        [_additionalDelegate textViewShouldEndEditing:textView];
    }
    return YES;
}


- (CGFloat)measureHeight
{
    if ([self respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)])
    {
        // This is the code for iOS 7. contentSize no longer returns the correct value, so
        // we have to calculate it.
        //
        // This is partly borrowed from HPGrowingTextView, but I've replaced the
        // magic fudge factors with the calculated values (having worked out where
        // they came from)
        
        CGRect frame = self.bounds;
        
        // Take account of the padding added around the text.
        
        UIEdgeInsets textContainerInsets = self.textContainerInset;
        UIEdgeInsets contentInsets = self.contentInset;
        
        CGFloat leftRightPadding = textContainerInsets.left + textContainerInsets.right + self.textContainer.lineFragmentPadding * 2 + contentInsets.left + contentInsets.right;
        CGFloat topBottomPadding = textContainerInsets.top + textContainerInsets.bottom + contentInsets.top + contentInsets.bottom;
        
        frame.size.width -= leftRightPadding;
        frame.size.height -= topBottomPadding;
        
        NSString *textToMeasure = self.text;
        if ([textToMeasure hasSuffix:@"\n"])
        {
            textToMeasure = [NSString stringWithFormat:@"%@-", self.text];
        }
        
        // NSString class method: boundingRectWithSize:options:attributes:context is
        // available only on ios7.0 sdk.
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
        
        NSDictionary *attributes = @{ NSFontAttributeName: self.font, NSParagraphStyleAttributeName : paragraphStyle };
        
        CGRect size = [textToMeasure boundingRectWithSize:CGSizeMake(CGRectGetWidth(frame), MAXFLOAT)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];
        
        CGFloat measuredHeight = ceilf(CGRectGetHeight(size) + topBottomPadding);
        return measuredHeight;
    }
    else
    {
        return self.contentSize.height;
    }
}
@end
