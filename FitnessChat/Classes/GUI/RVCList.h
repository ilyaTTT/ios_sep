//
//  RVCCountries.h
//  FitnessChat
//
//  Created by user on 14.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//
#import "FVC.h"
@class RVCList;


@protocol RVCListProtocol

- (void)list:(RVCList *)list selectedIndex:(NSInteger)index;

@end


@interface RVCList : FVC

@property (strong, nonatomic) NSArray *dataSource;
@property (weak, nonatomic) id<RVCListProtocol>delegate;

@end
