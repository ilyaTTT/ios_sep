//
//  FVCChooseSubscription.m
//  FitnessChat
//
//  Created by owel on 07/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import "FVCChooseSubscription.h"
#import "FRequestPaymentServices.h"
#import "Subscription.h"
#import "FVCCard.h"

@interface FVCChooseSubscription () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *tableArray;

@property (strong, nonatomic) UITableView *tableView;

@end


@implementation FVCChooseSubscription

-(instancetype)init
{
    self = [super init];
    
    self.backgroundName = @"back_bg"; //@"back_register";
    
    self.navigationItem.title = @"Выберите подписку";
    
    _tableArray = [[NSMutableArray alloc] initWithCapacity:3];
    [self makePaymentServicesRequest];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.backgroundName = @"back_bg"; //@"back_register";
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    _tableView.autoresizingMask = self.view.autoresizingMask;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.view addSubview:_tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
}

#pragma mark - 

-(void) makePaymentServicesRequest
{
    FRequestPaymentServices *request = [FRequestPaymentServices new];
    request.parent = self;
    [request start];
}

-(void) reloadTableData
{
    [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellId"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cellId"];
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = NO;
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        cell.textLabel.textColor = [UIColor gray];
        
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
        [arrow sizeToFit];
        UIView *accessoryView = [[UIView alloc] initWithFrame:arrow.frame];
        accessoryView.width += 10;
        arrow.left = 10;
        [accessoryView addSubview:arrow];
        
        cell.accessoryView = accessoryView;
    }
    
    Subscription *subscription = _tableArray[indexPath.row];
    
    cell.textLabel.text = subscription.subscriptionDescription;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld ₽    ", (long)subscription.subscriptionCost];
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FVCCard *vc = [FVCCard new];
    vc.subscription = _tableArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - NWRequestDelegate

-(void)requestCompletion:(NWRequest *)request withObject:(NSDictionary *)dict andError:(NSError *)error
{
    if ([dict[@"result"] isEqualToString:@"ok"])
    {
        [_tableArray removeAllObjects];
        
        for (NSDictionary *iDict in dict[@"services"])
        {
            Subscription *subscription = [[Subscription alloc] initWithDictionary:iDict];
            [_tableArray addObject:subscription];
        }
        
        [self reloadTableData];
    }
}

@end
