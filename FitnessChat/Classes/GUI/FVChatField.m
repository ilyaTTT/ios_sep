//
//  FVChatField.m
//  FitnessChat
//
//  Created by user on 24.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVChatField.h"
#import "FTextView.h"
#import <AVFoundation/AVFoundation.h>

#define AUDIO_FILE @"MyAudioMemo.m4a"

@interface FVChatField () <FTextViewProtocol, AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
    UILabel *_timeLabel;
    UILabel *_cancelLabel;
    UIImageView *_recordView;
    UILongPressGestureRecognizer *_gesture;
    int _beginX;
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    
    NSTimer *_timer;
    int _time;
    
    CGFloat _defaultHeight;
}

@property (strong, nonatomic) FTextView *textView;
@property (strong, nonatomic) UIView    *voiceView;

@end


@implementation FVChatField

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor colorWith256Red:67 green:74 blue:84];
        _defaultHeight = frame.size.height;
        
        // Actual text field view.
        _textView = [FTextView new];
        _textView.font = [UIFont light:14];
        _textView.placeholder = @"Введите сообщение";
        _textView.additionalDelegate = self;
        _textView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.05];
        _textView.layer.cornerRadius = 10;
        _textView.layer.masksToBounds = YES;
        [self addSubview:_textView];
        
        // Microphone image view.
        _voice = [UIView new];
        _voice.backgroundColor = self.backgroundColor;
        _voiceImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"voice"]];
        _voiceImageView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        [_voiceImageView sizeToFit];
        _voiceImageView.center = _voice.center;
        [_voice addSubview:_voiceImageView];
        
        // Gesture.
        _gesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLong:)];
        _gesture.minimumPressDuration = 0.5;
        [_voice addGestureRecognizer:_gesture];
        self.voiceView.hidden = YES;
        [self addSubview:_voice];
        
        // Plus sign button (for selecting photo/video).
        _attachButton = [UIButton new];
        [_attachButton setImage:[UIImage imageNamed:@"plus"]];
        [_attachButton addTarget:self action:@selector(onAttach)];
        [self addSubview:_attachButton];
        
        // "Отпр." button (for sending typed in text).
        _sendButton = [UIButton new];
        _sendButton.hidden = YES;
        [_sendButton setTitle:@"Отпр."];
        [_sendButton addTarget:self action:@selector(onSend)];
        [self addSubview:_sendButton];
        [self addSubview:self.voiceView];
    }
    return self;
}

/** Tracks long press gesture on the "microphone" button. */
- (void)onLong:(UILongPressGestureRecognizer *)gesture
{
        if (gesture.state == UIGestureRecognizerStateEnded)
        {
            [UIView animateWithDuration:0.2 animations:^{
                self.voiceView.left = _voice.left;
            } completion:^(BOOL finished) {
                self.voiceView.hidden = YES;
            }];
            
            self.voiceView.hidden = YES;
            gesture.enabled = YES;
            [self stopRecord];
        }
        else if (gesture.state == UIGestureRecognizerStateBegan)
        {
            self.voiceView.hidden = NO;
            self.voiceView.left = _voice.left;
            _cancelLabel.left = _timeLabel.right + 5;
            [UIView animateWithDuration:0.2 animations:^{
                self.voiceView.left = 0;
            }];
            _beginX = [gesture locationInView:self].x;
            [self initAudio];
            [self startRecord];
        }
        else if (gesture.state == UIGestureRecognizerStateChanged)
        {
            CGPoint p = [gesture locationInView:self];
            int dx = _beginX - p.x;
            if (dx > 20)
            {
                _cancelLabel.left = _timeLabel.right + 5 - dx / 2;
                if (dx > 150)
                {
                    gesture.enabled = NO;
                    [self cancelRecord];
                    self.voiceView.hidden = YES;
                    gesture.enabled = YES;
                }
            }
            else
            {
                _cancelLabel.left = _timeLabel.right + 5;
            }
        }
}

-(void)layoutSubviews
{
    _sendButton.frame = CGRectMake(self.width - 50, 0, 50, _defaultHeight);
    
    _voice.frame    = CGRectMake(0, 0, 50, _defaultHeight);
    _voice.centerX  = _sendButton.centerX;
    
    _attachButton.frame = CGRectMake(0, 0, 40, _defaultHeight);
    _attachButton.left  = 10; //<-- What the hell is this shit?
    
    _textView.frame = CGRectMake(_attachButton.right + 10, 6, _sendButton.left - _attachButton.right - 20, MIN(MAX(_defaultHeight - 12,[_textView measureHeight]),80));
}


/** Calls 'attachPressed' method from class FVCChat. */
-(void)onAttach
{
    [_delegate attachPressed];
}

/** Rids text field of any characters inside. */
- (void)clean
{
    [_textView clean];
}

/** Sends text to server that was inputted into the text field. */
- (void)onSend
{
    if (_textView.text.length > 0)
    {
        [_delegate chatFieldMesageSend];
        //NSLog(@"Sent a text message.");
    }
}

- (void)textViewTextChanged:(UITextView *)textView
{
    CGFloat height = MIN(MAX(_defaultHeight - 12,[_textView measureHeight]),80);
    if (_textView.height != height)
    {
        _textView.height = height;
        self.height = _textView.height + 12;
        [_delegate heightChangedIsEditing:YES];
    }
    if (_textView.text.length == 0)
    {
        _voice.hidden = NO;
        _voice.alpha = 1;
        _sendButton.alpha = 0;
        _sendButton.hidden = YES;
    }
    else
    {
        _voice.hidden = YES;
        _voice.alpha = 0;
        _sendButton.alpha = 1;
        _sendButton.hidden = NO;
    }
}

- (NSString *)text
{
    return _textView.text;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    _sendButton.hidden = NO;
    _sendButton.alpha = 0;
    [UIView animateWithDuration:0.2 animations:^{
        _sendButton.alpha = 1;
        _voice.alpha = 0;
    } completion:^(BOOL finished) {
        _voice.hidden = YES;
    }];
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if (_textView.text.length == 0)
    {
        _voice.hidden = NO;
        _voice.alpha = 0;
        [UIView animateWithDuration:0.2 animations:^{
            _sendButton.alpha = 0;
            _voice.alpha = 1;
        } completion:^(BOOL finished) {
            _sendButton.hidden = YES;
        }];
    }
    return YES;
}


- (UIView *)voiceView
{
    if (_voiceView == nil)
    {
        _voiceView = [[UIView alloc] initWithFrame:self.bounds];
        _voiceView.backgroundColor = self.backgroundColor;
        _voiceView.width -= 40;
        _voiceView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 60, self.height)];
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.text = @"00:00";
        
        UIView *view = [[UIView alloc] initWithFrame:_timeLabel.frame];
        view.backgroundColor = self.backgroundColor;
        view.left = 0;
        
        _cancelLabel = [[UILabel alloc] initWithFrame:CGRectMake(_timeLabel.right + 5, 0, 150, self.height)];
        _cancelLabel.text = @"Проведите для отмены";
        _cancelLabel.font = [UIFont light:14];
        _cancelLabel.textColor = [UIColor whiteColor];
        
        [_voiceView addSubview:_cancelLabel];
        [_voiceView addSubview:view];
        [_voiceView addSubview:_timeLabel];

        [self addSubview:_voiceView];
    }
    return _voiceView;
}

- (void)stopAllActions
{
    if (_timer != nil)
    {
        [_timer invalidate];
        _timer = nil;
        [self cancelRecord];
    }
}

- (void)startTimer
{
    _time = 0;
    [_timer invalidate];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tick) userInfo:nil repeats:YES];
    _timeLabel.text = stringTime(_time);
    
    [UIView animateWithDuration:0.3 animations:^{
        _timeLabel.alpha = 0.3;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            _timeLabel.alpha = 1;
        }];
    }];
}

- (void)stopTimer
{
    [_timer invalidate];
    _timer = nil;
}

- (void)tick
{
    _time++;
    _timeLabel.text = stringTime(_time);
    
    [UIView animateWithDuration:0.3 animations:^{
        _timeLabel.alpha = 0.3;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            _timeLabel.alpha = 1;
        }];
    }];
}


/** Start audio recording upon touching audio button. */
- (void)startRecord
{
    [self startTimer];
    _timeLabel.text = @"00:00";
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:nil];
    
    // Start recording
    [recorder record];
}


/** Cancels recording. */
-(void)cancelRecord
{
    [self stopTimer];
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}


/** Stops audio recording when finger is released from button. */
-(void)stopRecord
{
    [self stopTimer];
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    path = [path stringByAppendingPathComponent:AUDIO_FILE];
    
    NSData *data = [NSData dataWithContentsOfFile:path];
    [_delegate dataRecored:data withTime:_time];
}


/** Initialize the audio file. */
-(void)initAudio
{
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               AUDIO_FILE,
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate & prepare the recorder.
    recorder                    = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    recorder.delegate           = self;
    recorder.meteringEnabled    = YES;
    [recorder prepareToRecord];
}

@end
