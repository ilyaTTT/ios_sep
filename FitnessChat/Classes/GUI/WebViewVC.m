//
//  WebViewVC.m
//  YandexMoneyTest
//
//  Created by owel on 02/09/15.
//  Copyright (c) 2015 owel. All rights reserved.
//

#import "WebViewVC.h"
#import "AppDelegate.h"
#import "FVCContainer.h"

typedef void(^CheckCompletionBlock)(NSString*, NSError*);

@interface WebViewVC () <UIWebViewDelegate>

@property (strong, nonatomic) UIView    *topView;
@property (strong, nonatomic) UIButton  *backButton;
@property (strong, nonatomic) UIWebView *webView;

@property (copy,   nonatomic) CheckCompletionBlock  checkCompletionBlock;
@property (strong, nonatomic) NSTimer               *checkTimer;

@end

@implementation WebViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
    _topView.backgroundColor = [UIColor lightGrayColor];
    
    _backButton = [UIButton new];
    [_backButton setTitle:@"< Назад" forState:UIControlStateNormal];
    [_backButton sizeToFit];
    _backButton.frame = CGRectMake(10, _topView.frame.size.height / 2 - _backButton.frame.size.height / 2 + 5, _backButton.frame.size.width, _backButton.frame.size.height);
    [_backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, _topView.height, self.view.bounds.size.width, self.view.bounds.size.height - _topView.height)];
    [_webView loadRequest:self.requestToLoad];
    _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _webView.delegate = self;
    
    [self.topView addSubview:_backButton];
    
    [self.view addSubview:_webView];
    [self.view addSubview:_topView];
}

#pragma mark -

-(void) dismissSelf: (BOOL)success
{
    if (self.delegate)
        [self.delegate webViewWillReturn:success];
    [self dismissViewControllerAnimated:YES completion:nil];
    [[FVCContainer shared] switchToChild:1];
}

#pragma mark - buttons

-(void)backButtonPressed: (id)sender
{
    [self dismissSelf:NO];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    BOOL shouldStartLoad = YES;

    //NSLog(@"request: %@", request);
    //NSLog(@"request's url path: %@", request.URL.path);
    if ( [request.URL.absoluteString hasSuffix:@"/rest/payment/term-url"])
    {
        [self dismissSelf:YES];
    }

    return shouldStartLoad;
}

@end
