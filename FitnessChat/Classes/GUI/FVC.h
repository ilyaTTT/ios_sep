//
//  FVC.h
//  FitnessChat
//
//  Created by Arcanite LLC on 10.07.15.
//  Copyright (c) 2015 Arcanite LLC. All rights reserved.
//

#import "FormTextField.h"
#import "NWRequestDelegate.h"

@interface FVC : UIViewController <NWRequestDelegate>

@property (strong, nonatomic) NSString *backgroundName;

- (FormTextField *)phoneTF;
- (FormTextField *)codeTF;
- (FormTextField *)cardNumberTF;
- (FormTextField *)cardDateTF;
- (FormTextField *)cardCVVTF;

- (UITextField *)roundedTextFieldWithLeftView:(UIView *)view;
- (UITextField *)roundedTextFieldWithLeftImage:(UIImage *)image;
- (UIButton *)roundedButton;

- (UIView *)separator;
- (void)back;

- (void) setupTapToRemoveKeyboard;
- (void) cancelTapToRemoveKeyboard;

@end
