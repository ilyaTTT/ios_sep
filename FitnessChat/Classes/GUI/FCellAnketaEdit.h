//
//  FCellAnketaEdit.h
//  FitnessChat
//
//  Created by user on 22.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

@class Question;

@interface FCellAnketaEdit : UITableViewCell

@property (strong, nonatomic) Question *question;
@property (strong, nonatomic) NSString *radioAnswer;
@property (strong, nonatomic) UITextView *textView;
+ (CGFloat)heightForQuestion:(Question *)question;

- (NSString *)answer;

@end
