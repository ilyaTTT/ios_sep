//
//  FCellAnketa.h
//  FitnessChat
//
//  Created by user on 21.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Question;

@interface FCellAnketa : UITableViewCell
@property (strong, nonatomic) Question *question;

+ (CGFloat)heightForQuestion:(Question *)question;
@end
