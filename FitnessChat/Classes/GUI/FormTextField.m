//
//  FormTextField.m
//  RuRu
//
//  Created by Алексей on 21.05.14.
//  Copyright (c) 2014 Work. All rights reserved.
//

#import "FormTextField.h"

@interface FormTextField () <UITextFieldDelegate>
{
    UILabel *_leftViewLabel;
    UIImageView *_cardTypeImageView;
    BOOL _appended;
}
@property (strong, nonatomic) UIButton *captchaButton;

@end


@implementation FormTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _maxLength = 100;
        self.backgroundColor = [UIColor whiteColor];
        _textfield = [UITextField new];
        _textfield.delegate = self;
        _textfield.autocorrectionType = UITextAutocapitalizationTypeNone;
        
        _titleLabel = [UILabel new];
        
        _captchaButton = [UIButton new];
        [_captchaButton addTarget:self action:@selector(updateCaptcha) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_textfield];
        [self addSubview:_titleLabel];
        [self addSubview:_captchaButton];
        
        [self setHorizontalMode:NO];
        [self setBottomSeparatorShow:YES];
    }
    return self;
}

- (void)setCaptchaUrlString:(NSString *)captchaUrlString
{
    if (![captchaUrlString isEqualToString:_captchaUrlString])
    {
        _captchaUrlString = captchaUrlString;
        [self updateCaptcha];
    }
}

- (void)layoutSubviews
{
    _textfield.frame = CGRectMake(0, 0, self.width, self.height);
}




#pragma mark - private

- (void)setPrefix:(NSString *)prefix
{
    UIView * leftView = [[UIView alloc] init];
    leftView.backgroundColor = [UIColor clearColor];
    
    _leftViewLabel = [[UILabel alloc] init];
    _leftViewLabel.font = self.textfield.font;
    _leftViewLabel.textAlignment = NSTextAlignmentRight;
    _leftViewLabel.textColor = [UIColor blackColor];
    _leftViewLabel.backgroundColor = [UIColor clearColor];
    _leftViewLabel.text = [NSString stringWithFormat:@" +%@", prefix];
    [_leftViewLabel sizeToFit];
    
    _leftViewLabel.frame = CGRectOffset(_leftViewLabel.frame, 0, -1);
    leftView.frame = _leftViewLabel.frame;
    [leftView addSubview:_leftViewLabel];
    leftView.width -= 5;
    self.textfield.leftView = leftView;
    self.textfield.leftViewMode = UITextFieldViewModeAlways;

}

- (void)setKeyboardType:(UIKeyboardType)keyboardType
{
    _textfield.keyboardType = keyboardType;
}
- (void)setDescription:(NSString *)description
{
    if ([description isKindOfClass:[NSString class]])
    {
        _textfield.placeholder = description;
    }
}

- (void)setMask:(NSString *)mask
{
    NSString *text = self.text;
    self.text = @"";
    
    _mask = mask;
    if ([_mask isEqualToString:formPhoneMask])
    {
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:mask];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,2)];
        _textfield.attributedPlaceholder = string;
    } else
    if ([_mask isEqualToString:formPhoneMask380] || [_mask isEqualToString:formPhoneMask375])
    {
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:mask];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,4)];
        _textfield.attributedPlaceholder = string;
    }
    if (text.length > 0)
    self.text = text;
}

- (void)setTitle:(NSString *)title
{
    if ([title isKindOfClass:[NSString class]])
    {
        _title = title;
        _titleLabel.text = title;
    }
}

- (void)setText:(NSString *)text
{
    _textfield.text = [self textToMask:text];
    if ([self isFill])
    {
        [_textfieldDelegate formTextfieldFilled:self];
    }
    if (_hasCardDetector)
    {
        if ([text length] > 0 && [[text substringToIndex:1] intValue] == 4)
        {
            _cardTypeImageView.image = [UIImage imageNamed:@"card_visa"];
        }
        else
        {
            _cardTypeImageView.image = [UIImage imageNamed:@"card_mastercard"];
        }
        [self layoutCardDetector];
    }

}

- (NSString *)text
{
    return [self clearStringFromString:_textfield.text];
}

- (void)setCardDate:(NSString *)date
{
    NSString *firstHalf = [date substringToIndex:4];
    NSString *secondHalf = [date substringFromIndex:4];
    
    self.text = [NSString stringWithFormat:@"%@%@",secondHalf,[firstHalf substringFromIndex:2]];
}

- (BOOL)isFill
{
    NSString *text = [self clearStringFromString:_textfield.text];
    
    if (_maxLength == NSNotFound) return YES;
    if ([text length] >= _maxLength)
    {
        return YES;
    }
    return NO;
}

- (BOOL)resignFirstResponder
{
    return [_textfield resignFirstResponder];
}




#pragma mark -methods

- (void)updateCaptcha
{
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_captchaUrlString]]];
    [_captchaButton setImage:image forState:UIControlStateNormal];
    [_captchaButton sizeToFit];
}

- (CGFloat)heightForTextField
{
    if (_captchaUrlString != nil)
    {
        return 35 + _captchaButton.height;
    }
    if (_horizontalMode)
    {
        return 30;
    }
    return 65;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_appearDelegate onResignFirstResponder];
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if (self.text.length == 0)
//    {
//        textField.text = [[self textToMask:[self clearStringFromString:textField.text]] stringByAppendingString:@")"];
//        [self selectRange:NSMakeRange(textField.text.length - 1, 0)];
//        _appended = YES;
//    }
    [_appearDelegate onAppearElement:self];
}

- (void)selectRange:(NSRange)range {
    UITextPosition *start = [_textfield positionFromPosition:[_textfield beginningOfDocument]
                                                 offset:range.location];
    UITextPosition *end = [_textfield positionFromPosition:start
                                               offset:range.length];
    [_textfield setSelectedTextRange:[_textfield textRangeFromPosition:start toPosition:end]];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([[self clearStringFromString:textField.text] length] == 0)
    {
        textField.text = @"";
    }
    if (_maxLength == 100)
    {
        [_textfieldDelegate formTextfieldFilled:self];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string length] == 0) return YES;
 
    NSString *text = [self clearStringFromString:textField.text];
    text = [text stringByAppendingString:string];
    if (_appended)
    {
        text = [text stringByReplacingOccurrencesOfString:@")" withString:@""];
    }
    
    if (_hasCardDetector)
    {
        if ([[text substringToIndex:1] intValue] == 4)
        {
            _cardTypeImageView.image = [UIImage imageNamed:@"card_visa"];
        }
        else
        {
            _cardTypeImageView.image = [UIImage imageNamed:@"card_mastercard"];
        }
        [self layoutCardDetector];
    }
    
    if ([text length] >= _maxLength)
    {
        text = [text substringToIndex:_maxLength];
        [_appearDelegate onResignFirstResponder];
        [_textfield resignFirstResponder];
    }
    
    textField.text = [self textToMask:text];
    
    NSArray *comp = [textField.text componentsSeparatedByString:@"("];
    NSArray *comp2 = [textField.text componentsSeparatedByString:@")"];
    if (comp.count > 1 && comp2.count != 2)
    {
        textField.text = [textField.text stringByAppendingString:@")"];
        _appended = YES;
         [self selectRange:NSMakeRange(textField.text.length - 1, 0)];
    }
    
    if ([self isFill])
    {
        [_textfieldDelegate formTextfieldFilled:self];
    }
    return NO;
}

- (NSString *)textToMask:(NSString *)text
{
    if (_mask == nil) return text;

    NSMutableString *resString = [[NSMutableString alloc] initWithString:_mask];
    int i;
    
    for (i = 0; i < [text length]; i++)
    {
        NSString *simbol = [text substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [resString rangeOfString:@"_"];
        if (range.location != NSNotFound)
        {
            [resString replaceCharactersInRange:range withString:simbol];
        }
        else
        {
            break;
        }
    }
    
    NSRange range = [resString rangeOfString:@"_"];
    if (range.location != NSNotFound)
    {
        return [resString substringToIndex:range.location];
    }
    
    return resString;
}

- (NSString *)clearStringFromString:(NSString *)string
{
    if (_mask != nil)
    {
        NSString *resString = @"";
        for (int i = 0; i < MIN([_mask length],[string length]); i++)
        {
            NSString *simbolMask = [_mask substringWithRange:NSMakeRange(i, 1)];
            NSString *simbol = [string substringWithRange:NSMakeRange(i, 1)];
            
            if (![simbol isEqualToString:simbolMask])
            {
                resString = [resString stringByAppendingString:simbol];
            }
        }
        return resString;
    }
    
    return string;
}

- (void)setBottomSeparatorShow:(BOOL)isShow
{
    if (isShow)
    {
        _bottomSeparator = [[UIView alloc] initWithFrame:CGRectMake(10, self.height - 1, self.width - 20, 1)];
        _bottomSeparator.backgroundColor = [UIColor colorWith256Red:200 green:199 blue:204];
        [self addSubview:_bottomSeparator];
    }
    else
    {
        [_bottomSeparator removeFromSuperview];
    }
}

- (void)setHasCardDetector:(BOOL)hasCardDetector
{
    _hasCardDetector = hasCardDetector;
    _cardTypeImageView = [UIImageView new];
}

- (void)layoutCardDetector
{
    [_cardTypeImageView sizeToFit];
    _cardTypeImageView.right = self.width - 2;
    _cardTypeImageView.centerY = _textfield.centerY;
    if (_textfield.right > _cardTypeImageView.left - 5)
    {
        _textfield.right -= ((_textfield.right - _cardTypeImageView.left) + 10);
    }
    [self addSubview:_cardTypeImageView];
}

- (BOOL)becomeFirstResponder
{
    return [_textfield becomeFirstResponder];
}


@end
