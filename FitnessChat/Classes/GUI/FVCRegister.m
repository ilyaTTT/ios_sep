//
//  FVCRegister.m
//  FitnessChat
//
//  Created by user on 10.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVCRegister.h"
#import "AlertHelpers.h"
#import "RVDatePicker.h"
#import "FRequestRegister.h"
#import "FRequestPushTokenSave.h"
#import "FTabbar.h"
#import "File.h"
#import "FVCContainer.h"


@interface FVCRegister () <UITableViewDataSource, UITableViewDelegate, RVDatePickerProtocol, UIActionSheetDelegate, UITextFieldDelegate>
{
    UIButton *_addPhotoButton;
    RVDatePicker *_picker;
    File *_imageFile;
}

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UITextField *tfName;
@property (strong, nonatomic) UILabel *dateLabel;
@property (strong, nonatomic) UILabel *maleLabel;

@end


@implementation FVCRegister

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // Defaults.
    self.title = @"Регистрация";
    self.backgroundName = @"back_bg";
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:_tableView];
    
    _picker = [RVDatePicker pickerInView:self.view];
    _picker.delegate = self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Далее" style:UIBarButtonItemStylePlain target:self action:@selector(onNext)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)onNext
{
    if (_tfName.text.length == 0)
    {
        [AppDelegate.shared showErrors:@[@"Введите имя"]];
        return;
    }
    if (_maleLabel.text.length == 0)
    {
        [AppDelegate.shared showErrors:@[@"Выберите пол"]];
        return;
    }
    if (_dateLabel.text.length == 0)
    {
        [AppDelegate.shared showErrors:@[@"Выберите дату рождения"]];
        return;
    }
    if (_addPhotoButton.imageView.image == nil)
    {
        [AppDelegate.shared showErrors:@[@"Установите свою фотографию"]];
        return;
    }

    [[AppDelegate shared] showActivity];
    
    
    File *bigFile = [NSEntityDescription insertNewObjectForEntityForName:@"File" inManagedObjectContext:[RBase shared].moc];
    UIImage *bigImage = cropNScaleImageToSize(_addPhotoButton.imageView.image, CGSizeMake(180, 200));
    bigFile.data = UIImageJPEGRepresentation(bigImage, 0.7);
    
    File *smallFile = [NSEntityDescription insertNewObjectForEntityForName:@"File" inManagedObjectContext:[RBase shared].moc];
    UIImage *smallImage = cropNScaleImageToSize(_addPhotoButton.imageView.image, CGSizeMake(48, 48));
    smallFile.data = UIImageJPEGRepresentation(smallImage, 0.7);
    
   // [[NBS3Uploader uploader] setDelegate:weakSelf];
    
    void (^completion)() = ^ {
            FRequestRegister *request = [FRequestRegister new];
            request.parent = self;
            request.isMale = [_maleLabel.text isEqualToString:@"Мужской"];
            request.birthday = _picker.picker.date;
            request.name = _tfName.text;
            request.smallImage = smallFile;
            request.bigImage = bigFile;
            
            [request start];
            
            [[FRequestPushTokenSave new] start];
    };
    
    __block BOOL isSmallIMGReady = 0;
    __block BOOL isBigIMGReady = 0;
    
    [[NBS3Uploader uploader] uploadFile:smallFile completion:^(BOOL success) {
        isSmallIMGReady = YES;
        
        if(isSmallIMGReady && isBigIMGReady)
        {
            completion();
        }

    }];
    [[NBS3Uploader uploader] uploadFile:bigFile completion:^(BOOL success) {
        isBigIMGReady = YES;
        
        if(isSmallIMGReady && isBigIMGReady)
        {
            completion();
        }

    }];
}



- (void)requestCompletion:(NWRequest *)request withObject:(NSDictionary *)dict andError:(NSError *)error
{
    [AppDelegate.shared hideActivity];
    if ([dict[@"result"] isEqualToString:@"ok"])
    {
        [User setCurrUser:dict[@"userInfo"]];
        [User currentUser].photo = ((FRequestRegister *)request).bigImage;
        [[RBase shared] saveMain];
//        [self presentViewController:[FTabbar shared] animated:YES completion:nil];
//        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            appDelegate.window.rootViewController = [FVCContainer shared];
            [appDelegate.window makeKeyAndVisible];
//        }];
    
    }
    else
    {
        [AppDelegate.shared showErrors:dict[@"errors"]];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [UITableViewCell new];
    cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    UIColor *gray = [UIColor gray];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = [UIFont normal];
    titleLabel.textColor = gray;
    
    if (indexPath.row == 0)
    {
        titleLabel.text = @"Ваше имя";
        self.tfName.frame = CGRectMake(0, 0, 130, cell.height);
        _tfName.font = [UIFont normal];
        _tfName.right = self.view.width - 15;
        _tfName.textColor = gray;
        _tfName.textAlignment = NSTextAlignmentRight;
        _tfName.placeholder = @"Введите имя";
        _tfName.delegate = self;

        [cell addSubview:_tfName];
    }
    else
        if (indexPath.row == 1)
        {
            titleLabel.text = @"Дата рождения";
            
            self.dateLabel.frame = CGRectMake(0, 0, 130, cell.height);
            _dateLabel.textAlignment = NSTextAlignmentRight;
            _dateLabel.font = [UIFont normal];
            _dateLabel.textColor = gray;
            _dateLabel.right = self.view.width - 40;
            [cell addSubview:_dateLabel];

            
            UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
            [arrow sizeToFit];
            cell.accessoryView = arrow;
        }
        else
            if (indexPath.row == 2)
            {
                 titleLabel.text = @"Пол";

                self.maleLabel.frame = CGRectMake(0, 0, 130, cell.height);
                _maleLabel.textAlignment = NSTextAlignmentRight;
                _maleLabel.font = [UIFont normal];
                _maleLabel.textColor = gray;
                _maleLabel.right = self.view.width - 40;
                [cell addSubview:_maleLabel];
                
                UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
                [arrow sizeToFit];
                cell.accessoryView = arrow;
            }
    else
    {
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:@"Создавая аккаунт вы принимаете условия Пользовательского соглашения."];
        [text addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(39, 29)];
        cell.textLabel.font = [UIFont light:14];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.attributedText = text;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
        
        return cell;
    }
    
    [titleLabel sizeToFit];
    titleLabel.height = cell.height;
    titleLabel.left = 15;
    titleLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
    [cell addSubview:titleLabel];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 3)
    {
        return 60;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return self.topView.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.topView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0)
    {
        [_tfName becomeFirstResponder];
    }
    else
        if (indexPath.row == 1)
        {
            [_tfName resignFirstResponder];
            [_picker show];
        }
    else
        if (indexPath.row == 2)
        {
            UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Выберите пол" delegate:self cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles:@"Мужской",@"Женский", nil];
            [sheet showInView:self.view];
        }
    else
    {
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://stage-zone.fitconsultant.arcanite.ru/oferta.html"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://1fitchat.ru/wp-content/themes/fitchat%20IV/docs/02.06.15_Dogovor.pdf"]];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        _maleLabel.text = @"Мужской";
    }
    else
        if (buttonIndex == 1)
        {
            _maleLabel.text = @"Женский";
        }
}

- (void)datePicker:(RVDatePicker *)picker dateSelected:(NSDate *)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd.MM.yyyy"];
    _dateLabel.text = [formatter stringFromDate:date];
}

- (void)onPhotoButton
{
    [AlertHelpers showImagePickerFromVC:self usingTabbar:NO onSuccess:^(UIImage *image) {
        [_addPhotoButton setImage:cropNScaleImageToSize(image, USER_IMAGE_SIZE)];
    }];

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return true;
}


#pragma mark - private

- (UIView *)topView
{
    if (_topView == nil)
    {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 280)];
        
        UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.width, 25)];
        topLabel.textAlignment = NSTextAlignmentCenter;
        topLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:22];
        topLabel.text = @"Давайте знакомиться!";
        [_topView addSubview:topLabel];

        _addPhotoButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 140, 140)];
        _addPhotoButton.layer.cornerRadius = 70;
        _addPhotoButton.layer.masksToBounds = YES;
        _addPhotoButton.layer.borderWidth = 2;
        _addPhotoButton.layer.borderColor = [UIColor blue].CGColor;
        [_addPhotoButton setTitle:@"Добавить\nфото"];
        _addPhotoButton.titleLabel.font = [UIFont fontWithName:@"HelvaeticaNeue" size:20];
        _addPhotoButton.titleLabel.numberOfLines = 2;
        _addPhotoButton.centerX = _topView.width / 2;
        _addPhotoButton.centerY = (_topView.height + topLabel.bottom) / 2;
        _addPhotoButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        _addPhotoButton.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
        [_addPhotoButton setTitleColor:[UIColor blue]];
        [_addPhotoButton addTarget:self action:@selector(onPhotoButton)];
        [_topView addSubview:_addPhotoButton];
    }
    return _topView;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    _tableView.height = self.view.height - keyboardSize.height;
}

- (void)keyboardWillHide:(NSNotification *)n
{
    _tableView.height = self.view.height;
}

- (UITextField *)tfName
{
    if (_tfName == nil)
    {
        _tfName = [UITextField new];
        _tfName.text = [User currentUser].name;
    }
    return _tfName;
}

- (UILabel *)maleLabel
{
    if (_maleLabel == nil)
    {
        _maleLabel = [UILabel new];
        if ([[User currentUser].sex isEqualToString:@"male"])
        {
            _maleLabel.text = @"Мужской";
        }
        else
        {
            _maleLabel.text = @"Женский";
        }
    }
    return _maleLabel;
}

- (UILabel *)dateLabel
{
    if (_dateLabel == nil)
    {
        _dateLabel = [UILabel new];
        if ([User currentUser].birthday.timeIntervalSince1970 > 0)
        {
            NSDateFormatter *formatter = [NSDateFormatter new];
            [formatter setDateFormat:@"dd.MM.yyyy"];
            _dateLabel.text = [formatter stringFromDate:[User currentUser].birthday];
            _picker.picker.date = [User currentUser].birthday;
        }
    }
    return _dateLabel;
}
@end
