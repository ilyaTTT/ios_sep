//
//  FVCAnketaEdit.m
//  FitnessChat
//
//  Created by user on 22.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVCAnketaEdit.h"
#import "Anketa.h"
#import "Question.h"
#import "FCellAnketaEdit.h"
#import "Question.h"
#import "FRequestAnketaSendAnswer.h"


@interface FVCAnketaEdit ()<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UITextViewDelegate>
{
    NSIndexPath *_editingIndexPath;
    UIEdgeInsets _temp;
}
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedController;
@property (strong, nonatomic) NSMutableArray *cells;

@end

@implementation FVCAnketaEdit

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = _anketa.name;
     self.view.backgroundColor = [UIColor colorWith256Red:244 green:246 blue:245];
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:_tableView];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Готово" style:UIBarButtonItemStylePlain target:self action:@selector(onSave)];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)onSave
{
    for (int i = 0; i< _cells.count; i++)
    {
        Question *quest = [_fetchedController objectAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        FCellAnketaEdit *cell = _cells[i];
        quest.answer = cell.answer;
    }
    //NSLog(@"!!!%d",_cells.count);
    [[[RBase shared] moc] save:nil];
    [self.view endEditing:true];
    
    FRequestAnketaSendAnswer *request = [FRequestAnketaSendAnswer new];
    request.parent = self;
    request.anketa = _anketa;
    [request start];
    
    [[AppDelegate shared] showActivity];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([self.fetchedController performFetch:nil])
    {
        [_tableView reloadData];
    }
    _tableView.height = self.view.height - [FTabbar shared].tabBar.height;
}

- (void)requestCompletion:(NWRequest *)request withObject:(NSDictionary *)dict andError:(NSError *)error
{
    [[AppDelegate shared] hideActivity];
    if ([request isKindOfClass:[FRequestAnketaSendAnswer class]])
    {
        if ([dict[@"result"] isEqualToString:@"ok"])
        {
//            [AppDelegate.shared showTopText:@"Изменения сохранены."];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [AppDelegate.shared showErrors:dict[@"errors"]];
            return;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Question *quest = [self.fetchedController objectAtIndexPath:indexPath];
    return [FCellAnketaEdit heightForQuestion:quest];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.fetchedController fetchedObjects].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self cellForIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - fetchedController delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [_tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                      withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [_tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                      withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            
            break;
            
        case NSFetchedResultsChangeMove:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
        {
            [_tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete:

            [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
            
        case NSFetchedResultsChangeUpdate:
        {
        }
            break;
            
        case NSFetchedResultsChangeMove:
        {
            [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [_tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}

- (NSFetchedResultsController *)fetchedController
{
    if (_fetchedController == nil)
    {
        NSManagedObjectContext *context = [RBase shared].moc;
        
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Question"];
        request.predicate = [NSPredicate predicateWithFormat:@"anketa=%@",_anketa];
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        NSArray *sortDescriptors = @[sortDescriptor1];
        [request setSortDescriptors:sortDescriptors];
        
        _fetchedController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        _fetchedController.delegate = self;
    }
    
    return _fetchedController;
}

- (FCellAnketaEdit *)cellForIndexPath:(NSIndexPath *)indexPath
{
    if (_cells == nil)
    {
        _cells = [NSMutableArray new];
    }
    
    if (indexPath.row >= _cells.count)
    {
        FCellAnketaEdit *cell = [[FCellAnketaEdit alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"kCell"];
        Question *quest = [self.fetchedController objectAtIndexPath:indexPath];
        cell.question = quest;
        [_cells addObject:cell];
        cell.textView.delegate = self;
    }
    
    return _cells[indexPath.row];
}


//Shows keyboard upon clicking on the text field.
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    _temp = self.tableView.contentInset;
    UIEdgeInsets contentInsets;

    contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0);
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    [self.tableView scrollToRowAtIndexPath:_editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    self.tableView.contentInset             = _temp;
    self.tableView.scrollIndicatorInsets    = UIEdgeInsetsZero;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    _editingIndexPath = [NSIndexPath indexPathForRow:[_cells indexOfObject:textView.superview.superview] inSection:0];
    return YES;
}
@end
