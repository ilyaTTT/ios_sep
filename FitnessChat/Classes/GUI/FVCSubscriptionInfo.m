//
//  FVCSubscriptionInfo.m
//  FitnessChat
//
//  Created by owel on 11/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import "FVCSubscriptionInfo.h"
#import "FVCChooseSubscription.h"

@interface FVCSubscriptionInfo () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView   *tableView;
@property (strong, nonatomic) UIButton      *extendButton;

@end

@implementation FVCSubscriptionInfo

- (void)viewDidLoad {
    [super viewDidLoad];

    self.backgroundName = @"back_bg"; //@"back_register";
    
    // footer
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 100)];
    footerView.backgroundColor = [UIColor clearColor];
    
    _extendButton = [self roundedButton];
    _extendButton.centerX = footerView.width / 2;
    _extendButton.bottom = footerView.height - 15;
    [_extendButton setTitle:@"Продлить"];
    [_extendButton addTarget:self action:@selector(extendButtonPressed:)];
    
    [footerView addSubview:_extendButton];
    
    // table view
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    _tableView.autoresizingMask = self.view.autoresizingMask;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.bounces = NO;
    _tableView.allowsSelection = NO;
    _tableView.tableFooterView = footerView;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    [self.view addSubview:_tableView];
}

#pragma mark - Buttons

- (void)extendButtonPressed: (id)sender
{
    FVCChooseSubscription *vc = [FVCChooseSubscription new];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.layoutMargins = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = NO;
    cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    cell.textLabel.font = [UIFont normal];
    cell.textLabel.textColor = [UIColor gray];
    
    cell.detailTextLabel.font = [UIFont normal];
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    
    if (indexPath.row == 0)
    {
        cell.textLabel.text = @"Номер телефона";
//        cell.detailTextLabel.text = [User currentUser].phone;
        NSString *phone = [User currentUser].phone;
        NSString *countryPart = [phone substringToIndex:1];
        NSString *codePart = [phone substringWithRange:NSMakeRange(1, 3)];
        NSString *numberPart = [NSString stringWithFormat:@"%@-%@-%@", [phone substringWithRange:NSMakeRange(4, 3)], [phone substringWithRange:NSMakeRange(7, 2)], [phone substringWithRange:NSMakeRange(9, 2)]];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"+%@ (%@) %@", countryPart, codePart, numberPart];
    }
    else if (indexPath.row == 1)
    {
        cell.textLabel.text = @"Подписка истекает";
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd.MM.yyyy";
        cell.detailTextLabel.text = [dateFormatter stringFromDate:[User currentUser].paidUntil];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate



@end
