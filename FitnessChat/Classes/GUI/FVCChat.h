//
//  FVCChat.h
//  FitnessChat
//
//  Created by user on 22.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVC.h"


@interface FVCChat : FVC

@property (strong, nonatomic) Chat *chat; // Core Data's NSManagedObject.

@property (strong, nonatomic) NSString      *consultantImageUrlString;
@property (strong, nonatomic) UIImage       *consultnatImage;
@property (strong, nonatomic) NSString      *consultantRole;
@property (strong, nonatomic) NSString      *name;
@property (strong, nonatomic) NSString      *info;

@end



@interface FVCChatGroup : FVCChat

@end
