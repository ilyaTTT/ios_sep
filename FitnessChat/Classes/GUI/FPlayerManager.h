//
//  FPlayerManager.h
//  FitnessChat
//
//  Created by Алексей on 30.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FPlayerManagerProtocol

-(void)updateTime:(float)time progress:(float)progress isPlaying:(BOOL)isPlaying;

@end

@interface FPlayerManager : NSObject

+ (FPlayerManager *)shared;

-(void)playFile:(File *)file forTarget:(id<FPlayerManagerProtocol>)target;
-(void)prooveFile:(File *)file forTarget:(id<FPlayerManagerProtocol>)target;

@end
