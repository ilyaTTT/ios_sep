//
//  FRequestMessageSend.m
//  FitnessChat
//
//  Created by user on 24.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FRequestMessageSend.h"

@implementation FRequestMessageSend

- (NSString *)urlString
{
    NSString *string = [super urlString];
    return [string  stringByAppendingFormat:@"/rest/chat/send-message"];
}

- (NSString *)body
{
    return [NSString stringWithFormat:@"%@&room_id=%@&type=%@&text=%@&local_id=%d",[self accessTokenString],_chat.chatId,@"text",_text, (int)self.backgroundSenderId];
}

- (NSString *)method
{
    return @"POST";
}

- (void)setResponse:(NSDictionary*)response
{
    [super setResponse:response];
}

-(BackgroundSenderInfo *)createBackgroundSenderInfo
{
    BackgroundSenderInfo *info = [BackgroundSenderInfo new];
    info.type = BITMessage;
    return info;
}
@end
