//
//  FVCRegister.m
//  FitnessChat
//
//  Created by user on 10.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVCProfile.h"
#import "AlertHelpers.h"
#import "RVDatePicker.h"
#import "FRequestUserGet.h"
#import "FRequestRegister.h"
#import "FRequestAnkets.h"
#import "Anketa.h"
#import "Replies.h"
#import "Question.h"
#import "FVCAnketa.h"
#import "File.h"
#import "FVCChooseSubscription.h"
#import "FVCContainer.h"
#import "FVCSubscriptionInfo.h"

#import "FRequestAnketaGetAnswer.h"

@interface FVCProfile () <UITableViewDataSource, UITableViewDelegate, RVDatePickerProtocol, UIActionSheetDelegate, UITextFieldDelegate, NSFetchedResultsControllerDelegate>
{
    UIButton *_addPhotoButton;
    BOOL _imageChanged;
    BOOL _firstLoad;
}
@property (strong, nonatomic) RVDatePicker *picker;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UITextField *tfName;
@property (strong, nonatomic) UILabel *dateLabel;
@property (strong, nonatomic) UILabel *maleLabel;
@property (strong, nonatomic) UILabel *cityLabel;
@property (strong, nonatomic) NSFetchedResultsController *fetchedController;

@end

@implementation FVCProfile

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Профиль";
    self.backgroundName = @"back_bg"; //@"back_register";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:NDM_NOTIFICATION_UPDATE_PROFILE object:nil];
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _tableView.delegate     = self;
    _tableView.dataSource   = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    _tableView.sectionFooterHeight = 0;
    [self.view addSubview:_tableView];
    
    _firstLoad = YES;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Сохранить" style:UIBarButtonItemStylePlain target:self action:@selector(onSave)];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    // navigation bar
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"t1"] style:UIBarButtonItemStylePlain target:self action:@selector(chatButtonPressed:)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    //
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (_firstLoad)
    {
        [self reloadData];
    }
//    _tableView.height = self.view.height - [FTabbar shared].tabBar.height;
    
    // owel's temporary
    
    [FTabbar shared].tabBar.hidden = YES;
    [FTabbar shared].tabBar.alpha = 0;
    _tableView.height = self.view.height;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
}

- (void)reloadData
{
    _firstLoad = NO;
    FRequestUserGet *request = [FRequestUserGet new];
    request.parent = self;
    request.userId = [User currentUser].userId;
    [request start];
    
    if ([self.fetchedController performFetch:nil])
    {
        [_tableView reloadData];
    }
}

- (void) reloadTableView
{
    [_tableView reloadData];
}




#pragma mark -

-(void) chatButtonPressed: (id)sender
{
//    [FTabbar shared].selectedIndex = 0;
    [[FVCContainer shared] switchToChild:0];
    
}

- (void)onExit
{
    [[AppDelegate shared] logout];
}

- (void)onSave
{
    if (_tfName.text.length == 0)
    {
        [AppDelegate.shared showErrors:@[@"Введите имя"]];
        return;
    }
    if (_maleLabel.text.length == 0)
    {
        [AppDelegate.shared showErrors:@[@"Выберите пол"]];
        return;
    }
    if (_dateLabel.text.length == 0)
    {
        [AppDelegate.shared showErrors:@[@"Выберите дату рождения"]];
        return;
    }
    
    
    [AppDelegate.shared showActivity];
    
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            File *bigFile;
            File *smallFile;
            
            User *user = [User currentUser];
            if (user.photo == nil)
            {
                bigFile = [NSEntityDescription insertNewObjectForEntityForName:@"File" inManagedObjectContext:[RBase shared].moc];
                user.photo = bigFile;
            }
            else
            {
                [bigFile clear];
                bigFile = user.photo;
            }
            if (user.smallPhoto == nil)
            {
                smallFile = [NSEntityDescription insertNewObjectForEntityForName:@"File" inManagedObjectContext:[RBase shared].moc];
                user.smallPhoto = bigFile;
            }
            else
            {
                [smallFile clear];
                smallFile = user.smallPhoto;
            }
            
            if (_imageChanged)
            {
                _imageChanged = NO;
                UIImage *bigImage = cropNScaleImageToSize(_addPhotoButton.imageView.image, CGSizeMake(180, 200));
                bigFile.data = UIImageJPEGRepresentation(bigImage, 0.7);
                
                UIImage *smallImage = cropNScaleImageToSize(_addPhotoButton.imageView.image, CGSizeMake(48, 48));
                smallFile.data = UIImageJPEGRepresentation(smallImage, 0.7);
                
                [[NBS3Uploader uploader] uploadFile:bigFile];
                [[NBS3Uploader uploader] uploadFile:smallFile];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                FRequestRegister *request = [FRequestRegister new];
                request.parent = self;
                request.isMale = [_maleLabel.text isEqualToString:@"Мужской"];
                request.birthday = self.picker.picker.date;
                request.name = _tfName.text;
                request.smallImage = smallFile;
                request.bigImage = bigFile;
                
                [request start];
            });
        });
}

- (void)requestCompletion:(NWRequest *)request withObject:(NSDictionary *)dict andError:(NSError *)error
{
    if ([request isKindOfClass:[FRequestAnkets class]])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSArray *ankets = (NSArray *)dict;
            NSManagedObjectContext *context = [[RBase shared] backgroundMOC];
            
            [context performBlock:^{
                for (NSDictionary *anketaDict in ankets)
                {
                    NSNumber *anketaId = numberValue(anketaDict[@"id"]);
                    Anketa *anketa = [[[RBase shared] objectsForEntity:@"Anketa" withPredicate:[NSPredicate predicateWithFormat:@"anketaId=%@",anketaId] context:context] lastObject];
                    if (anketa == nil)
                    {
                        anketa = [NSEntityDescription insertNewObjectForEntityForName:@"Anketa" inManagedObjectContext:context];
                        anketa.anketaId = anketaId;
                    }
                    anketa.name = anketaDict[@"name"];

                    NSArray *questionsArr = anketaDict[@"questions"];
                    
                    for (NSDictionary *questDict in questionsArr)
                    {
                        NSNumber *questId = numberValue(questDict[@"id"]);
                        Question *quest = [[[RBase shared] objectsForEntity:@"Question" withPredicate:[NSPredicate predicateWithFormat:@"questionId=%@ and anketa=%@",questId,anketa] context:context] lastObject];
                        if (quest == nil)
                        {
                            quest = [NSEntityDescription insertNewObjectForEntityForName:@"Question" inManagedObjectContext:context];
                            quest.questionId = questId;
                            [anketa addQuestionsObject:quest];
                        }
                        quest.name = questDict[@"name"];
                        quest.type = questDict[@"type"];
                        
                        if (![quest.type isEqualToString:@"text"])
                        {
                            for (NSDictionary *replyDict in questDict[@"replies"])
                            {
                                NSString *replyId = stringValue(replyDict[@"id"]);
                                Replies *reply = [[[RBase shared] objectsForEntity:@"Replies" withPredicate:[NSPredicate predicateWithFormat:@"repliesId=%@ and question=%@",replyId,quest] context:context] lastObject];
                                if (reply == nil)
                                {
                                    reply = [NSEntityDescription insertNewObjectForEntityForName:@"Replies" inManagedObjectContext:context];
                                    reply.repliesId = replyId;
                                    [quest addRepliesObject:reply];
                                }
                                reply.text = replyDict[@"text"];
                            }
                        }
                    }
                }
                [context save:nil];
            }];
            dispatch_async(dispatch_get_main_queue(), ^{
                FRequestAnketaGetAnswer *request = [FRequestAnketaGetAnswer new];
                request.parent = self;
                [request start];
            });
        });
        return;
    }
    else
      if ([request isKindOfClass:[FRequestAnketaGetAnswer class]])
      {
          if ([dict[@"result"] isEqualToString:@"ok"])
          {
              [[RBase shared] fillReplies:dict];
          }
          else
          {
              [AppDelegate.shared showErrors:dict[@"errors"]];
              _firstLoad = YES;
          }
          return;
      }
    
    [AppDelegate.shared hideActivity];
    
    if ([request isKindOfClass:[FRequestRegister class]])
    {
        if ([dict[@"result"] isEqualToString:@"ok"])
        {
            [[AppDelegate shared] showTopText:@"Изменения сохранены"];
        }
        else
        {
            [AppDelegate.shared showErrors:dict[@"errors"]];
            _firstLoad = YES;
        }
    }
    else
    {
        if ([dict[@"result"] isEqualToString:@"ok"])
        {
            //NSLog(@"user dict:\n%@", dict);
            [User setCurrUser:dict[@"userInfo"]];
            [self reloadTableView];
        }
        else
        {
            [AppDelegate.shared showErrors:dict[@"errors"]];
            _firstLoad = YES;
        }
        
        FRequestAnkets *request = [FRequestAnkets new];
        request.parent = self;
        [request start];
        
        [self fill];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    else
        if (section == 1)
        {
            return 5;
        }
    return _fetchedController.fetchedObjects.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return self.topView.height;
    }
    return 35;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return self.topView;
    }
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.width, 35)];
    label.backgroundColor = [UIColor colorWithWhite:71/255.0 alpha:0.8];
    label.textColor = [UIColor whiteColor];
    if (section == 1)
    {
        label.text = @"   Основная информация";
    }
    else
        if (section == 2)
        {
            label.text = @"   Анкеты";
        }
    return label;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [UITableViewCell new];
    cell.layoutMargins = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = NO;
    cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    UIColor *gray = [UIColor gray];
    
    UILabel *titleLabel     = [UILabel new];
    titleLabel.font         = [UIFont normal];
    titleLabel.textColor    = gray;
    
    if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            titleLabel.text     = @"Ваше имя";
            self.tfName.frame   = CGRectMake(0, 0, 130, cell.height);
            _tfName.font        = [UIFont normal];
            _tfName.right       = self.view.width - 15;
            _tfName.textColor   = gray;
            _tfName.textAlignment = NSTextAlignmentRight;
            _tfName.placeholder = @"Введите имя";
            _tfName.delegate    = self;
            
            [cell addSubview:_tfName];
        }
        else
            if (indexPath.row == 1)
            {
                titleLabel.text = @"Дата рождения";
                
                self.dateLabel.frame        = CGRectMake(0, 0, 130, cell.height);
                _dateLabel.textAlignment    = NSTextAlignmentRight;
                _dateLabel.font             = [UIFont normal];
                _dateLabel.textColor        = gray;
                _dateLabel.right            = self.view.width - 40;
                [cell addSubview:_dateLabel];
                
                
                UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
                [arrow sizeToFit];
                cell.accessoryView = arrow;
            }
            else
                if (indexPath.row == 2)
                {
                    titleLabel.text = @"Пол";
                    
                    self.maleLabel.frame = CGRectMake(0, 0, 130, cell.height);
                    _maleLabel.textAlignment = NSTextAlignmentRight;
                    _maleLabel.font = [UIFont normal];
                    _maleLabel.textColor = gray;
                    _maleLabel.right = self.view.width - 40;
                    [cell addSubview:_maleLabel];
                    
                    UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
                    [arrow sizeToFit];
                    cell.accessoryView = arrow;
                }
                else
                    if (indexPath.row == 3)
                    {
                        titleLabel.text = @"Сменить пользователя";
                        
//                        self.cityLabel.frame = CGRectMake(0, 0, 130, cell.height);
//                        _cityLabel.textAlignment = NSTextAlignmentRight;
//                        _cityLabel.font = [UIFont normal];
//                        _cityLabel.textColor = gray;
//                        _cityLabel.right = self.view.width - 40;
//                        [cell addSubview:_cityLabel];
//                        
                        UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
                        [arrow sizeToFit];
                        cell.accessoryView = arrow;
                    }
                else
                    if (indexPath.row == 4)
                    {
                        titleLabel.text = @"Подписка";
                        
                        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 180, cell.height)];
                        textLabel.textAlignment = NSTextAlignmentRight;
                        textLabel.font = [UIFont normal];
                        textLabel.textColor = gray;
                        textLabel.right = self.view.width - 40;
                        [cell.contentView addSubview:textLabel];
                        
                        if ([User currentUser].paidAccess)
                        {
//                            NSTimeInterval sInterval = [[User currentUser].paidUntil timeIntervalSinceNow];
                            NSCalendar *calendar = [NSCalendar currentCalendar];
                            NSDateComponents *dateComponents = [calendar components:NSDayCalendarUnit fromDate:[NSDate date] toDate:[User currentUser].paidUntil options:0];
                            NSString *daysText = stringFromCountString(@"день|дня|дней", dateComponents.day);
                            textLabel.text = [NSString stringWithFormat:@"Осталось %li %@", (long)dateComponents.day, daysText];
                        }
                        else
                        {
                            textLabel.text = @"Не активирована";
                        }
                        
                        UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
                        [arrow sizeToFit];
                        cell.accessoryView = arrow;
                    }
    }
    else
        if (indexPath.section == 2)
        {
            indexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
            Anketa *anketa = [_fetchedController objectAtIndexPath:indexPath];
            titleLabel.text = anketa.name;
            UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
            [arrow sizeToFit];
            cell.accessoryView = arrow;
        }
    
    [titleLabel sizeToFit];
    titleLabel.height = cell.height;
    titleLabel.left = 15;
    titleLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
    [cell addSubview:titleLabel];
    
    return cell;
}


//Did SELECT row.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            [_tfName becomeFirstResponder];
        }
        else
            if (indexPath.row == 1)
            {
                [_tfName resignFirstResponder];
                
                [self.picker show];
            }
            else
                if (indexPath.row == 2)
                {
                    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Выберите пол" delegate:self cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles:@"Мужской",@"Женский", nil];
                    [sheet showInView:self.view];
                }
                else
                    if (indexPath.row == 3)
                    {
                        [self onExit];
                    }
                else
                    if (indexPath.row == 4)
                    {
                        if ([User currentUser].paidAccess)
                        {
                            FVCSubscriptionInfo *vc = [FVCSubscriptionInfo new];
                            [self.navigationController pushViewController:vc animated:YES];
                        }
                        else {
                            FVCChooseSubscription *vc = [FVCChooseSubscription new];
                            [self.navigationController pushViewController:vc animated:YES];
                        }
                    }
    }
    else
        if (indexPath.section == 2)
        {
            indexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
            FVCAnketa *vc = [FVCAnketa new];
            vc.anketa = [self.fetchedController objectAtIndexPath:indexPath];
            [self.navigationController pushViewController:vc animated:true];
        }
    else
    {
        [self onExit];
    }
}


- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        _maleLabel.text = @"Мужской";
    }
    else
        if (buttonIndex == 1)
        {
            _maleLabel.text = @"Женский";
        }
}

- (void)datePicker:(RVDatePicker *)picker dateSelected:(NSDate *)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd.MM.yyyy"];
    _dateLabel.text = [formatter stringFromDate:date];
}

- (void)onPhotoButton
{
    [AlertHelpers showImagePickerFromVC:self usingTabbar:NO onSuccess:^(UIImage *image) {
        [_addPhotoButton setTitle:@""];
        [_addPhotoButton setImage:cropNScaleImageToSize(image, USER_IMAGE_SIZE)];
        _imageChanged = YES;
        [[FVCContainer shared] switchToChild:1 animated:NO];
    } onFailure:^()
     {
         [[FVCContainer shared] switchToChild:1 animated:NO];
     }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return true;
}




#pragma mark - private

- (UIView *)topView
{
    if (_topView == nil)
    {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 157)];
        
        _addPhotoButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 130, 130)];
        _addPhotoButton.layer.cornerRadius = 65;
        _addPhotoButton.layer.masksToBounds = YES;
        _addPhotoButton.layer.borderWidth = 2;
        _addPhotoButton.layer.borderColor = [UIColor blue].CGColor;
        [_addPhotoButton setTitle:@"Добавить\nфото"];
        _addPhotoButton.titleLabel.font = [UIFont fontWithName:@"HelvaeticaNeue" size:20];
        _addPhotoButton.titleLabel.numberOfLines = 2;
        _addPhotoButton.centerX = _topView.width / 2;
        _addPhotoButton.centerY = _topView.height / 2;
        _addPhotoButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        _addPhotoButton.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
        [_addPhotoButton setTitleColor:[UIColor blue]];
        _addPhotoButton.alpha = 0;
        [_addPhotoButton addTarget:self action:@selector(onPhotoButton)];
        [_topView addSubview:_addPhotoButton];
    }
    return _topView;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    _tableView.height = self.view.height - keyboardSize.height;
}

- (void)keyboardWillHide:(NSNotification *)n
{
    _tableView.height = self.view.height;
}

- (void)fill
{
    _tfName.text = [User currentUser].name;
    if ([[User currentUser].sex isEqualToString:@"male"])
    {
        _maleLabel.text = @"Мужской";
    }
    else
    {
        _maleLabel.text = @"Женский";
    }
    if ([User currentUser].birthday.timeIntervalSince1970 > 0)
    {
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"dd.MM.yyyy"];
        _dateLabel.text = [formatter stringFromDate:[User currentUser].birthday];
    }
    if ([User currentUser].photo != nil)
    {
        [[User currentUser].photo loadDataIfNeddedWithCompletion:^(NSData *data) {
            [_addPhotoButton setTitle:@""];
            [_addPhotoButton setImage:[UIImage imageWithData:data]];
            _addPhotoButton.alpha = 0;
            [UIView animateWithDuration:0.3 animations:^{
                _addPhotoButton.alpha = 1;
            }];

        }];
    }
}

- (UITextField *)tfName
{
    if (_tfName == nil)
    {
        _tfName = [UITextField new];
    }
    return _tfName;
}

- (UILabel *)maleLabel
{
    if (_maleLabel == nil)
    {
        _maleLabel = [UILabel new];
    }
    return _maleLabel;
}

- (UILabel *)dateLabel
{
    if (_dateLabel == nil)
    {
        _dateLabel = [UILabel new];
    }
    return _dateLabel;
}

- (UILabel *)cityLabel
{
    if (_cityLabel == nil)
    {
        _cityLabel = [UILabel new];
    }
    return _cityLabel;
}




#pragma mark - fetchedController delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [_tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                      withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [_tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                      withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            
            break;
            
        case NSFetchedResultsChangeMove:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
        {
            newIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row inSection:2];
            [_tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete:
            newIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row inSection:2];
            [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
            
        case NSFetchedResultsChangeUpdate:
        {
            [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
            break;
            
        case NSFetchedResultsChangeMove:
        {
            newIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row inSection:2];
            indexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:2];
            [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [_tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}

- (NSFetchedResultsController *)fetchedController
{
    if (_fetchedController == nil)
    {
        NSManagedObjectContext *context = [RBase shared].moc;
        
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Anketa"];
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        NSArray *sortDescriptors = @[sortDescriptor1];
        [request setSortDescriptors:sortDescriptors];
        
        _fetchedController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        _fetchedController.delegate = self;
    }
    
    return _fetchedController;
}

- (RVDatePicker *)picker
{
    if (_picker == nil)
    {
        _picker = [RVDatePicker pickerInView:self.view];
        _picker.delegate = self;
        //_picker.picker.date = [User currentUser].birthday;
    }
    
    return _picker;
}

@end
