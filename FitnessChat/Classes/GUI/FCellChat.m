//
//  FCellChat.m
//  FitnessChat
//
//  Created by Алексей on 26.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FCellChat.h"
#import "File.h"
#import "FPlayerManager.h"
#import <AVFoundation/AVFoundation.h>
#import "FVCContainer.h"
#import "FVCChooseSubscription.h"
#import "Common.h"
#import "UCZProgressView.h"
#import "AppDelegate.h"

@interface FCellChat () <FPlayerManagerProtocol, UIActionSheetDelegate>
{
    UCZProgressView *progressView;
    @public
    UITextView *_textView;
    UIImageView *_cloud;
    UIImageView *_imageView;
    UILabel *_timeLabel;
    UILabel *_readedLabel;
    UIImageView *_userImgView;
    UIView *_verticalLine;
    
    UIButton *_videoButton;
    UIButton *_audioButton;
    UIView *_audioProgressView;
    UILabel *_playerTimeLabel;
    float _duration;
    AVAudioPlayer * _player;
    
    UIActivityIndicatorView *_activity;
    UIButton *_imageButton;
}

@property (strong, nonatomic) UIView *audioView;
@end

@implementation FCellChat

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];

    if (self)
    {
        viewSending = [UIView new];
        viewSending.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
        
        _textView                   = [UITextView new];
        _textView.backgroundColor   = [UIColor clearColor];
        _textView.scrollEnabled     = NO;
        _textView.selectable        = YES;
        _textView.editable          = NO;
        _textView.dataDetectorTypes = UIDataDetectorTypeLink;
        _textView.font              = [UIFont light:16];
        _textView.userInteractionEnabled = NO;
        _imageView                  = [UIImageView new];
        _imageView.layer.cornerRadius = 10;
        _imageView.layer.masksToBounds = YES;
        _imageButton                = [UIButton new];
        [_imageButton addTarget:self action:@selector(onImage)];
        
        _cloud = [UIImageView new];
        
        _timeLabel = [UILabel new];
        _timeLabel.font = [UIFont regular:11];
        
        _readedLabel = [UILabel new];
        _readedLabel.font = [UIFont regular:11];
        
        _userImgView = [UIImageView new];
        _userImgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
        _userImgView.layer.cornerRadius = 22;
        _userImgView.layer.masksToBounds = YES;
        _userImgView.hidden = YES;
        
        _verticalLine = [UIView new];
        _verticalLine.backgroundColor = [UIColor blue];
        
        _videoButton = [UIButton new];
        _videoButton.layer.cornerRadius = 10;
        _videoButton.layer.masksToBounds = YES;
        [_videoButton setImage:[UIImage imageNamed:@"video_play"]];
        [_videoButton setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.4]];
        [_videoButton addTarget:self action:@selector(onPlay)];
        
        _activity = [UIActivityIndicatorView new];
        _activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        _activity.hidden = YES;
        
        [self addSubview:_cloud];
        [self addSubview:_timeLabel];
        [self addSubview:_readedLabel];
        [self addSubview:_textView];
        [self addSubview:_imageView];
        [self addSubview:_imageButton];
        [self addSubview:_userImgView];
        [self addSubview:_verticalLine];
        [self addSubview:_videoButton];
        [self addSubview:self.audioView];
        [self addSubview:_activity];
        
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        UILongPressGestureRecognizer *gesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLong:)];
        gesture.minimumPressDuration = 1;
        [self addGestureRecognizer:gesture];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        [self addGestureRecognizer:tapGesture];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateStatus:) name:NDM_NOTIFICATION_READED_CHANGED object:nil];
    }
    
    return self;
}

-(void)updateStatus: (NSNotification *)notif
{
    NSString *messageId = (NSString *)notif.object;
    if ([messageId isEqualToString:_message.messageId])
        [self updateStatusAtReaded: YES];
}

-(void)updateStatusAtReaded: (BOOL)readed
{
    if (_message.sending)
    {
        [self addSubview:viewSending];
        _readedLabel.text = NULL;
    }
    else
    {
        [viewSending removeFromSuperview];
        _readedLabel.text = readed ? @"Прочитано" : @"Доставлено";
    }
}

- (void)onTap:(UITapGestureRecognizer*)delegate
{
    if (_message.type == MESSAGE_GOTO)
    {
        UINavigationController *nc  = [FVCContainer shared].childViewControllers[1];
        FVCChooseSubscription *vc   = [FVCChooseSubscription new];
        [nc pushViewController:vc animated:NO];
        [[FVCContainer shared] switchToChild:1];
    }
}


//Long press gesture deletes cell data.
-(void)onLong:(UILongPressGestureRecognizer *)gesture
{
    //Disables the built-in long press recognizer if touching '_textView'. Skips right past selecting the text inside the text view and just selects the cell to display an action sheet. /*Stopped working for some reason...*/
    for (UIGestureRecognizer *recognizer in _textView.gestureRecognizers)
    {
        if ([recognizer isKindOfClass:[UILongPressGestureRecognizer class]])
        {
            recognizer.enabled = NO;
        }
    }
    
    
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        if (_message.type == MESSAGE_Text)
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Действия" delegate:self cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles:@"Удалить",@"Копировать", nil];
            [actionSheet showFromTabBar:[FTabbar shared].tabBar];
        }
        else if (_message.type == MESSAGE_Video || _message.type == MESSAGE_Image)
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Действия" delegate:self cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles:@"Удалить", @"Сохранить", nil];
            [actionSheet showFromTabBar:[FTabbar shared].tabBar];

        }
        else
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Действия" delegate:self cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles:@"Удалить", nil];
            [actionSheet showFromTabBar:[FTabbar shared].tabBar];

        }
     }
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) // "Удалить"
    {
        [_delegate cellOnDelete:_message]; // Cell deletion by using UIActionSheet.
    }
    else if (buttonIndex == 1)
    {
        switch (_message.type) {
            case MESSAGE_Text:
            {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = _message.text;
                break;
            }
            case MESSAGE_Image:
            {
                UIImage *image = [UIImage imageWithData:_message.file.data];
                if (image)
                    UIImageWriteToSavedPhotosAlbum(image, NULL, NULL, NULL);
                break;
            }
            case MESSAGE_Video:
            {
                progressView = [[UCZProgressView alloc] initWithFrame:[UIScreen mainScreen].bounds];
                [((AppDelegate *)[UIApplication sharedApplication].delegate).window addSubview:progressView];
                [_message.file loadDataIfNeddedWithCompletion:^(NSData *data) {
                    NSString *path = _message.file.localPath;
                    if ( UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(path))
                    {
                        UISaveVideoAtPathToSavedPhotosAlbum(path, self, @selector(videoSaved:didFinishSavingWithError:contextInfo:), nil);
                    }
                    else
                        [progressView removeFromSuperview];
                }];

                break;
            }
            default:
                break;
        }
        
    }
}

- (void)videoSaved:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    [progressView removeFromSuperview];
}

- (void)onImage
{
    [_delegate cellChatShowImage:_message];
}

- (void)onAudioPlay
{
    _audioButton.selected = YES;
    self.loading = YES;
    [_message.file loadDataIfNeddedWithCompletion:^(NSData *data) {
        [[FPlayerManager shared] playFile:_message.file forTarget:self];
        self.loading = NO;
    }];

}

- (void)onPlay
{
    [_delegate cell:self chatPlay:_message];
}

- (float)getDuration:(NSURL *)url
{
    AVURLAsset* audioAsset = [AVURLAsset URLAssetWithURL:url options:nil];
    CMTime audioDuration = audioAsset.duration;
    float audioDurationSeconds = CMTimeGetSeconds(audioDuration);
    
    return audioDurationSeconds;
}

-(UIImage *) giveThumbofVideoUrl:(NSURL *) url{
//    NSString *urlString = [url absoluteString];
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    //imageGenerator.maximumSize = CGSizeMake(320,480);
    CMTime time = CMTimeMake(1, 1);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *image = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return image;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [_timeLabel sizeToFit];
    _timeLabel.bottom = self.height - 5;
    _readedLabel.hidden = !_message.isMy;
    [_readedLabel sizeToFit];
    _readedLabel.bottom = self.height - 5;
    
    
    _userImgView.frame = CGRectMake(0, 0, 44, 44);
    _userImgView.bottom = self.height - 5;
    
    _textView.width = self.width * 2 / 3;
    [_textView sizeToFit];
    _textView.top = 10;
    
    _verticalLine.hidden = YES;
    
    if ([_message isMy]) // RIGHT text bubble.
    {
        _userImgView.right = self.width - 5;
        UIView *view;
        _timeLabel.left = 10;
        
        if (_message.type == MESSAGE_Text)
        {
            _textView.right = _userImgView.left - 5;
            _textView.userInteractionEnabled = YES; // Allows interaction with any URL's inside the text.
            view = _textView;
        }
        else
            if (_message.type == MESSAGE_Video)
            {
                _videoButton.frame = CGRectMake(_userImgView.left - 125, 10, 120, 90);
                view = _videoButton;
            }
            else
                if (_message.type == MESSAGE_Image)
                {
                    CGFloat width = 140;
                    CGFloat heigth = 140;
                    if (!_loading)
                    {
                        UIImage *image = [UIImage imageWithData:_message.file.data];
                        CGSize size = image.size;
                        width = size.width / size.height * heigth;
                        if (size.height == 0)
                        {
                            width = 140;
                        }
                    }
                    
                    _imageView.frame = CGRectMake(_userImgView.left - width - 5, 10, width, heigth);
                    _imageButton.frame = _imageView.frame;
                    view = _imageView;
                }
        else
            if (_message.type == MESSAGE_Audio || _message.type == MESSAGE_Audio_Voice )
            {
                _audioView.frame = CGRectMake(_userImgView.left - 220, 10, 215, 60);
                view = _audioView;
            }
        
        _cloud.frame = CGRectMake(view.left - 5, view.top - 5, view.width + 15 , view.height + 10);
        _readedLabel.right = _cloud.right;
        
    } // LEFT text bubble.
    else
    {
//        _userImgView.left = 5;
        
        _userImgView.width = 10;
        _timeLabel.right = self.width - 10;
//        _readedLabel.right = 10;
        
        UIView *view;
        if (_message.type == MESSAGE_Text)
        {
            _textView.left = _userImgView.right + 5;
            _textView.userInteractionEnabled = YES; // Allows interaction with any URL's inside the text.
            view = _textView;
            //NSLog(@"Yop. Text view data:\n%@", _textView);
        }
        else
            if (_message.type == MESSAGE_Video)
            {
                _videoButton.frame = CGRectMake(_userImgView.right + 5, 10, 120, 90);
                view = _videoButton;
            }
        else
            if (_message.type == MESSAGE_Image)
            {
                CGFloat width = 140;
                CGFloat heigth = 140;
                if (!_loading)
                {
                    UIImage *image = [UIImage imageWithData:_message.file.data];
                    CGSize size = image.size;
                    width = size.width / size.height * heigth;
                    if (size.height == 0)
                    {
                        width = 140;
                    }
                }
                _imageView.frame = CGRectMake(_userImgView.right + 5, 10, width, heigth);
                _imageButton.frame = _imageView.frame;
                view = _imageView;
            }
        else
            if (_message.type == MESSAGE_Audio || _message.type == MESSAGE_Audio_Voice)
            {
                _audioView.frame = CGRectMake(_userImgView.right + 5, 10, 215, 60);
                view = _audioView;
            }
        else
            if (_message.type == MESSAGE_GOTO)
            {
                _textView.left = _userImgView.right + 5;
                view = _textView;
                
                _verticalLine.hidden = NO;
                NSDictionary *attributes = @{ NSFontAttributeName : [UIFont light:16] };

                CGRect subTextRect = [@"\n\n" boundingRectWithSize:CGSizeMake(_textView.width, _textView.height) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
                _verticalLine.width = 2;
                _verticalLine.height = subTextRect.size.height / 10 * 11;
                _verticalLine.left = _textView.left + 5;
                _verticalLine.bottom = _textView.height + 10;
            }
        _cloud.frame = CGRectMake(view.left - 10, view.top - 5, view.width + 15 , view.height + 10);
    }
    _activity.frame = _cloud.frame;
    
    if ([_message isMy])
    {
        UIView *view;
        if (self.message.type == MESSAGE_Text)
        {
            view = _textView;
        }
        else
            if (self.message.type == MESSAGE_Video)
            {
                view = _videoButton;
            }
            else
                if (self.message.type == MESSAGE_Image)
                {
                    view = _imageView;
                    _imageButton.right = _userImgView.right - 25;
                }
                else
                    if (self.message.type == MESSAGE_Audio)
                    {
                        view = self.audioView;
                    }
        view.right = _userImgView.right - 25;
        
        _cloud.right = _userImgView.right - 15;
        
        _activity.frame = _cloud.frame;
    }
}

- (void)setMessage:(Message *)message
{
    _message = message;
    if (message.type == MESSAGE_Text)
    {
        _textView.text = message.text;
        [self hiddenAllWithout:_textView];
    }
    else
        
        if (message.type == MESSAGE_Video)
        {
            [self hiddenAllWithout:_videoButton];
#ifdef DEBUG
            const NSInteger maxLoadSize = 5*1024*1024;
#else
            const NSInteger maxLoadSize = 20*1024*1024;
#endif
            if (_message.file.filesize > 0 && _message.file.filesize < maxLoadSize)
            {
                self.loading = YES;
                [message.file loadDataIfNeddedWithCompletion:^(NSData *data) {
                    self.loading = NO;
                }];
            }
        }
    else
        if (message.type == MESSAGE_Image)
        {
            self.loading = YES;
            [_message.file loadDataIfNeddedWithCompletion:^(NSData *data) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    UIImage *image = [UIImage imageWithData:data];
                    UIImage *preview = cropScaleImageToSize(image, CGSizeMake(image.size.width / 4, image.size.height / 4));
                        dispatch_async(dispatch_get_main_queue(), ^{
                            _imageView.image = preview;
                            _imageView.alpha = 0;
                            [UIView animateWithDuration:0.2 animations:^{
                                _imageView.alpha = 1;
                            }];
                            self.loading = NO;
                            [self setNeedsLayout];
                        });
                });
            }];
            [self hiddenAllWithout:_imageView];
        }
    else
        if (message.type == MESSAGE_Audio || _message.type == MESSAGE_Audio_Voice)
        {
            if (_message.file.info.length == 0)
            {
                _playerTimeLabel.text = @"-:-";
            }
            else
            {
                _playerTimeLabel.text = stringTime([_message.file.info intValue]);
            }
            [[FPlayerManager shared] prooveFile:_message.file forTarget:self];
            [self hiddenAllWithout:_audioView];
        }
    else
        if (message.type == MESSAGE_GOTO)
        {
//            _textView.text = message.text;
            [self hiddenAllWithout:_textView];
            _textView.attributedText = [FCellChat attrStringForGotoMessage:message];
            _verticalLine.hidden = NO;
        }
    

    static NSDateFormatter *formatter;
    if (formatter == nil)
    {
        formatter = [NSDateFormatter new];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        [formatter setDateFormat:@"HH:mm"];
    }
    _timeLabel.text = [formatter stringFromDate:_message.date];
    [self updateStatusAtReaded:_message.readed];
    [_readedLabel sizeToFit];
    if (message.isMy)
    {
        static UIImage *myImage;
        if (myImage == nil)
        {
            myImage = [UIImage imageNamed:@"my_message_bg"];
            myImage = [myImage resizableImageWithCapInsets:UIEdgeInsetsMake(20, 16, 20, 30) resizingMode:UIImageResizingModeStretch];
        }
        _textView.textColor = [UIColor whiteColor];
        _cloud.image = myImage;
         _userImgView.hidden = YES;
    }
    else
    {
//        _userImgView.width = 0;
        _userImgView.hidden = YES;
        static UIImage *otherImage;
        if (otherImage == nil)
        {
            otherImage = [UIImage imageNamed:@"other_message_bg"];
            otherImage = [otherImage resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 30, 20) resizingMode:UIImageResizingModeStretch];
        }
        if (message.type != MESSAGE_GOTO)
            _textView.textColor = [UIColor colorWithWhite:103/255.0 alpha:1];
        _cloud.image = otherImage;
    }
   
    _userImgView.image = nil;
    [[RCore shared] loadUser:message.userId withCompletion:^(User *user) {
        [self fillUser:user];
    }];
    if (_message.sending && _message.type != MESSAGE_Image)
    {
        self.loading = YES;
        _imageView.alpha = 0;
    }
    else
    {
//        self.loading = NO;
        _imageView.alpha = 1;
    }
    viewSending.frame = CGRectMake(0, 0, self.frame.size.width, [FCellChat heightForMessage:_message]);
}

- (void)hiddenAllWithout:(UIView *)view
{
    _textView.hidden = YES;
    _videoButton.hidden = YES;
    _imageView.hidden = YES;
    _audioView.hidden = YES;
    _imageButton.hidden = YES;
    view.hidden = NO;
    if (view == _imageView)
    {
        _imageButton.hidden = NO;
    }

}
- (void)fillUser:(User *)user
{
    [user.smallPhoto loadDataIfNeddedWithCompletion:^(NSData *data) {
        _userImgView.image = [UIImage imageWithData:data];
    }];
}


- (void)updateTime:(float)time progress:(float)progress isPlaying:(BOOL)isPlaying
{
    _audioButton.selected = isPlaying;
    _playerTimeLabel.text = stringTime(time);
    _audioProgressView.left = _audioButton.right + (_audioView.width - _audioButton.width - 20) * progress;
}


- (void)setLoading:(BOOL)loading
{
    if (_message.type == MESSAGE_Video)
        _loading = loading;
    _loading = loading;
    _activity.hidden = !loading;
    [self bringSubviewToFront:_activity];
    if (loading)
    {
        [_activity startAnimating];
    } else {
        [_activity stopAnimating];
    }
}


- (UIView *)audioView
{
    if (_audioView == nil)
    {
        _audioView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 215, 60)];
        _audioView.layer.cornerRadius = 10;
        _audioView.layer.masksToBounds = YES;
        _audioView.backgroundColor = [UIColor blue];
        
        _audioButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        [_audioButton setImage:[UIImage imageNamed:@"play"]];
        [_audioButton setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateSelected];
        [_audioButton addTarget:self action:@selector(onAudioPlay)];
        [_audioView addSubview:_audioButton];
        
        UIView *whiteView =  [[UIView alloc] initWithFrame:CGRectMake(_audioButton.right, 0, _audioView.width - _audioButton.width - 20, 3)];
        whiteView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.6];
        whiteView.centerY = _audioButton.centerY;
        [_audioView addSubview:whiteView];
        
        _audioProgressView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 3, 15)];
        _audioProgressView.backgroundColor = [UIColor whiteColor];
        _audioProgressView.left = _audioButton.right;
        _audioProgressView.centerY = _audioButton.centerY;
        
        [_audioView addSubview:_audioProgressView];
        _playerTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(whiteView.left, whiteView.bottom, 100, 30)];
        _playerTimeLabel.textColor = [UIColor whiteColor];
        _playerTimeLabel.text = @"-:-";
        _playerTimeLabel.textAlignment = NSTextAlignmentLeft;
        [_audioView addSubview:_playerTimeLabel];
    }
    return _audioView;
}

#pragma mark -

+ (NSAttributedString*) attrStringForGotoMessage:(Message *)message
{
    NSString *mainText = message.text;
//    NSRange mainTextRange = NSMakeRange(0, mainText.length);
    
    NSString *subText = [NSString stringWithFormat:@"\n\nОплата подписки на месяц\n"];
    NSRange subTextRange = NSMakeRange(mainText.length, subText.length);
    
    NSString *moneyText = [NSString stringWithFormat:@"%li ₽", (long)message.paymentAmount];
    NSRange moneyTextRange = NSMakeRange(mainText.length + subText.length, moneyText.length);
    
    NSString *wholeString = [@[mainText, subText, moneyText] componentsJoinedByString:@""];
    NSRange wholeStringRange = NSMakeRange(0, wholeString.length);
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:wholeString];
    
    [attrString addAttribute:NSFontAttributeName value:[UIFont light:16] range:wholeStringRange];
//    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor red] range:wholeStringRange];
//    [attrString addAttribute:NSBackgroundColorAttributeName value:[UIColor orangeColor] range:wholeStringRange];
//    [attrString addAttribute:NSStrokeWidthAttributeName value:@(-1.0) range:wholeStringRange];
    //            [attrString addAttribute:NSStrokeColorAttributeName value:[UIColor blue] range:wholeStringRange];
    
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor blue] range:subTextRange];
    [attrString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:subTextRange];
    NSMutableParagraphStyle *subTextParagrphStyle = [[NSMutableParagraphStyle alloc] init];
    subTextParagrphStyle.firstLineHeadIndent = 7;
    [attrString addAttribute:NSParagraphStyleAttributeName value:subTextParagrphStyle range:subTextRange];
    
    [attrString addAttribute:NSFontAttributeName value:[UIFont light:16] range:moneyTextRange];
    [attrString addAttribute:NSParagraphStyleAttributeName value:subTextParagrphStyle range:moneyTextRange];
    
    return attrString;
}

+ (CGFloat)heightForMessage:(Message *)message
{
    static  UITextView *textView;
    if (textView == nil)
    {
        textView = [UITextView new];
    }
    
    if (message.type == MESSAGE_Text)
    {
        textView.text = message.text;
        textView.font = [UIFont light:16];
        textView.width = [UIScreen mainScreen].bounds.size.width * 2 / 3;
        [textView sizeToFit];
        CGFloat height = textView.height + 20;
        if (message.isMy)
            height+=15;
        return height;
    }
    else
        if (message.type == MESSAGE_Video)
        {
            CGFloat height = 110;
            if (message.isMy)
                height+=15;
            return height;
        }
    else
        if (message.type == MESSAGE_Image)
        {
            CGFloat height = 170;
            if (message.isMy)
                height+=15;
            return height;
        }
    else
        if (message.type == MESSAGE_Audio || message.type == MESSAGE_Audio_Voice)
        {
            CGFloat height = 80;
            if (message.isMy)
                height+=15;
            return height;
        }
    else
        if (message.type == MESSAGE_GOTO)
        {
            textView.font = [UIFont light:16];
            textView.width = [UIScreen mainScreen].bounds.size.width * 2 / 3;
            textView.attributedText = [FCellChat attrStringForGotoMessage:message];
            [textView sizeToFit];
            return textView.height + 20;
        }
    return 50;
}

@end

@interface FCellChatGroup ()
{
    UILabel *_authorLabel;
}
@end


@implementation FCellChatGroup

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        _authorLabel = [UILabel new];
        _authorLabel.font = [UIFont medium:15];
        _authorLabel.textAlignment = NSTextAlignmentLeft;
        _authorLabel.textColor = [UIColor colorWith256Red:41 green:94 blue:111];
        [_cloud addSubview:_authorLabel];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (![self.message isMy])
    {
        UIView *view;
        if (self.message.type == MESSAGE_Text)
        {
            _textView.top -= 5;
            _cloud.width  += 10;
            _textView.left = _userImgView.left + 15;
            _textView.top += 15;
        }
        else
            if (self.message.type == MESSAGE_Video)
            {
                view = _videoButton;
            }
            else
                if (self.message.type == MESSAGE_Image)
                {
                    view = _imageView;
                    _imageButton.top += 15;
                    _imageButton.left = _userImgView.left;
                }
                else
                    if (self.message.type == MESSAGE_Audio)
                    {
                        view = self.audioView;
                    }
        view.top += 15;
        view.left = _userImgView.left + 10;
        
        _cloud.left = _userImgView.left;
        _cloud.height += 15;

        [_authorLabel sizeToFit];
        _cloud.width = MAX(_authorLabel.width + 40, _cloud.width);
        _authorLabel.left = 20;
        _authorLabel.centerY = 10;
        
        _activity.frame = _cloud.frame;
    }
    else
    {
        return;
        
        UIView *view;
        if (self.message.type == MESSAGE_Text)
        {
            view = _textView;
        }
        else
            if (self.message.type == MESSAGE_Video)
            {
                view = _videoButton;
            }
            else
                if (self.message.type == MESSAGE_Image)
                {
                    view = _imageView;
                    _imageButton.right = _userImgView.right - 25;
                }
                else
                    if (self.message.type == MESSAGE_Audio)
                    {
                        view = self.audioView;
                    }
        view.right = _userImgView.right - 25;
        
        _cloud.right = _userImgView.right - 15;
        
        _activity.frame = _cloud.frame;
    }
}

- (void)fillUser:(User *)user
{
    _authorLabel.text = user.name;
}

- (void)setMessage:(Message *)message
{
    [super setMessage:message];
    if ([message isMy])
    {
        _authorLabel.hidden = YES;
    }
    else
    {
        _authorLabel.hidden = NO;
    }
    _userImgView.hidden = YES;

}

+ (CGFloat)heightForMessage:(Message *)message
{
    if ([message isMy])
    {
        return [super heightForMessage:message];
    }
    return [super heightForMessage:message] + 15;
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end

