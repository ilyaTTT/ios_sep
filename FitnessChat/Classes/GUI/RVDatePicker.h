//
//  RVDataPicker.h
//  RGS
//
//  Created by user on 08.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

@class RVDatePicker;

@protocol RVDatePickerProtocol
- (void)datePicker:(RVDatePicker *)picker dateSelected:(NSDate *)date;

@end

@interface RVDatePicker : UIView
+ (RVDatePicker *)pickerInView:(UIView *)view;
@property (strong, nonatomic) UIDatePicker *picker;
@property (weak, nonatomic) id<RVDatePickerProtocol>delegate;
- (void)show;
- (void)hide;

@end
