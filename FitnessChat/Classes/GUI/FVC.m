//
//  FVC.m
//  FitnessChat
//
//  Created by Arcanite LLC on 10.07.15.
//  Copyright (c) 2015 Arcanite LLC. All rights reserved.
//

#import "FVC.h"
#import "FormTextField.h"


@interface FVC ()

@property (strong, nonatomic) UIImageView *backgroundView;

@property (strong, nonatomic) UITapGestureRecognizer *tapGestureRecognizer;

@end


@implementation FVC

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.navigationController.childViewControllers.count > 1)
    {
        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"back_button"]];
        [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [backBtn sizeToFit];
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
        self.navigationItem.leftBarButtonItem = backButton;
    }
    
    if (!self.hidesBottomBarWhenPushed)
    {
        self.tabBarController.tabBar.translucent = YES;
    }
}


#pragma mark - rotation

-(BOOL)shouldAutorotate
{
    return YES;
}

//-(NSUInteger)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskPortrait;
//}
//
//-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    return UIInterfaceOrientationPortrait;
//}


#pragma mark - tap to remove keyboard

- (void) setupTapToRemoveKeyboard
{
    _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)];
    _tapGestureRecognizer.cancelsTouchesInView = NO;
//    _tapGestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:_tapGestureRecognizer];
}

- (void) cancelTapToRemoveKeyboard
{
    [self.view removeGestureRecognizer:_tapGestureRecognizer];
}

- (void) onTap
{
    [self.view endEditing:YES];
}


#pragma mark -

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIView *)separator
{
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 1)];
    separator.backgroundColor = [UIColor colorWith256Red:220 green:220 blue:220];
    separator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    return separator;
}

- (FormTextField *)codeTF
{
    FormTextField*textField         = [[FormTextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
    textField.maxLength             = 4;
    textField.textfield.textAlignment = NSTextAlignmentCenter;
    textField.keyboardType          = UIKeyboardTypeNumberPad;
    textField.textfield.font        = [UIFont normal];
    textField.autoresizingMask      = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    textField.layer.cornerRadius    = 22;
    textField.layer.masksToBounds   = YES;
    textField.layer.borderColor     = [UIColor colorWith256Red:220 green:220 blue:220].CGColor;
    textField.layer.borderWidth     = 1;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"key"]];
    [imageView sizeToFit];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [leftView addSubview:imageView];
    imageView.center = leftView.center;
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];

    textField.textfield.rightView       = rightView;
    textField.textfield.rightViewMode   = UITextFieldViewModeAlways;
    textField.textfield.leftView        = leftView;
    textField.textfield.leftViewMode    = UITextFieldViewModeAlways;
    
    return textField;
}

- (FormTextField *)phoneTF
{
    FormTextField*textField         = [[FormTextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
    textField.mask                  = formPhoneMask;
    textField.keyboardType          = UIKeyboardTypeNumberPad;
    textField.textfield.font        = [UIFont normal];
    textField.autoresizingMask      = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    textField.layer.cornerRadius    = 22;
    textField.layer.masksToBounds   = YES;
    textField.layer.borderColor     = [UIColor colorWith256Red:220 green:220 blue:220].CGColor;
    textField.layer.borderWidth     = 1;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone"]];
    [imageView sizeToFit];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [leftView addSubview:imageView];
    imageView.center = leftView.center;
    
    textField.textfield.leftView        = leftView;
    textField.textfield.leftViewMode    = UITextFieldViewModeAlways;
    
    return textField;
}


//METHOD NOT USED!
- (FormTextField *)cardNumberTF;
{
    FormTextField*textField = [[FormTextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
    
    return textField;
}

//METHOD NOT USED!
- (FormTextField *)cardDateTF
{
    FormTextField*textField = [[FormTextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
    
    return textField;
}

//METHOD NOT USED!
- (FormTextField *)cardCVVTF
{
    FormTextField*textField = [[FormTextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
    
    return textField;
}

/** Takes in a view & spews out a UITextField with rounded corners by utilizing "QuartzCore" frameworks' layers property. */
- (UITextField *)roundedTextFieldWithLeftView:(UIView *)view
{
    UITextField*textField = [[UITextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
    textField.backgroundColor = [UIColor whiteColor];
    textField.font = [UIFont normal];
    textField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    textField.layer.cornerRadius = 22;
    textField.layer.masksToBounds = YES;
    textField.layer.borderColor = [UIColor colorWith256Red:220 green:220 blue:220].CGColor;
    textField.layer.borderWidth = 1;
    
    textField.leftView = view;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    return textField;
}

- (UITextField *)roundedTextFieldWithLeftImage:(UIImage *)image
{
    UITextField*textField = [[UITextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
    textField.backgroundColor = [UIColor whiteColor];
    textField.font = [UIFont normal];
    textField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    textField.layer.cornerRadius = 22;
    textField.layer.masksToBounds = YES;
    textField.layer.borderColor = [UIColor colorWith256Red:220 green:220 blue:220].CGColor;
    textField.layer.borderWidth = 1;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    [imageView sizeToFit];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [leftView addSubview:imageView];
    imageView.center = leftView.center;
    
    textField.leftView = leftView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    return textField;
}

- (UIButton *)roundedButton
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
    button.backgroundColor = [UIColor blue];
    button.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    button.layer.cornerRadius = 22;
    button.layer.masksToBounds = YES;
    [button setTitleColor:[UIColor whiteColor]];
    
    return button;
}

/** Setter method for setting a background image for self. */
-(void)setBackgroundName:(NSString *)backgroundName
{
    self.backgroundView.image = [UIImage imageNamed:backgroundName];
}

- (UIImageView *)backgroundView
{
    if (_backgroundView == nil)
    {
        _backgroundView = [[UIImageView alloc] initWithFrame:self.view.bounds];
        _backgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        UIView *overlayView = [[UIView alloc] initWithFrame:_backgroundView.bounds];
        overlayView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        //overlayView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
        //overlayView.backgroundColor = [UIColor colorWithWhite:1 alpha:1.0];
        [_backgroundView addSubview:overlayView];
        
        [self.view addSubview:_backgroundView];
        [self.view sendSubviewToBack:_backgroundView];
    }
    return _backgroundView;
}

@end
