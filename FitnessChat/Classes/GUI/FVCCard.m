//
//  FVCCard.m
//  FitnessChat
//
//  Created by owel on 07/10/15.
//  Copyright © 2015 Алексей. All rights reserved.
//

#import "FVCCard.h"
//#import "FormTextField.h"
#import <CloudPaymentsAPI/CPService.h>
#import "Common.h"
#import "FRequestPayment.h"
#import "AFNetworking.h"
#import "WebViewVC.h"
#import "FVCSubscriptionSuccess.h"
#import "FVCContainer.h"


//static const NSInteger TEXTFIELD_START_TAG = 100;


@interface FVCCard () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, /*FormTextFieldProtocol,*/ WebViewVCDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) CPService     *apiService;

@property (strong, nonatomic) UITableView   *tableView;

//@property (strong, nonatomic) UIView        *headerView;
@property (strong, nonatomic) UIView        *headerView;
@property (strong, nonatomic) UIView        *subscriptionView;
@property (strong, nonatomic) UILabel       *subscriptionLabel;
@property (strong, nonatomic) UILabel       *sumLabel;
//@property (strong, nonatomic) UILabel       *subscriptionCaptionLabel;
//@property (strong, nonatomic) UILabel       *subscriptionLabel;
//@property (strong, nonatomic) UILabel       *sumCaptionLabel;
//@property (strong, nonatomic) UILabel       *sumLabel;

@property (strong, nonatomic) UIView        *payView;
//@property (strong, nonatomic) UIButton      *payButton;
//@property (strong, nonatomic) UIView        *footerView;

//@property (strong, nonatomic) FormTextField *cardNumberField;
//@property (strong, nonatomic) UITextField   *nameField;
//@property (strong, nonatomic) FormTextField *cvvField;
////@property (strong, nonatomic) FormTextField *dateField;
//@property (strong, nonatomic) FormTextField *monthField;
//@property (strong, nonatomic) FormTextField *yearField;

//@property (strong, nonatomic) UIDatePicker  *datePicker;




/*NIKITA IGNATENKO STUFF!*/
//5 different text fields for inputting card information.
@property (strong, nonatomic) UITextField *textfieldCardNumber;
@property (strong, nonatomic) UITextField *textfieldName;
@property (strong, nonatomic) UITextField *textfieldCVV;
@property (strong, nonatomic) UITextField *textfieldMonth;
@property (strong, nonatomic) UITextField *textfieldYear;

//These BOOL's will be used to check if everything is in order when the 'Pay Button' at the end will be pressed. If some text field won't be complete, a warning will appear.
@property BOOL completeCardNumber;
@property BOOL completeName;
@property BOOL completeCVV;
@property BOOL completeMonth;
@property BOOL completeYear;

// Will help track number of mess ups.
@property int numberOfIncompleteFields;

@end


@implementation FVCCard

-(void)viewDidLoad
{
    [super viewDidLoad];

    _apiService = [[CPService alloc] init];
    
    self.backgroundName = @"back_bg"; //@"back_register";
    [self setupTapToRemoveKeyboard]; //<-- ??
    
    //Navigation bar.
    self.navigationItem.title = @"Оплата";
    
    //Header.
    _subscriptionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width / 10 * 9, 100)];
    _subscriptionView.centerX = self.view.width / 2;
    _subscriptionView.top = 10;
    _subscriptionView.backgroundColor = [UIColor whiteColor];
    _subscriptionView.layer.cornerRadius = 15.0f;
    
    _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, _subscriptionView.height + 20)];
    _headerView.backgroundColor = [UIColor clearColor];
    
    _subscriptionLabel = [UILabel new];
    _subscriptionLabel.font = [UIFont medium:20];
    _subscriptionLabel.textColor = [UIColor gray];
    _subscriptionLabel.text = [NSString stringWithFormat:@"Подписка на %@", self.subscription.subscriptionDuration];
    [_subscriptionLabel sizeToFit];
    _subscriptionLabel.centerX = _subscriptionView.width / 2;
    _subscriptionLabel.centerY = _subscriptionView.height / 4 * 1;
    
    _sumLabel = [UILabel new];
    _sumLabel.font = [UIFont bold:21];
    _sumLabel.textColor = [UIColor gray];
    _sumLabel.text = [NSString stringWithFormat:@"К оплате %li ₽", (long)self.subscription.subscriptionCost];
    [_sumLabel sizeToFit];
    _sumLabel.centerX = _subscriptionView.width / 2;
    _sumLabel.centerY = _subscriptionView.height / 4 * 3;
    
    
    //Pay view.
    _payView                    = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 100)];
    _payView.bottom             = self.view.height;
    _payView.backgroundColor    = [UIColor clearColor];
    _payView.autoresizingMask   = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
    
    
    //Pay button 'Оплатить'.
//    _payButton          = [self roundedButton];
//    _payButton.centerX  = _payView.width/2;
//    _payButton.centerY  = _payView.height/20 * 9;
//    [_payButton setTitle:@"Оплатить"];
//    _payButton.hidden   = YES;
//    [_payButton addTarget:self action:@selector(payButtonMethod:)]; //forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *paymentButton          = [[UIBarButtonItem alloc] initWithTitle:@"Оплатить" style:UIBarButtonItemStyleBordered target:self action:@selector(payButtonMethod:)];
    self.navigationItem.rightBarButtonItem  = paymentButton;
    
    
//    _payButton                      = [[UIButton alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
//    _payButton.backgroundColor      = [UIColor colorWith256Red:87 green:161 blue:184];
//    _payButton.autoresizingMask     = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
//    _payButton.layer.cornerRadius   = 22;
//    _payButton.layer.masksToBounds  = YES;
//    [_payButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    _payButton.centerX              = _payView.width/2;
//    _payButton.centerY              = _payView.height/20 * 9;
//    [_payButton setTitle:@"Оплатить"];
//    _payButton.hidden               = YES;
//    [_payButton addTarget:self action:@selector(payButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //Table view.
    _tableView                      = [[UITableView alloc] initWithFrame:self.view.bounds];
    _tableView.backgroundColor      = [UIColor clearColor];
    _tableView.autoresizingMask     = self.view.autoresizingMask;
    _tableView.keyboardDismissMode  = UIScrollViewKeyboardDismissModeOnDrag;
    _tableView.dataSource           = self;
    _tableView.delegate             = self;
    _tableView.tableHeaderView      = _headerView;
    //_tableView.tableFooterView      = _payView;
//    _tableView.top = _subscriptionView.bottom + 10;
//    _tableView.width = self.view.width;
//    _tableView.height = self.view.height - _tableView.top - _payView.height;
    
    
    
    [_subscriptionView addSubview:_subscriptionLabel];
    [_subscriptionView addSubview:_sumLabel];
    
    [_headerView addSubview:_subscriptionView];
    
    //[_payView addSubview:_payButton];
    
    [self.view addSubview:_tableView];
    
    //Keyboard notifications.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    //Catches the END of editing events for a textfield.
    //[_textfieldMonth addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}




#pragma mark - Buttons

//"Оплатить" button. Called upon when user is done inputting their card data.
-(void)payButtonMethod:(id)sender
{
    //NSLog(@"Pay Button PRESSED.");
    
//    //NSLog(@"%i", _numberOfIncompleteFields);
//    //IF at least 1 text field is incomplete, then this message will appear.
//    if (_numberOfIncompleteFields >= 1)
//    {
//        NSString *message = [NSString stringWithFormat:@"%i незакончиных полей" , _numberOfIncompleteFields];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Подождити" message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
//        [alert show];
//    }
//    else
//    {
//        //IF all is good, then run the following method.
//        [self everythingIsGood];
//    }
    
    
    [self everythingIsGood];
}


/** Called by 'payButtonPressed' button when everything is in order in regards to filling out the card form. */
-(void)everythingIsGood
{
//    NSString *dateString = [NSString stringWithFormat:@"%@%@", [_dateField.text substringFromIndex:2], [_dateField.text substringToIndex:2]];
    //NSString *dateString = [_yearField.text stringByAppendingString:_monthField.text];
    
    NSString *dateString = [_textfieldYear.text stringByAppendingString:_textfieldMonth.text];
    
    
    //LINK: https://www.cloudpayments.ru/Docs/MobileSDK#ios
    NSString *cryptogramPacket = [_apiService makeCardCryptogramPacket:_textfieldCardNumber.text andExpDate:dateString andCVV:_textfieldCVV.text andStorePublicID:CLOUD_PAYMENT_PUBLIC_ID];
    
    //    FRequestPayment *request = [FRequestPayment new];
    //    request.serviceId = self.subscription.subscriptionId;
    //    request.cryptogram = cryptogramPacket;
    //    request.parent = self;
    //    [request start];
    
    
    NSMutableDictionary *paramsDictionary = [[NSMutableDictionary alloc] init];
    
    [paramsDictionary setObject:cryptogramPacket forKey:@"cryptogram"];
    [paramsDictionary setObject:self.subscription.subscriptionId forKey:@"service_id"];
    [paramsDictionary setObject:[RCore shared].token forKey:@"token"];
    
    //    NSString *apiURLString = @"http://dev-zone.fitconsultant.arcanite.ru/rest/payment/payment";
    NSString *apiURLString = [SERVER_URL stringByAppendingString:@"rest/payment/payment"];
    
    // Setup AFHTTPRequestOperationManager HTTP BasicAuth and serializers
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:apiURLString parameters:paramsDictionary success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        //NSLog(@"payment responce:\n%@", responseObject);
        NSDictionary *responseDict = responseObject;
        
        if (responseDict[@"serviceResponse"][@"Model"] == nil)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Что-то пошло не так" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else {
            
            NSString *url3ds = responseDict[@"serviceResponse"][@"Model"][@"AcsUrl"];
            
            //        NSString *termUrl = @"http://fit-consultant.php.dev/rest/payment/term-url";
            NSString *termUrl = responseDict[@"term_url"];
            
            NSDictionary *params = @{   @"PaReq"    :   responseDict[@"serviceResponse"][@"Model"][@"PaReq"],
                                        @"MD"       :   responseDict[@"serviceResponse"][@"Model"][@"TransactionId"],
                                        @"TermUrl"  :   termUrl   };
            
            //        //NSLog(@"params :\n%@", params);
            
            NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:url3ds parameters:params error:nil];
            WebViewVC *vc = [WebViewVC new];
            vc.delegate         = self;
            vc.requestToLoad    = request;
            
            
            [self presentViewController:vc animated:YES completion:nil];
            
            //        [manager POST:url3ds parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //
            //        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            ;
            //        }];
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        ;
    }];
}




#pragma mark - Keyboard Notifications

-(void)keyboardWillShow:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[CATransaction animationDuration] animations:^{
        _tableView.height           = self.view.height - keyboardSize.height;
        _tableView.contentOffset    = CGPointMake(0, _headerView.height);
    }];
}


-(void)keyboardWillHide:(NSNotification *)n
{
    [UIView animateWithDuration:[CATransaction animationDuration] animations:^{
        _tableView.height = self.view.height;
    }];
}




#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
            return 3;
        case 1:
            return 2;
        default:
            return 0;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell.layoutMargins = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = NO;
    cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
//            _cardNumberField = [[FormTextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, cell.height)];
//            _cardNumberField.tag                        = 0;
//            _cardNumberField.textfield.textColor        = [UIColor gray];
//            _cardNumberField.textfield.tag              = TEXTFIELD_START_TAG;
//            _cardNumberField.mask                       = formCardMask;
//            _cardNumberField.bottomSeparator.hidden     = YES;
//            _cardNumberField.textfield.placeholder      = @"Номер карты";
//            _cardNumberField.keyboardType               = UIKeyboardTypeNumberPad;
//            _cardNumberField.textfield.font             = [UIFont normal];
//            _cardNumberField.textfield.returnKeyType    = UIReturnKeyNext;
//            _cardNumberField.autoresizingMask           = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
//
//            _cardNumberField.text = @"4242424242424242"; // test
//            
//            [cell.contentView addSubview:_cardNumberField];
            
            
            _textfieldCardNumber                = [[UITextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, cell.height)];
            _textfieldCardNumber.delegate       = self;
            _textfieldCardNumber.tag            = 0;
            _textfieldCardNumber.adjustsFontSizeToFitWidth = YES;
            _textfieldCardNumber.textColor      = [UIColor lightGrayColor];
            _textfieldCardNumber.font           = [UIFont fontWithName:@"HelveticaNeue" size:20];
            _textfieldCardNumber.text           = @"Номер карты"; //@"---- ---- ---- ----";
            _textfieldCardNumber.keyboardType   = UIKeyboardTypeNumberPad;
            _textfieldCardNumber.returnKeyType  = UIReturnKeyDefault;
            [cell.contentView addSubview:_textfieldCardNumber];
        }
        else if (indexPath.row == 1) //Name - TextField.
        {
//            _nameField = [[UITextField alloc] initWithFrame:CGRectMake(35, 0, cell.width - 30, cell.height)];
//            _nameField.textColor = [UIColor gray];
//            _nameField.tag = TEXTFIELD_START_TAG + 1;
//            _nameField.font = [UIFont normal];
//            _nameField.placeholder = @"Имя владельца";
//            _nameField.returnKeyType = UIReturnKeyNext;
//            _nameField.delegate = self;
//            
//            _nameField.text = @"Michael P"; // test
//            
//            [cell.contentView addSubview:_nameField];
            
            
            _textfieldName                  = [[UITextField alloc] initWithFrame:CGRectMake(35, 0, cell.width - 30, cell.height)];
            _textfieldName.delegate         = self;
            _textfieldName.tag              = 1;
            _textfieldName.adjustsFontSizeToFitWidth = YES;
            _textfieldName.textColor        = [UIColor lightGrayColor];
            _textfieldName.font             = [UIFont fontWithName:@"HelveticaNeue" size:20];
            _textfieldName.text             = @"Имя Фамилия";
            _textfieldName.keyboardType     = UIKeyboardTypeAlphabet;
            _textfieldName.returnKeyType    = UIReturnKeyDone;
            _textfieldName.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
            [cell.contentView addSubview:_textfieldName];
        }
        else if (indexPath.row == 2)
        {
//            _cvvField = [[FormTextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
//            _cvvField.textfield.textColor = [UIColor gray];
//            _cvvField.textfield.tag = TEXTFIELD_START_TAG + 2;
//            _cvvField.mask = formCardCVVMask;
//            _cvvField.bottomSeparator.hidden = YES;
//            _cvvField.textfield.placeholder = @"CVV";
//            _cvvField.keyboardType = UIKeyboardTypeNumberPad;
//            _cvvField.textfield.font = [UIFont normal];
//            _cvvField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
////            [_cvvField.textfield addTarget:self action:@selector(cvvFieldChanged:) forControlEvents:UIControlEventEditingChanged];
//            
//            _cvvField.text = @"111"; // test
//            
//            [cell.contentView addSubview:_cvvField];
            
            
            _textfieldCVV               = [[UITextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
            _textfieldCVV.delegate      = self;
            _textfieldCVV.tag           = 2;
            _textfieldCVV.adjustsFontSizeToFitWidth = YES;
            _textfieldCVV.textColor     = [UIColor lightGrayColor];
            _textfieldCVV.font          = [UIFont fontWithName:@"HelveticaNeue" size:20];
            _textfieldCVV.text          = @"CVV"; //@"---";
            _textfieldCVV.keyboardType  = UIKeyboardTypeNumberPad;
            [cell.contentView addSubview:_textfieldCVV];
        }
        else if (indexPath.row == 3)
        {
//            _dateField = [[FormTextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
//            _dateField.mask = formCardDateMask;
//            _dateField.bottomSeparator.hidden = YES;
//            _dateField.textfield.placeholder = @"Срок действия карты";
//            _dateField.keyboardType = UIKeyboardTypeNumberPad;
//            _dateField.textfield.font = [UIFont normal];
//            _dateField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
//            _dateField.maxLength = 4;
//            _dateField.textfieldDelegate = self;
//            
//            _dateField.text = @"1216"; // test
//            
//            [cell.contentView addSubview:_dateField];
        }
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
//            _monthField = [[FormTextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
//            _monthField.textfield.textColor = [UIColor gray];
//            _monthField.textfield.tag = TEXTFIELD_START_TAG + 3;
//            _monthField.mask = @"__";
//            _monthField.bottomSeparator.hidden = YES;
//            _monthField.textfield.placeholder = @"Месяц";
//            _monthField.keyboardType = UIKeyboardTypeNumberPad;
//            _monthField.textfield.font = [UIFont normal];
//            _monthField.maxLength = 2;
//            _monthField.textfieldDelegate = self;
//            _monthField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
//            
//            _monthField.text = @"12"; // fixme : test
//            
//            [cell.contentView addSubview:_monthField];
            
            
            _textfieldMonth                 = [[UITextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
            _textfieldMonth.delegate        = self;
            _textfieldMonth.tag             = 3;
            _textfieldMonth.adjustsFontSizeToFitWidth = YES;
            _textfieldMonth.textColor       = [UIColor lightGrayColor];
            _textfieldMonth.font            = [UIFont fontWithName:@"HelveticaNeue" size:20];
            _textfieldMonth.text            = @"Месяц"; //@"--";
            _textfieldMonth.keyboardType    = UIKeyboardTypeNumberPad;
            [cell.contentView addSubview:_textfieldMonth];
        }
        else if (indexPath.row == 1)
        {
//            _yearField = [[FormTextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
//            _yearField.textfield.textColor = [UIColor gray];
//            _yearField.textfield.tag = TEXTFIELD_START_TAG + 4;
//            _yearField.mask = @"__";
//            _yearField.bottomSeparator.hidden = YES;
//            _yearField.textfield.placeholder = @"Год";
//            _yearField.keyboardType = UIKeyboardTypeNumberPad;
//            _yearField.textfield.font = [UIFont normal];
//            _yearField.maxLength = 2;
//            _yearField.textfieldDelegate = self;
//            _yearField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
//            
//            _yearField.text = @"16"; // fixme : test
//            
//            [cell.contentView addSubview:_yearField];
            
            
            _textfieldYear                  = [[UITextField alloc] initWithFrame:CGRectMake(35, 0, self.view.width - 70, 44)];
            _textfieldYear.delegate         = self;
            _textfieldYear.tag              = 4;
            _textfieldYear.adjustsFontSizeToFitWidth = YES;
            _textfieldYear.textColor        = [UIColor lightGrayColor];
            _textfieldYear.font             = [UIFont fontWithName:@"HelveticaNeue" size:20];
            _textfieldYear.text             = @"Год"; //@"--";
            _textfieldYear.keyboardType     = UIKeyboardTypeNumberPad;
            [cell.contentView addSubview:_textfieldYear];
        }
    }
    
    return cell;
}




#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
//    if (section == 0)
//    {
//        return self.topView;
//    }
    
    UILabel *label          = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.width, 35)];
    label.backgroundColor   = [UIColor colorWithWhite:71/255.0 alpha:0.8];
    label.textColor         = [UIColor whiteColor];
    if (section == 0)
    {
        label.text = @"   Сведения о карте";
    }
    else
        if (section == 1)
        {
            label.text = @"   Дата окончания срока действия";
        }
    return label;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 40;
//}




#pragma mark - TEXT FIELD DELEGATE

//Return NO to disallow editing.
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    ////NSLog(@"Text Field Should Begin Editing.");
    
    return YES;
}


//Became first responder.
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    ////NSLog(@"Text Field Did Begin Editing.");
    
    switch (textField.tag)
    {
        case 0: //Card Number.
            if ([textField.text isEqualToString:@"Номер карты"]) //@"---- ---- ---- ----"])
            {
                textField.text = @"";
            }
            if (textField.text.length <= 0)
            {
                textField.text = @"";
            }
            break;
            
        case 1: //Name Lastname.
            if ([textField.text isEqualToString:@"Имя Фамилия"])
            {
                textField.text = @"";
            }
            if (textField.text.length <= 0)
            {
                textField.text = @"";
            }
            break;
            
        case 2: //CVV.
            if ([textField.text isEqualToString:@"CVV"]) //@"---"])
            {
                textField.text = @"";
            }
            if (textField.text.length <= 0)
            {
                textField.text = @"";
            }
            break;
            
        case 3: //Month.
            if ([textField.text isEqualToString:@"Месяц"]) //@"--"])
            {
                textField.text = @"";
            }
            if (textField.text.length <= 0)
            {
                textField.text = @"";
            }
            break;
            
        case 4: //Year.
            if ([textField.text isEqualToString:@"Год"]) //@"--"])
            {
                textField.text = @"";
            }
            if (textField.text.length <= 0)
            {
                textField.text = @"";
            }
            break;
            
            
        default:
            break;
    }
}


//Return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end.
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    ////NSLog(@"Text Field Should End Editing.");
    
    return YES; //Editing will stop and resign first responder status.
}


//May be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called.
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    ////NSLog(@"Text Field Did End Editing.");
    
    switch (textField.tag)
    {
        case 0: //Card Number.
            if (textField.text.length < 16) //19)
            {
                if ([textField.text isEqualToString:@""])
                {
                    textField.text = @"Номер карты"; //@"---- ---- ---- ----";
                }
                else if (textField.text.length >= 1)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Подождити" message:@"Номер карточки не дописан!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                    [alert show];
                }
                
                //_numberOfIncompleteFields += 1; //Incomplete text field.
            }
            else if (textField.text.length == 16)
            {
                _completeCardNumber = YES; //This means the text field is satisfied.
            }
            
            
//            //IF all text fields are satisfied, then display the pay button.
//            if (_completeCardNumber && _completeName && _completeCVV && _completeMonth && _completeYear)
//            {
//                _payButton.hidden = NO;
//                [_payButton bringSubviewToFront:self.view];
//                [_payButton becomeFirstResponder];
//            }
            
            //[_textfieldCardNumber resignFirstResponder];
            break;
            
        case 1: //Name Lastname.
            if (textField.text.length == 0)
            {
                if ([textField.text isEqualToString:@""])
                {
                    textField.text = @"Имя Фамилия";
                }
                else if (textField.text.length >= 1)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Подождите" message:@"Впишите имя и фамилию!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                    [alert show];
                }
                
                //_numberOfIncompleteFields += 1; //Incomplete text field.
            }
            else if (textField.text.length > 0)
            {
                _completeName = YES; //This means the text field is satisfied.
            }
            
            
//            //IF all text fields are satisfied, then display the pay button.
//            if (_completeCardNumber && _completeName && _completeCVV && _completeMonth && _completeYear)
//            {
//                _payButton.hidden = NO;
//                [_payButton becomeFirstResponder];
//            }
            
            //[_textfieldName resignFirstResponder];
            break;
            
        case 2: //CVV.
            if (textField.text.length < 3)
            {
                if ([textField.text isEqualToString:@""])
                {
                    textField.text = @"CVV"; //@"---";
                }
                else if (textField.text.length >= 1)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Подождите" message:@"CVV не дописан! В этой строчке принимаются только три цифры!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                    [alert show];
                }
                
                //_numberOfIncompleteFields += 1; //Incomplete text field.
            }
            else if (textField.text.length == 3)
            {
                _completeCVV = YES; //This means the text field is satisfied.
            }
            
            
//            //IF all text fields are satisfied, then display the pay button.
//            if (_completeCardNumber && _completeName && _completeCVV && _completeMonth && _completeYear)
//            {
//                _payButton.hidden = NO;
//                [_payButton becomeFirstResponder];
//            }
            
            //[_textfieldCVV resignFirstResponder];
            break;
            
        case 3: //Month.
            if (textField.text.length < 2)
            {
                if ([textField.text isEqualToString:@""])
                {
                    textField.text = @"Месяц"; //@"--";
                }
                else if (textField.text.length >= 1)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Подождите" message:@"Месяц не дописан! В этой строчке принимаются только две цифры!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                    [alert show];
                }
                
                //_numberOfIncompleteFields += 1; //Incomplete text field.
            }
            else if (textField.text.length == 2)
            {
                _completeMonth = YES; //This means the text field is satisfied.
            }
            
            
//            //IF all text fields are satisfied, then display the pay button.
//            if (_completeCardNumber && _completeName && _completeCVV && _completeMonth && _completeYear)
//            {
//                _payButton.hidden = NO;
//                [_payButton becomeFirstResponder];
//            }
            
            //[_textfieldMonth resignFirstResponder];
            break;
            
        case 4: //Year.
            if (textField.text.length < 2)
            {
                if ([textField.text isEqualToString:@""])
                {
                    textField.text = @"Год"; //@"--";
                }
                else if (textField.text.length >= 1)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Подождите" message:@"Год не дописан! В этой строчке принимаются только две цифры!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                    [alert show];
                }
                
                //_numberOfIncompleteFields += 1; //Incomplete text field.
            }
            else if (textField.text.length == 2)
            {
                _completeYear = YES; //This means the text field is satisfied.
            }
            
            
//            //IF all text fields are satisfied, then display the pay button.
//            if (_completeCardNumber && _completeName && _completeCVV && _completeMonth && _completeYear)
//            {
//                _payButton.hidden = NO;
//                [_payButton becomeFirstResponder];
//            }
            
            //[_textfieldYear resignFirstResponder];
            break;
            
            
        default:
            break;
    }
}


//#define ACCEPTABLE_CHARECTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."
#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

//Return NO to NOT change text. This method is called BEFORE the text field has changed.
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    switch (textField.tag)
    {
        case 0: //Card Number.
            if (textField.text.length > 19 && !([string length] == 0 && range.length > 0)) //([string length] == 0 && range.length > 0) means some characters were deleted (delete key pressed). In our case, if some characters WEREN'T deleted.
            {
                [_textfieldCardNumber resignFirstResponder];
            }
            break;
            
        case 1: //Name Lastname.
        {
            //CAPITILIZES every letter.
            NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
            if (lowercaseCharRange.location != NSNotFound)
            {
                textField.text = [textField.text stringByReplacingCharactersInRange:range withString:[string uppercaseString]];
                
                return NO;
            }
            
            
            //Limits what can be written inside the text field. Allows only the characters defined inside 'ACCEPTABLE_CHARACTERS'.
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            
            return [string isEqualToString:filtered];
        }
            
            break;
            
        case 2: //CVV.
            if (textField.text.length > 2 && !([string length] == 0 && range.length > 0))
            {
                [_textfieldCVV resignFirstResponder];
                
                
//                //Does not allow the user input non-numeric characters. Clean approach.
//                NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
//                return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0) || [string isEqualToString:@""]; //This returns YES if the string is numeric, NO otherwise. The [string isEqualToString@""] is to support the backspace key to delete.
            }
            break;
            
        case 3: //Month.
            if (textField.text.length > 1 && !([string length] == 0 && range.length > 0))
            {
                int intValue = [textField.text intValue];
                if (intValue < 13)
                {
                    [_textfieldMonth resignFirstResponder];
                }
                else
                {
                    textField.text = [NSString stringWithFormat:@"Месяц"];
                    [_textfieldMonth resignFirstResponder];
                    
                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Подождити" message:@"Это строчка принимает цифры только от 01 до 12" delegate:self  cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [message show];
                }
            }
            break;
            
        case 4: //Year.
            if (textField.text.length > 1 && !([string length] == 0 && range.length > 0))
            {
                [_textfieldYear resignFirstResponder];
            }
            break;
            
            
        default:
            break;
    }
    
    ////NSLog(@"Character typed.");
    
    return YES;
}




//Called when 'return' key pressed. Return NO to ignore.
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //Automatically jump to the next text field when the user has completed each text fields requirements.
//    UIView *nextTextField = [self.view viewWithTag:textField.tag + 1];
//    [nextTextField becomeFirstResponder];
    
    [textField resignFirstResponder];
    
    return YES;
}




#pragma mark - EXTRAS

//This method is a reference to the text field of interest. It will catch whatever was inputted into the UITextField (_textfieldMonth in this case) and display it only after this method returns - It's like 'shouldChangeCharactersInRange' method except works afterwards. This method is used by the one line of code looking at it, inside viewDidLoad, called [yourTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged].
//-(void)textFieldDidChange:(UITextField *)textfield
//{
//    ////NSLog(@"Catch -> %@", textfield.text);
//}


//ADDS WHITESPACES to strings containing integers. Modify string to have appropriate whitespaces so a number such as 1000 would look like 1 000.
-(NSString*)addWhitespacesTo:(NSInteger)value
{
    NSNumberFormatter *formatter    = [NSNumberFormatter new];
    formatter.numberStyle           = NSNumberFormatterDecimalStyle; //This line is important! This is what actually formats the NSInteger value into a specific style. In our case, it's 1 000 from a previous value of 1000.
    NSString *formatted             = [formatter stringFromNumber:[NSNumber numberWithInteger:value]]; //Add whitespaces.
    
    return formatted;
}








//-(void) cvvFieldChanged: (id)sender
//{
//    if (_cvvField.text.length == 5)
//        [_cvvField resignFirstResponder];
//}


//-(BOOL)textFieldShouldReturn:(UITextField *)textField
//{
////    if (textField == _cvvField.textfield)
////    {
////        [self payButtonPressed:nil];
////        return YES;
////    }
//    
//    UIView *nextTextField = [self.view viewWithTag:textField.tag + 1];
//    [nextTextField becomeFirstResponder];
//
//    return YES;
//}


//-(void)formTextfieldFilled:(FormTextField *)textField
//{
//    if (textField == _yearField)
//    {
//        [_yearField resignFirstResponder];
//    }
//}




#pragma mark - NWRequestDelegate

-(void)requestCompletion:(NWRequest *)request withObject:(NSDictionary *)dict andError:(NSError *)error
{
    //NSLog(@"Payment response:\n%@", dict);
    if ([dict[@"result"] isEqualToString:@"ok"])
    {
        
    }
}




#pragma mark - WebViewVCDelegate

-(void)webViewWillReturn:(BOOL)success
{
    if (success)
    {
        FVCSubscriptionSuccess *vc  = [FVCSubscriptionSuccess new];
        vc.subscription             = self.subscription;
        [self.navigationController pushViewController:vc animated:YES];
        [[FVCContainer shared] switchToChild:1];
    }
}




//#pragma mark - UIAlertView
//
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex == 0)
//    {
//        //Delete it.
//    }
//    else if (buttonIndex == 1)
//    {
//        
//    }
//}

@end
