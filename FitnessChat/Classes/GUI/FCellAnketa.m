//
//  FCellAnketa.m
//  FitnessChat
//
//  Created by user on 21.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FCellAnketa.h"
#import "Question.h"
#import "Replies.h"


#define indent 10

@implementation FCellAnketa

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.contentView.backgroundColor = [UIColor colorWith256Red:244 green:246 blue:245];
        self.textLabel.font = [UIFont medium:15];
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.textLabel.numberOfLines = 0;
        self.detailTextLabel.font = [UIFont light:15];
        self.detailTextLabel.numberOfLines = 0;
        self.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (UIEdgeInsets)layoutMargins
{
    return UIEdgeInsetsZero;
}

- (void)setQuestion:(Question *)question
{
    _question = question;
    self.textLabel.text = _question.name;

    self.detailTextLabel.text = [_question createAnswer];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.textLabel.width = self.width - 2 *indent;
    self.detailTextLabel.width = self.width - 2 * indent;
    
    [self.textLabel sizeToFit];
    [self.detailTextLabel sizeToFit];
    
    self.textLabel.left = indent;
    self.detailTextLabel.left = indent;
    
    self.textLabel.top = indent;
    self.detailTextLabel.top = self.textLabel.bottom + indent;
}

+ (CGFloat)heightForQuestion:(Question *)question
{
    static UILabel *label;
    if (label == nil)
    {
        label = [UILabel new];
        label.font = [UIFont medium:15];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
    }
    label.width = [UIScreen mainScreen].bounds.size.width - indent * 2;
    label.text = question.name;
    [label sizeToFit];
    
    static UILabel *detailLabel;
    if (detailLabel == nil)
    {
        detailLabel = [UILabel new];
        detailLabel.font = [UIFont light:15];
        detailLabel.numberOfLines = 0;
        detailLabel.lineBreakMode = NSLineBreakByWordWrapping;
        detailLabel.text = @"Вы еще не дали ответ";
    }
    detailLabel.width = [UIScreen mainScreen].bounds.size.width - indent * 2;
    detailLabel.text = [question createAnswer];
    [detailLabel sizeToFit];

    
    return MAX(detailLabel.height,20) + label.height + indent * 3;
}

@end
