//
//  TutorialViewController.m
//  FitnessChat
//
//  Created by Михаил Кузеванов on 29.04.16.
//  Copyright © 2016 Алексей. All rights reserved.
//

#import "TutorialViewController.h"

#define TUT_COUNT 3

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_IPHONE_4_OR_LESS (SCREEN_MAX_LENGTH < 568.0)

@interface TutorialViewController ()<UIScrollViewDelegate>{
    UIScrollView *scrollView;
    UIPageControl *pageControl;
}

@end

@implementation TutorialViewController

- (void)viewDidLoad {
//    UIApplication *application = [UIApplication sharedApplication];
//    [application setStatusBarOrientation:UIInterfaceOrientationPortrait
//                                animated:NO];
    [super viewDidLoad];
    
    CGRect frameSc = self.view.bounds;
    if (frameSc.size.height < frameSc.size.width)
    {
        frameSc.size.width = self.view.bounds.size.height;
        frameSc.size.height = self.view.bounds.size.width;
    }
    scrollView = [[UIScrollView alloc] initWithFrame:frameSc];
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width*TUT_COUNT, scrollView.frame.size.height);
    [self.view addSubview:scrollView];
    CGFloat x = 0;
    for (int i=0; i<TUT_COUNT; i++)
    {
        NSString *imageName;
        if (IS_IPHONE_4_OR_LESS)
            imageName = [NSString stringWithFormat:@"tutor_ipad_%d", i+1];
        else
            imageName = [NSString stringWithFormat:@"tutor_%d", i+1];
        
        UIImageView *image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        //NSLog(@"%f", [[UIScreen mainScreen] scale]);
        [scrollView addSubview:image];
        image.frame = CGRectMake(x, 0, scrollView.frame.size.width, scrollView.frame.size.height);
        x+=scrollView.frame.size.width;
    }
    
    pageControl = [UIPageControl new];
    pageControl.numberOfPages = TUT_COUNT;
    [pageControl setDefersCurrentPageDisplay: YES];
    [pageControl setHighlighted:YES];
    [self.view addSubview:pageControl];
    [pageControl setCenter: CGPointMake(scrollView.center.x, scrollView.bounds.size.height - 10)];
    
    
    UIImage *imgNext = [UIImage imageNamed:@"tutor_next"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setBackgroundImage:imgNext];
    
    btn.frame = CGRectMake(0, 0, imgNext.size.width, imgNext.size.height);
    [btn addTarget:self action:@selector(nextTaped:)];
    [self.view addSubview:btn];
    
    CGRect frame = btn.frame;
    CGRect superFrame = scrollView.frame;
    frame.origin.x = superFrame.size.width - frame.size.width - 10;
    frame.origin.y = superFrame.size.height - frame.size.height - 10;
    btn.frame = frame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender
{
    //    //NSLog(@"scrollViewDidEndDecelerating");
    [self afterScroll];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView1
{
    //    //NSLog(@"scrollViewDidEndScrollingAnimation");
    [self afterScroll];
    
}

-(NSInteger)scrollPage
{
    CGFloat pageWidth = scrollView.bounds.size.width ;
    if (pageWidth == 0)
        return 0;
    
    NSInteger scroolPage = scrollView.contentOffset.x / pageWidth ;
    return scroolPage;
}

-(void)afterScroll
{
    pageControl.currentPage = [self scrollPage];;

}

-(void)nextTaped: (id)sender
{
    CGFloat newPage = [self scrollPage]+1;
    if (newPage < TUT_COUNT)
        [scrollView setContentOffset:CGPointMake(scrollView.frame.size.width*newPage, 0) animated:YES];
    else
        [self dismissViewControllerAnimated:YES completion:NULL];
}

- (BOOL)shouldAutorotate {
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
