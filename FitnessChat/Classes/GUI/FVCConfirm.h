//
//  FVCConfirm.h
//  FitnessChat
//
//  Created by user on 10.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "FVC.h"


@interface FVCConfirm : FVC

@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *fullPhone;
@property (nonatomic) BOOL isLogin;

@end
