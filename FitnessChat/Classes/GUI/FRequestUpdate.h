//
//  FRequestUpdate.h
//  FitnessChat
//
//  Created by user on 24.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "NWRequest.h"

@interface FRequestUpdate : NWRequest
@property (nonatomic) int timestamp;

@end
