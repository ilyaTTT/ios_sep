//
//  FVCContainer.h
//  FitnessChat
//
//  Created by Arcanite LLC on 10/10/15.
//  Copyright © 2015 Arcanite LLC. All rights reserved.
//

#import "FVC.h"

//@class FVCContainer;

@interface FVCContainer : FVC

@property (strong, nonatomic) UIScrollView *rootScrollView;


+(instancetype) shared;
- (void) reset;

- (void) addChild:(UIViewController*)vc;
- (void)switchToChild:(NSInteger)childNumber animated:(BOOL)animated;
- (void) switchToChild: (NSInteger)childNumber;
- (void) reloadData;
- (void) updateConsultant: (NSString*)consultantId;

@end
