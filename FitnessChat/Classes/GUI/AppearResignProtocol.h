//
//  ApeearResignProtocol.h
//  RuRu
//
//  Created by Алексей on 29.05.14.
//  Copyright (c) 2014 Work. All rights reserved.
//

@protocol AppearElementProtocol;

@protocol AppearResignProtocol <NSObject>

- (void)onResignFirstResponder;
- (void)onAppearElement:(UIView <AppearElementProtocol> *)element;
@end

@protocol AppearElementProtocol
- (void)setAppearDelegate:(id <AppearResignProtocol>)delegate;

@end