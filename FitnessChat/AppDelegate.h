//
//  AppDelegate.h
//  PATTERN
//
//  Created by Алексей on 31.05.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//Core Data properties.
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator    *persistentStoreCoordinator; //The persistent store coordinator is your more direct access to the data store; it is the connection to the database. Big Core Data projects can have their data model spread over several data model files; each one corresponds to an object store. The persistent store coordinator coordinates the interactions with each store file on disk; there is only one coordinator per stack.
@property (readonly, strong, nonatomic) NSManagedObjectModel            *managedObjectModel; //The managed object model represents the objects in the data store during the execution of the program. It contains information about the objects (“entities”) you defined in the data store; it gets an aggregate of all the entities across all the store files from the persistent store coordinator.
@property (readonly, strong, nonatomic) NSManagedObjectContext          *managedObjectContext; //The managed object context is where a lot of the interaction with Core Data occurs. This is where objects are created, modified, and then committed to disk. Apple calls this a “scratch pad for managed objects."

@property (assign, nonatomic) UIDeviceOrientation allowedOrientation;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

+ (AppDelegate *)shared;

- (void)showActivity;
- (void)hideActivity;
- (void)showErrors:(NSArray *)errors;
- (void)showTopText:(NSString *)text;
- (void)showImage:(UIImage *)image;

- (void)logout;

- (UIViewController*) mainVC;

-(void)showLoginViewController; 

@end

