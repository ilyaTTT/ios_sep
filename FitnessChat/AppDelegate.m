//
//  AppDelegate.m
//  PATTERN
//
//  Created by Алексей on 31.05.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "AppDelegate.h"
#import "FVCLogin.h"
#import "FTabbar.h"
#import "NBS3Uploader.h"
#import "FImageView.h"
#import "FRequestPushTokenSave.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "FVCContainer.h"
#import "FVCSubscriptionSuccess.h"

@interface AppDelegate ()
{
    UINavigationController *_loginNC;
}

@property (strong, nonatomic) UIActivityIndicatorView   *activity;
@property (strong, nonatomic) UILabel                   *topLabel;
@property (strong, nonatomic) FImageView                *imageView;

@end


@implementation AppDelegate

+ (AppDelegate *)shared
{
    return [UIApplication sharedApplication].delegate;
}

- (void)showActivity
{
    if ([NSThread isMainThread])
    {
        [self.window addSubview:self.activity];
        [self.activity startAnimating];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.window addSubview:self.activity];
            [self.activity startAnimating];
        });
    }
}

- (void)hideActivity
{
    if ([NSThread isMainThread])
    {
        [self.activity removeFromSuperview];
        [self.activity stopAnimating];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.activity removeFromSuperview];
            [self.activity stopAnimating];
        });
    }
}

- (UIActivityIndicatorView *)activity
{
    if (_activity == nil)
    {
        _activity = [[UIActivityIndicatorView alloc] initWithFrame:self.window.bounds];
        _activity.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
        _activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        
    }
    
    return _activity;
}

- (void)showImage:(UIImage *)image
{
//    _imageView = nil;
    self.imageView.image = image;
    [self.imageView show];
//    //NSLog(@"-------------------------------");
//    for (int i = 0; i < self.window.rootViewController.view.subviews.count; ++i) // test
//    {
//        //NSLog(@"\n%@", self.window.rootViewController.view.subviews[i]);
//    }
}

- (void)logout
{
    [User setCurrUser:nil];
    [User clean];
    [FTabbar clean];
    
//    if (_loginNC)
//    {
//        [_loginNC popToRootViewControllerAnimated:NO];
//        [_loginNC dismissViewControllerAnimated:YES completion:nil];
//    }
//    else
//    {
//
//    }
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:[FVCLogin new]];
    nc.navigationBar.translucent = NO;
    _loginNC = nc;
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = nc;
    [self.window makeKeyAndVisible];
    
    [User clean];
    [[FVCContainer shared] reset];
}

- (FImageView *)imageView
{
    if (_imageView == nil)
    {
        _imageView = [[FImageView alloc] initWithFrame:self.window.bounds];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//        [self.window.rootViewController.view addSubview:_imageView];
        [self.window addSubview:_imageView];
    }
    return _imageView;
}


//Displays a message at the top of the screen every time UINavigationBarItem.Right ("Сохранить") is clicked.
- (void)showTopText:(NSString *)text
{
    self.topLabel.text = text;
    self.topLabel.bottom = 0;
    self.window.top = 0.01;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.topLabel.top = 0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 delay:1 options:UIViewAnimationOptionAllowUserInteraction animations:^{
            self.topLabel.bottom = 0;
        } completion:^(BOOL finished) {
            [UIApplication sharedApplication].statusBarHidden = NO;
            self.window.top = 0;
        }];
    }];
}

- (UILabel *)topLabel
{
    if (_topLabel == nil)
    {
        CGFloat height = [UIApplication sharedApplication].statusBarFrame.size.height;
        _topLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, - height, self.window.width, height)];
        _topLabel.backgroundColor = [UIColor gray];
        _topLabel.textColor = [UIColor whiteColor];
        _topLabel.textAlignment = NSTextAlignmentCenter;
        [self.window addSubview:_topLabel];
    }
    return _topLabel;
}

- (void)showErrors:(NSArray *)errors
{
    if (errors.count == 0)
    {
//        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Проверьте интернет соединение" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    NSMutableString *string = [NSMutableString new];
    
    for (NSString *error in errors)
    {
        [string appendFormat:@"%@\n",error];
    }
    [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:string delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

-(void)showLoginViewController
{
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:[FVCLogin new]];
    nc.navigationBar.translucent = NO;
    nc.view.alpha = 0;
    
    self.window.rootViewController = nc;
    [self.window makeKeyAndVisible];
    
    [UIView animateWithDuration:0.5 animations:^{
            nc.view.alpha = 1;
    }];
    
    _loginNC = nc;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    if ([UIApplication.sharedApplication respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationSettings * settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge | UIUserNotificationTypeAlert | UIUserNotificationTypeSound categories:nil];
        [UIApplication.sharedApplication registerUserNotificationSettings:settings];
        [UIApplication.sharedApplication registerForRemoteNotifications];
    }
    else
    {
        [UIApplication.sharedApplication registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
    }
    
    [Fabric with:@[[Crashlytics class]]];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];

    if ([User currentUser])
    {
        [[FRequestPushTokenSave new] start];
        
        UIViewController *controller = [self mainVC];
        controller.view.alpha = 0;
        
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:1.2 animations:^{
            controller.view.alpha = 1;
            [weakSelf.window setRootViewController:controller];
        }];
        
//        self.window.rootViewController = [FVCSubscriptionSuccess new]; // test
    }
    else
    {
//        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:[FVCLogin new]];
//        nc.navigationBar.translucent = NO;
//        self.window.rootViewController = nc;
//        _loginNC = nc;
        
        [self showLoginViewController];
    }
    [NBS3Uploader uploader];

    
    [self.window makeKeyAndVisible];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor patternColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"HelveticaNeue-Light" size:17], NSFontAttributeName, nil]];

    UIImage * backButtonImage = [UIImage imageNamed: @"back_button"];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage: backButtonImage forState: UIControlStateNormal barMetrics: UIBarMetricsDefault];
   
    self.allowedOrientation = UIInterfaceOrientationMaskPortrait;

    
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSData *data = deviceToken;
    unichar* hexChars = (unichar*)malloc(sizeof(unichar) * (data.length*2));
    unsigned char* bytes = (unsigned char*)data.bytes;
    for (NSUInteger i = 0; i < data.length; i++)
    {
        unichar c = bytes[i] / 16;
        if (c < 10) c += '0';
        else c += 'A' - 10;
        hexChars[i*2] = c;
        c = bytes[i] % 16;
        if (c < 10) c += '0';
        else c += 'A' - 10;
        hexChars[i*2+1] = c;
    }
    NSString* retVal = [[NSString alloc] initWithCharactersNoCopy:hexChars length:data.length*2 freeWhenDone:YES];
    [[NSUserDefaults standardUserDefaults] setObject:retVal forKey:@"kPushToken"];
    
    if ([User currentUser])
    {
        [[FRequestPushTokenSave new] start];
    }
    
    //NSLog(@"My token is: %@", retVal);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //NSLog(@"Failed to get token, error: %@", error);
}

 -(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    return self.allowedOrientation;
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    application.applicationIconBadgeNumber = 0;
}


//Right when the application is about to terminate, this method is called, which in turn calls 'saveContext' method. 'saveContext' will save Core Data entities.
- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}




#pragma mark - 

-(UIViewController*) mainVC
{
    return [FVCContainer shared];
}




#pragma mark - Core Data stack

@synthesize managedObjectContext        = _managedObjectContext;
@synthesize managedObjectModel          = _managedObjectModel;
@synthesize persistentStoreCoordinator  = _persistentStoreCoordinator; //The persistent object store represents the stored data file on disk; you normally don’t interact with the object store.

- (NSURL *)applicationDocumentsDirectory
{
    // The directory the application uses to store the Core Data store file. This code uses a directory named "ru.studiomobile..PATTERN" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


//
-(NSManagedObjectModel *)managedObjectModel
{
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil)
    {
        return _managedObjectModel;
    }
    NSURL *modelURL     = [[NSBundle mainBundle] URLForResource:@"PATTERN" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil)
    {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store.
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL         = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"PATTERN.sqlite"];
    //NSLog(@"!!!STORE URL: %@", [storeURL absoluteString]);
    NSError *error          = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption: @(true),
                              NSInferMappingModelAutomaticallyOption: @(true)
                              };
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error])
    {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


//CORE DATA.
- (NSManagedObjectContext *)managedObjectContext
{
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil)
    {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator)
    {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}




#pragma mark - Core Data Saving support

//
-(void)saveContext
{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) //IF 'hasChanges' is YES (if there are changes to managed objects) then 'save' will save these managed objects.
        {
            // Something's gone seriously wrong.
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
