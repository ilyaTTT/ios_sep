//
//  Chat.h
//  FitnessChat
//
//  Created by Arcanite LLC on 23.07.15.
//  Copyright (c) 2015 Arcanite LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@class Message;

@interface Chat : NSManagedObject


// Core Data Managed Objects.
@property (nonatomic, retain) NSString * chatId;    // Attribute.
@property (nonatomic, retain) NSString * title;     // Attribute.
@property (nonatomic, retain) NSSet *messages;      // Relationship.

@end


@interface Chat (CoreDataGeneratedAccessors)

- (void)addMessagesObject:(Message *)value;
- (void)removeMessagesObject:(Message *)value;
- (void)addMessages:(NSSet *)values;
- (void)removeMessages:(NSSet *)values;

@end
