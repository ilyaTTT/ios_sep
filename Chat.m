//
//  Chat.m
//  FitnessChat
//
//  Created by user on 23.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "Chat.h"
#import "Message.h"


@implementation Chat

@dynamic chatId;
@dynamic title;
@dynamic messages;

@end
