//
//  File.m
//  FitnessChat
//
//  Created by Алексей on 26.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import "File.h"
#import "Message.h"
#import "User.h"
#import "NBS3Uploader.h"


@implementation File

@dynamic name;
@dynamic url;
@dynamic type;
@dynamic message;
@dynamic user;
@dynamic filesize;


- (void)prooveName
{
    if (self.name.length == 0)
    {
        self.name = [[NSUUID UUID] UUIDString];
        
        if (self.type == FILE_Video)
        {
            self.name = [self.name stringByAppendingString:@".mp4"];
        }
    }

}


- (void)setData:(NSData *)data
{
    [self prooveName];
    [self saveImageData:data withName:self.name];
}

-(void)loadDataIfNeddedWithCompletion:(void (^)(NSData *))completion
{
    [self loadDataIfNeddedWithCompletion:completion progress:NULL];
}

- (void)loadDataIfNeddedWithCompletion:(void (^)(NSData *data))completion progress:(NBS3UploaderProgress)progress
{
    NSData *data = self.data;
    
    if (data == nil)
    {
        [[NBS3Uploader uploader] exportFile:self completion:^(BOOL succeess) {
            completion([self loadImageWithName:self.name]);
        }
         progress:progress];
    }
    else
    {
        completion(data);
    }
}


- (NSData *)data
{
    [self prooveName];
    
    NSData *data = [self loadImageWithName:self.name];
    
    return data;
}


- (void)saveImageData:(NSData*)data withName:(NSString *)name
{
    if (data != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        //        //NSLog(@"%@",documentsDirectory);
        NSString* path = [documentsDirectory stringByAppendingPathComponent:
                          name];
        
//        if (self.type == FILE_Video)
//        {
//            path = [path stringByAppendingString:@".mp4"];
//        }
        if(![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory isDirectory:nil])
        {
            NSError * error = nil;
            [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory
                                      withIntermediateDirectories:YES
                                                       attributes:nil
                                                            error:&error];
            if (error != nil) {
                //NSLog(@"error creating directory: %@", error);
            }
        }
        
        if ([data writeToFile:path atomically:YES])
        {
            
        }
    }
}

- (NSData *)loadImageWithName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      name];
//    if (self.type == FILE_Video)
//    {
//        path = [path stringByAppendingString:@".mp4"];
//    }
    
    return [NSData dataWithContentsOfFile:path];
}

- (void)clear
{
    if (self.name.length > 0)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:
                          self.name];
//        if (self.type == FILE_Video)
//        {
//            path = [path stringByAppendingString:@".mp4"];
//        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
        });
    }
    self.name = [[NSUUID UUID] UUIDString];
}

-(NSString *)localPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      self.name];
    
//    if (self.type == FILE_Video)
//    {
//        path = [path stringByAppendingString:@".mp4"];
//    }
    return path;
}

- (NSURL *)localUrl
{
    return [NSURL fileURLWithPath:[self localPath]];
}

@end
