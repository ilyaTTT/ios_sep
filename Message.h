//
//  Message.h
//  FitnessChat
//
//  Created by user on 23.07.15.
//  Copyright (c) 2015 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Chat, File;

typedef enum : int {
    MESSAGE_Text          = 0,
    MESSAGE_Image         = 1,
    MESSAGE_Audio         = 2,
    MESSAGE_Video         = 3,
    MESSAGE_Audio_Voice   = 4,
    MESSAGE_GOTO          = 5
} MessageType;

@interface Message : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * messageId;
@property (nonatomic, retain) NSString * text;
@property (nonatomic) MessageType type;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) Chat *chat;
@property (nonatomic, retain) File * file;
@property (nonatomic) BOOL sending;
@property (nonatomic, strong) NSString *helpDate;
@property (nonatomic) BOOL deleted;
@property (nonatomic) BOOL readed;
@property (nonatomic) int paymentAmount;
@property (nonatomic) int localId;

- (BOOL)isMy;

- (NSString *)sectionName;

@end
